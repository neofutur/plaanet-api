const errorHandler = require('../middleware/error-handler');

module.exports = async function errorHandlerLoader({expressApp}) {
  expressApp.use(errorHandler());
}