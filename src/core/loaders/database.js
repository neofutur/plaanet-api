const mongoose = require('mongoose');

const Config = require('../config');

module.exports = async function databaseLoader() {
  const dbUrl = Config.Database.PASS
    ? `mongodb://${Config.Database.HOST}:${Config.Database.PORT}/${Config.Database.NAME}?authSource=admin&readPreference=primary&appName=plaanet-api&ssl=${Config.Database.SECURE}`
    : `mongodb://${Config.Database.HOST}:${Config.Database.PORT}/${Config.Database.NAME}?readPreference=primary&appName=plaanet-api&ssl=${Config.Database.SECURE}`;
  
  try {
    if (!Config.Database.PASS) {
      console.log('WARN: Running in unsecure mode');
      console.log(`WARN: You are connecting to a non-password protected MongoDB database,
      make sure you don't have port ${Config.Database.PORT} accessible from the Internet.
      Please consider securing your database with a password.
      Not doing that may result in hackers/ransomwares to fuck with your data.`);
    }


    const mongooseOpts = {
      useNewUrlParser: true,
      useUnifiedTopology: true,
      user: Config.Database.USER,
      pass: Config.Database.PASS,
      dbName: Config.Database.NAME,
    };

    await mongoose.connect(dbUrl, mongooseOpts);
    console.log(`INFO: Connnected to database (${dbUrl}) !`);
  } catch (err) {
    console.error(`ERROR: Error: ${err.message}`);
    console.error(`ERROR: Cannot connect to database (${dbUrl}). Terminating...`);
    throw err;
  }
};