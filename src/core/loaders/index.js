const databaseLoader = require('./database');
const databaseModelsLoader = require('./databaseModels');
const initializedStateLoader = require('./initializedState');
const corsLoader = require('./cors');
const bodyParserLoader = require('./bodyParser');
const cookiesLoader = require('./cookies');
const staticFolderLoader = require('./staticFolder');
const websocketLoader = require('./websocket');
const lastUserActivityLoader = require('./lastUserActivity');

// In this function, execution order MATTERS.
module.exports = async function runLoaders(ctx) {
  // Initialize database connection first
  await executeLoader(ctx, databaseLoader);

  // Then Mongoose ORM models
  await executeLoader(ctx, databaseModelsLoader);

  // Check if instance is initialized and generate init token if false
  await executeLoader(ctx, initializedStateLoader);

  // Setup CORS middleware 
  await executeLoader(ctx, corsLoader);

  // Setup body parser
  await executeLoader(ctx, bodyParserLoader);

  // Setup CookieParser middleware
  await executeLoader(ctx, cookiesLoader);

  // Setup static folder/files serving
  await executeLoader(ctx, staticFolderLoader);

  // Setup websocket for notifications
  await executeLoader(ctx, websocketLoader);

  // Setup last user activity global middleware
  await executeLoader(ctx, lastUserActivityLoader)
};

async function executeLoader(ctx, loader) {
  try {
    console.log(`DEBUG: Running loader ${loader.name}`);
    return loader.call(null, ctx);
  } catch (err) {
    console.log(`ERROR: Cannot execute loader ${loader.name}: ${err.message}`);
    throw err;
  }
}