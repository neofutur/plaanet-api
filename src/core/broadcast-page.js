const Instance = require('../models/Instance')
const Page = require('../models/Page')
const PageService = require("../services/page");
const axios = require('axios');


const core = require('../core/core');

let broadPage = {
    serializeList : async function(pageList, res){
        let dataRes = []
        const localNode = await Instance.findOne({ isLocal:true })
        await Promise.all(pageList.map(async (page, i) => {
            let completPage = await broadPage.getPageInfo(page, localNode.name)
            if(completPage != false){
                //console.log("completPage", completPage)
                dataRes.push(completPage)
            }
        }))
        return dataRes
    } ,

    getPageInfo : async function(page, nodeName){
        //console.log("broadPage.getPageInfo", page.uid)
        if(nodeName == core.iName(page.uid)){
            let pageService = new PageService()
            let p = pageService.getPageProfile(page.uid, null) 
            return p
        }else{
            let nodePath = await core.getNodePath(page.uid)
            let resTmp = await core.asyncPost(nodePath + '/broadcast/page-profil', 
                                                { pageUid: page.uid })

            if(resTmp != false && resTmp.data.error !== true){
                //console.log("resTmp.data", resTmp.data)
                return resTmp.data.page
            } 
            else return false
        }
        return false
    }
   
    
}
module.exports = broadPage;