/*var express = require("express")
var router = express.Router()
var app = express()

const auth = require("../core/middleware/auth-anonymous");
const bcrypt = require("bcryptjs");


const jwt = require('jsonwebtoken');
const axios = require('axios');
const config = require('config');
*/
const { User, validate } = require('../models/User')
//const Wallet = require('../models/Wallet')
const Instance = require('../models/Instance')
//const StateHistory = require('../models/StateHistory')
const Page = require('../models/Page')
const Publication = require('../models/Publication')
const Notification = require('../models/Notification')
const Comment = require('../models/Comment')
const Survey = require('../models/Survey')

      auth: '{{ notifAuthor.uid }}'
const config = require("config");
const https = require('https');
const axios = require('axios');
var fs = require('fs');

/*var axios = null

if(config.get('env') == "prod"){
    var agent = new https.Agent({ 
        ca: fs.readFileSync('ca_bundle.crt'),
    })
    console.log("CREATED AGENT FOR BROADCAST", agent)
    axios = ax.create({ agent: agent });
}else{
    axios = ax
}*/

let core = {
    iName: function(uid){
        let i = uid.indexOf(':')
        return uid.substr(0, i)
    },
    iKey: function(uid){
        let i = uid.indexOf(':')
        return uid.substr(i+1, uid.length)
    },
    getNodePath: async function(uid){
        let nodeName = core.iName(uid)
        const rootNode = await Instance.findOne({'name': nodeName}) 
        //console.log("getNodePath",  rootNode.url + ":" + rootNode.port)
        return rootNode.url + ":" + rootNode.port
    },
    getRandUid: function(iName){
        //console.log("Math.random().toString(36)", Math.random().toString(36))
        let uid = ''
        for(let i=0; i<3; i++) uid += Math.random().toString(36).substr(2, 9)
        return iName + ':' + uid;
    },
    kmMax : 1000000,
    getKmToday : async function(postSignedUid){
        let today = new Date()
        today.setHours(0,0,0,0);
        let todayStream = await Publication.find( { created: { '$gte' : today },
                                                    scopeType: 'localised',
                                                    signed: core.iKey(postSignedUid),
                                                    isFirst: { '$ne' : true }
                                                  } )
        let kmToday = 0
        //console.log("kmToday", kmToday, todayStream)
        if(todayStream != null){
            todayStream.forEach((post) => {
                kmToday += post.radius
                //console.log("kmToday", kmToday)
            })
        } 
        return kmToday
    },
    extractTags : function(text){
        let tags = text.match(/(^|\s)(#[a-zA-Z0-9áàâäãåçéèêëíìîïñóòôöõúùûüýÿæœÁÀÂÄÃÅÇÉÈÊËÍÌÎÏÑÓÒÔÖÕÚÙÛÜÝŸÆŒ_\d-]+)/ig);
        //console.log("extractTags", tags)
        if(tags != null)
        tags.forEach((tag, key) => {
            tags[key] = tag.replace(" ", "");
        })
        //console.log("extractTags", tags)
        
        return tags
    },
    extractMention : function(text){
        let mentions = text.match(/(^|\s)(@[a-zA-Z0-9áàâäãåçéèêëíìîïñóòôöõúùûüýÿæœÁÀÂÄÃÅÇÉÈÊËÍÌÎÏÑÓÒÔÖÕÚÙÛÜÝŸÆŒ_\d-]+)/ig);
        //console.log("extractTags", tags)
        if(mentions != null)
        mentions.forEach((mention, key) => {
            mentions[key] = mention.replace(" ", "_");
        })
        //console.log("extractTags", tags)
        
        return tags
    },
    getSlug: function(str){
        return str
            .replace(/\s(.)/g, function($1) { return $1.toUpperCase(); })
            .replace(/\s/g, '')
            .replace(/^(.)/, function($1) { return $1.toLowerCase(); });
    },
    isRole: (userPageUid, page, role) => {
      let found = false
      page.roles[role].forEach((p)=>{  if(p.uid == userPageUid) found = true })
      return found
    },
    

    broadcaster : {

        init: function(){ //console.log("XXXXX init core - token:")
            axios.interceptors.request.use(
                async (config) => {
                let i = await Instance.findOne({ isLocal: true })
                let token = i.name + ':' + i.nodeKey
                //console.log("XXXXX init core - token:", token)
                if (token) {
                    config.headers['x-auth-token'] = token
                }
                return config;
                }, 
                (error) => {
                return Promise.reject(error)
                }
            );
        },
        
        get : async function(urlAction, fThen, fCatch, concat){
            core.broadcaster.init()
            //get info about all instances in the network
            const network = await Instance.find() //{'isLocal': false}) 
            const nbNode = network.length
            //init array for response datas
            let dataRes = []
            //init counter of response from nodes
            let nbNodeOk = 0

            try{
                network.forEach(node => {
                    console.log("send request to", node.url+":"+node.port)
                    //send request to each node in the network
                    //only if the node is not lockedOut 
                    if(node.lockOut == false && node.isActive == true){
                        axios.get(node.url+":"+node.port + urlAction)
                            .then(result => {
                                //incremment counter of response
                                nbNodeOk++
                                //for each response
                                if(result.data.error != true){
                                    //if params concat is not null && there are data in response : 
                                    //concat data reponse to finally send all in one array
                                    if(concat != null && result.data[concat] != null)
                                        dataRes = dataRes.concat(result.data[concat])
                                    console.log("nbNodeOk", nbNodeOk, "nbNode", nbNode)
                                    //if it is the last responses
                                    if(nbNodeOk == nbNode){  fThen(dataRes) } //exec fThen function from parameters
                                }else{
                                    //if response send an error
                                    //fCatch(result) //exec fCatch function from parameters
                                }

                            }).catch(result => { //if request failed
                                //incremment counter of response
                                nbNodeOk++
                                console.log("error unreachable node", node.url+":"+node.port)
                                if(nbNodeOk == nbNode){  fThen(dataRes) }
                                //fCatch(result) //exec fCatch function from parameters
                            })
                    }else{
                        nbNodeOk++
                        if(nbNodeOk == nbNode){  fThen(dataRes) }
                    }
                })
            }
            catch(e){
                console.log("CATCH ERROR", e)
            }
            //if no instance in the network
            if(network.length==0) 
                res.json({  error: true,
                            errorMsg: 'This network is empty' })
        },
    

        post : async function(urlAction, postParams, fThen, fCatch, concat, ignoreSelfNode=false){
            core.broadcaster.init()
            //get info about all instances in the network
            const network = await Instance.find() //{'isLocal': false}) 
            const localNode = await Instance.find({'isLocal': true}) 
            const nbNode = network.length
            //init array for response datas
            let dataRes = []
            //init counter of response from nodes
            let nbNodeOk = 0

            await Promise.all(network.map(async (node) => {
                //console.log("#CORE.BC.post SEND to", node.url+":"+node.port)
                //send request to each node in the network
                //only if the node is not lockedOut 
                if(!ignoreSelfNode || (node.name != localNode.name)){
                    if(node.lockOut == false && node.isActive == true){
                        try{
                            let result = await axios.post(node.url+":"+node.port + urlAction, postParams, { timeout: 5000 })
                            //console.log("#CORE.BC.post RESULT from", node.url+":"+node.port, result.status)

                            if(result.data.error != true){
                                //console.log("#CORE.BC.post SUCCESS from", node.url+":"+node.port)
                                //if params concat is not null : concat data reponse to finally send all in one array
                                if(concat != null)
                                    dataRes = dataRes.concat(result.data[concat])
                            }                                
                        }catch(e){
                            //console.log("#CORE.BC.post ERROR from", node.url+":"+node.port, 'error.code:', e.code)
                        }
                    }else{ //if the node is lockedOut 
                        //nbNodeOk++
                        //if(nbNodeOk == nbNode){  fThen(dataRes) }
                    }
                }
            }))
            fThen(dataRes)
            return dataRes
            
            //if no instance in the network
            // if(network.length==0) 
            //     res.json({  error: true,
            //                 errorMsg: 'This network is empty' })
        },

        checkActivity : async function(fThen, fCatch){
            core.broadcaster.init()
            //get info about all instances in the network
            const network = await Instance.find() //{'isLocal': false}) 
            const nbNode = network.length
            //init array for response datas
            let dataRes = []
            //init counter of response from nodes test
            let nbNodeOk = 0

            await Promise.all(network.map(async (node) => {
                console.log("send request to", node.url+":"+node.port)
                //send request to each node in the network
                let res = await axios.get(node.url+":"+node.port + '/broadcast/instance-info', { timeout: 5000 })
                        .then((data) => {
                            console.log("> success " + node.url+":"+node.port + '/broadcast/instance-info')
                            node.isActive = true

                            if(data.data.errorStatus == 'lockIn') node.lockBy = true
                            else node.lockBy = false

                            let atts = ['name', 'nodeKey', 'url', 'port', 'portSocket',
                                        'city', 'state', 'position', 'clientUrl', 'clientPort', 'env',
                                        'nbPosts', 'nbUsers', 'nbNotifs', 'nbMobileUsers', 'dateInfoUpdated', 'admins']
                            atts.forEach((att) => { node[att] = data.data.instance[att] })
                            
                            node.unreachable = false
                            node.save()
                            dataRes.push(node)
                        })
                    .catch((error) => {
                        console.log(`ERROR: Cannot broadcast instance infos to ${node.url}:${node.port}. Error: ${error.message}`);
                            console.log("> error " + node.url+":"+node.port + '/broadcast/instance-info')
                            node.isActive = false

                            node.unreachable = (error.message.indexOf('ECONNREFUSED') > -1 
                                                || error.message.indexOf('ETIMEDOUT') > -1
                                                || error.message.indexOf('ECONNABORTED') > -1)
                            //if(error.message.indexOf('401')) node.unreachable = false
                            node.save()
                            dataRes.push(node)
                        })
                console.log("infos", res)
            }))

            console.log("endof core.bcstr.checkActivity", 'dataRes.length', dataRes.length)
            return { error : false, network: dataRes }
            //if no instance in the network
            // if(network.length==0) 
            //     res.json({  error: true,
            //                 errorMsg: 'This network is empty' })
            

        }
    },

    get : async function(url, params, fThen, fCatch){
        core.broadcaster.init()
        axios.get(url, params).then(res => {
            fThen(res)
        }).catch(res => {
            fCatch(res)
        })
    },

    get : async function(url, params, fThen, fCatch){
        core.broadcaster.init()
        axios.get(url, { params: params }).then(res => {
            fThen(res)
        }).catch(res => {
            fCatch(res)
        })
    },

    post : async function(url, postParams, fThen, fCatch){
        core.broadcaster.init()
        axios.post(url, postParams).then(res => {
            fThen(res)
        }).catch(res => {
            fCatch(res)
        })
    },

    asyncPost : async function(url, postParams){
        core.broadcaster.init()
        //console.log("asyncPost", url)
        let res = await axios.post(url, postParams, { timeout: 5000 }).catch((e) => {
            console.log("error in asyncPost", url, postParams)
            return false
        })
        //console.log("asyncPost res", res.data)
        return res
    },

    notify : {
        send : async function(req, notifParams, targetsTypes, inDB=false){
            //get all target for this notification
            let allTargets1 = []
            await Promise.all(targetsTypes.map(async (targetType, i) => {
                let allT = await core.notify.getTargets(targetType, notifParams)
                allTargets1 = allTargets1.concat(allT)
            }));

            //remove double
            let allTargets = []
            if(typeof allTargets1 != "undefined")
            allTargets1.forEach((targetUid, i) => {
                if(allTargets.indexOf(targetUid) == -1 
                && targetUid != notifParams.authors[0].uid) 
                    allTargets.push(targetUid)
            })

            
            let now = new Date()
            if(inDB == true){
                let notif = await Notification.findOne({ 
                                        'verb': notifParams.verb,
                                        'whatObj.uid' : notifParams.whatObj.uid })
                if(notif == null && notifParams.aboutObj != null){
                    notif = await Notification.findOne({ 
                                    'verb': notifParams.verb,
                                    'aboutObj.uid' : notifParams.aboutObj.uid })
                }
                
                if(notif != null){

                    let found=false
                    notif.authors.forEach((author, i) => {
                        if(author.uid == notifParams.authors[0].uid)
                        found = i
                    })
                    if(found != false) notif.authors.splice(found, 1)
                    notif.authors.push(notifParams.authors[0])

                    notif.whatObj = notifParams.whatObj
                    notif.aboutObj = notifParams.aboutObj
                    notif.updated = now
                    notif.targets = allTargets
                    notif.save()
                }else{
                    //new Notification
                    console.log("must save Notification in DB")
                    let notif = new Notification(notifParams)
                    notif.created = now
                    notif.updated = now
                    notif.targets = allTargets
                    notif.save()
                }
            }

            notifParams.updated = now

            console.log("all target found", allTargets)
            if(typeof allTargets != "undefined")
            allTargets.forEach((targetUid, i) => {
                console.log("try emit 'receive-notif' to", targetUid)
                req.ws.emit(targetUid, 'receive-notif', notifParams)
                console.log("** EMITED sock to", targetUid)
                
            })
        },

        getTargets: async function(targetType, params){
            let allTargets = []
            //console.log("targetType", targetType)
            //await targetsTypes.forEach(async (targetType, i) => {
                console.log("targetType", targetType)
                if(params.verb == 'COMMENT'){
                    if(targetType == 'viewers'){
                        let post = await Publication.findOne({ uid: params.aboutObj.uid })
                        allTargets = post.viewers
                    }
                    if(targetType == 'commenters'){
                        let post = await Publication.findOne({ uid: params.aboutObj.uid }).populate('comments')
                        post.comments.forEach((com, i) => {
                            if(allTargets.indexOf(com.author.uid) == -1)
                                allTargets.push(com.author.uid)
                        })
                    }
                    if(targetType == 'favorites'){
                        let post = await Publication.findOne({ uid: params.aboutObj.uid })
                        allTargets = post.favorites
                    }
                    if(targetType == 'signed'){
                        let post = await Publication.findOne({ uid: params.aboutObj.uid }).populate('signed')
                        allTargets.push(post.signed.uid)
                    }
                    if(targetType == 'receivers'){
                        let post = await Publication.findOne({ uid: params.aboutObj.uid })
                        //allTargets = post.receivers
                        post.receivers.forEach((page, i) => {
                            allTargets.push(page.uid)
                        })
                    }
                }

                console.log("send notif check", params)
                if((params.verb == 'LIKES' || params.verb == 'DISLIKES' 
                ||  params.verb == 'VIEWERS'||  params.verb == 'FAVORITES' 
                ||  params.verb == 'NEW_PRIVATE_MSG')
                     && params.whatObj.type == "publication"){
                    if(targetType == 'viewers'){
                        let post = await Publication.findOne({ uid: params.whatObj.uid })
                        allTargets = post.viewers
                    }
                    if(targetType == 'commenters'){
                        let post = await Publication.findOne({ uid: params.whatObj.uid }).populate('comments')
                        post.comments.forEach((com, i) => {
                            if(allTargets.indexOf(com.author.uid) == -1)
                                allTargets.push(com.author.uid)
                        })
                    }
                    if(targetType == 'receivers'){
                        console.log("target receivers", params)
                        let post = await Publication.findOne({ uid: params.whatObj.uid }).populate('comments')
                        post.receivers.forEach((page, i) => {
                            if(allTargets.indexOf(page.uid) == -1)
                                allTargets.push(page.uid)
                        })
                    }
                    if(targetType == 'favorites'){
                        let post = await Publication.findOne({ uid: params.whatObj.uid })
                        allTargets = post.favorites
                    }
                    if(targetType == 'signed'){
                        let post = await Publication.findOne({ uid: params.whatObj.uid }).populate('signed')
                        allTargets.push(post.signed.uid)
                    }
                }

                if((params.verb == 'EMOJI')
                && params.whatObj.type == "comment"){
                    if(targetType == 'viewers'){
                        let post = await Publication.findOne({ uid: params.aboutObj.uid })
                        allTargets = post.viewers
                        console.log('allTargets EMOJI', allTargets, post, post.viewers)
                    }
                    if(targetType == 'commenters'){
                        let post = await Publication.findOne({ uid: params.aboutObj.uid }).populate('comments')
                        post.comments.forEach((com, i) => {
                            if(allTargets.indexOf(com.author.uid) == -1)
                                allTargets.push(com.author.uid)
                        })
                    }
                    // if(targetType == 'signed'){
                    //     let comment = await Comment.findById( params.whatObj.uid ).populate('signed')
                    //     allTargets.push(comment.author.uid)
                    // }
                }

                if(params.verb == 'NEW_ROLE_ACCEPTED'){
                    if(targetType == 'onPageUid')
                    allTargets = [params.whatObj.forPageUid]
                }
                console.log("getTargets res", params.verb, targetType, allTargets)
            //})
            return allTargets

            
        }
    },

    deleteCascade : {
        page : async function(params, req){
            console.log("--deleteCascade page", params.obj.uid)
            let pageObj = null
            if(params.uid === null){
                console.log("load page ?")
                pageObj = await Page.findOne( { uid: params.uid, owner: req.user._id } )
                                    .populate("author").populate("signed").populate("target")
            }else{ pageObj = params.obj }
            
            //récupère toutes les publications de cette page
            console.log("delete publications", params.obj.uid)
            let stream = await Publication.find({ $or: [{ author: pageObj._id },
                                                        { signed: pageObj._id },
                                                        { target: pageObj._id }] })
                                            .populate({
                                                path: 'comments',
                                                populate: { path: 'answers' }
                                            })

            console.log("delete notification about post")
            //supprime chacune de ces publications
            if(typeof stream !== 'undefined')                                               
            await Promise.all(stream.map(async (post, i) => {
                await core.deleteCascade.publication({ obj: post })
            }))
            
            await core.deleteCascade.notification({ uid: pageObj.uid })
            await core.deleteCascade.survey(pageObj)

            await pageObj.remove()
        },
        survey : async function(params){
            console.log("--deleteCascade survey")
            if(params.type == "assembly"){
                let surveys = await Survey.find( { roomUid: params.uid } )
                await Promise.all(surveys.map(async (survey, s) => {
                    let post = await Publication.findOne( { uid: survey.postUid } )
                    await core.deleteCascade.publication({ obj: post })
                    await survey.remove()
                }))
            }            
        },
        publication : async function(params){
            console.log("--deleteCascade publication")
            let postObj = null
            if(params.uid === null){
                postObj = await Publication.findOne( { uid: params.uid } )
                                           .populate("comment")
            }else{ postObj = params.obj }
            if(postObj == null ) return 

            console.log("delete comments about post")
            //supprime tous les commentaires liés à cette publication           
            if(typeof postObj.comments !== 'undefined')                                         
            await Promise.all(postObj.comments.map(async (com, i) => {
                await core.deleteCascade.comment({ obj: com })
            }))

            //supprime les notifications liées à cette pubilcation
            await core.deleteCascade.notification({ uid: postObj.uid })
            await postObj.remove()
        },
        comment : async function(params){
            console.log("--deleteCascade coment")
            let comObj = null
            if(params.uid === null){
                comObj = await Publication.findOne( { uid: params.uid } )
                                           .populate("comment")
            }else{ comObj = params.obj }

            //supprime toutes les réponses liées à ce commentaire      
            console.log("delete anwsers about comment")         
            if(typeof comObj.answers !== 'undefined')
            await Promise.all(comObj.answers.map(async (com, i) => {
                await core.deleteCascade.comment({ obj: com })
            }))

            //supprime les notifications liées à ce commentaire
            await core.deleteCascade.notification({ uid: comObj.uid })
            await comObj.remove()
        },
        notification : async function(params){
            console.log("--deleteCascade notif")
            await Notification.deleteMany({ $or: [{ whatObj:  params.uid },
                                                  { aboutObj: params.uid }] })
        },
    }
}
module.exports = core;