const unless = require('express-unless');

const isInstanceInitialized = function(req, res, next) {  
  if (!global.store.isInstanceInitialized) {
    throw new Error('Cannot perform request, instance is not initialized yet.');
  }
  
  next();
};

isInstanceInitialized.unless = unless;

module.exports = isInstanceInitialized;