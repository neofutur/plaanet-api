const UserService = require("../../services/user");
const AuthTokenService = require("../../services/authToken");
const CronTokenService = require("../../services/cronToken");

module.exports = async function authAdminMiddleware(req, _, next) { 
  try {
    const cronTokenService = new CronTokenService();
    const authTokenService = new AuthTokenService();
    const userService = new UserService();

    const token = await cronTokenService.getTokenFromRequest(req);
    if (!token) {
      throw new Error('Cron token is missing.');
    }

    const isTokenValid = await authTokenService.isTokenValid(token);
    if (!isTokenValid) {
      throw new Error('Cron token has been tampered or not created by us.');
    }

    const tokenPayload = await authTokenService.getTokenPayload(token);
    if (!tokenPayload) {
      throw new Error('Cron token is invalid. Please generate a new token.');
    }

    const user = await userService.getUserById(tokenPayload._id);
    if (!user) {
      throw new Error('User specified in cron token does not exists.');
    }

    const userIsAdmin = await userService.isUserAdmin(user._id);
    if (!userIsAdmin) {
      throw new Error('User specified in cron token is not an admin.')
    }

    // Attach user to request object.
    req.user = user;

    // Allow request if user is set.
    next();
  } catch (err) {
    next(err);
  }
};