module.exports.AuthenticationError = require('./AuthenticationError');
module.exports.CredentialsError = require('./CredentialsError');
module.exports.EntityDoesNotExistsError = require('./EntityDoesNotExistsError');
module.exports.ValidationError = require('./ValidationError');
module.exports.UsernameTakenError = require('./UsernameTaken')