const express = require("express");
const bcrypt = require("bcryptjs");

const config = require('config');
const jwt = require('jsonwebtoken');
const fs = require('fs');

const { ValidationError, UsernameTakenError, AuthenticationError, CredentialsError } = require("../core/errors");
const Config = require('../core/config');
const core = require('../core/core');

const UserService = require("../services/user");
const PageService = require("../services/page");
const AuthTokenService = require("../services/authToken");
const InstanceService = require("../services/instance");

const { User, validate } = require('../models/User');
const Page = require('../models/Page');

const cookieConfig = {
    httpOnly: true, // to disable accessing cookie via client side js
    secure: Config.COOKIES_SECURE, // to force https (if you use it)
    maxAge: 1000000000, // ttl in ms (remove this option and cookie will die when browser is closed)
    signed: true, // if you use the secret with cookieParser,
    sameSite: Config.COOKIES_SAME_SITE, // to allow setting cross-domain cookies
};

const router = express.Router();

router.get('/whoami', async (req, res, next) => {
  try {
    console.log('DEBUG: /auth/whoami called ...');
    const authTokenService = new AuthTokenService();

    const token = await authTokenService.getTokenFromRequest(req);
    if (!token) {
      return res.json({
        success: true,
        data: {
          isLoggedIn: false,
          role: 'guest',
        },
      });
    }

    const { iat, ...payload } = await authTokenService.getTokenPayload(token);
    return res.json({
      success: true,
      data: {
        isLoggedIn: true,
        ...payload,
      },
    });
  } catch (err) {
    next(err);
  }
});

router.post('/register', async (req, res, next) => {
    try{
        console.log("DEBUG: /auth/register called ...");

        const userService = new UserService();
        const instanceService = new InstanceService();

        // validate the request body first
        const validation = validate(req.body);
        if (validation.error) {
            throw new ValidationError('Cannot validate body.', validation.error);
        }

        const { username, password, referrerUid } = req.body;

        const user = await userService.getUserByName(username);
        if (user) {
            throw new UsernameTakenError('Username is already in use.');
        }
    
        const localNode = await instanceService.getLocalInstance();
        const nbUsers = await userService.countUsers();

        //create new user
        const newUser = new User({
            name: username,
            password: password,
            referrerUid: referrerUid || null,
            isActive: false,
            isAdmin: (nbUsers == 0), //if it is the first user created, it is the ADMIN
        });

        // Generate a salt to prevent rainbow table attacks.
        const salt = await bcrypt.genSalt(10);

        newUser.password = await bcrypt.hash(newUser.password, salt);

        const page = new Page();
        page.name = newUser.name;
        page.type = "user";
        page.owner = newUser.id;
        page.slug = core.getSlug(page.name);
        page.slug = localNode.name + ":" + page.slug;
        page.uid = localNode.name + ":" + page.id;
        page.coordinates = localNode.position.coordinates;
        page.blacklist = [];
        page.follows = [];
                
        const now = Date.now();
        page.created = now;
        page.dateLastPosition = now;
        page.dateReadNotif = now;
        page.dateReadPrivate = now;
        
        let id = { uid: page.uid, name: page.name }
        page.roles =        { 'admin': [id], 'moderator': [id], 'editor': [id] }
        page.pendingRoles = { 'admin': [],   'moderator': [],   'editor': [] }
        page.inviteRoles =  { 'admin': [],   'moderator': [],   'editor': [] }

        await page.save()

        newUser.pages.push(page);
        await newUser.save();

        localNode.nbUserGlobal += 1
        await localNode.save();

        if (referrerUid) {
            const nodeName = referrerUid.split(':')[0];
            const referrerId = referrerUid.split(':')[1];

            if (referrerId.length < 24) {
                throw new ValidationError('Referrer UID must be a 24 bytes string.', referrerUid);
            }

            const localNode = await instanceService.getLocalInstance(); 
            if (nodeName === localNode.name) {
                const referrerUser = await userService.getUserById(referrerId);
                referrerUser.referredCount = referrerUser.referredCount + 1 || 1;
                await referrerUser.save();

                console.log(`DEBUG: Incremented referredCount for user (id: ${referrerUser._id}): ${referrerUser.name}`);
            } else {
                // TODO: Broadcast to /broadcast/increment-referrer-count
                // on nodeName instance
            }
        }

        fs.copyFile(
            `${Config.PUBLIC_FOLDER_PATH}/default-avatar.png`, 
            `${Config.PUBLIC_FOLDER_PATH}/uploads/avatar/${page.uid}.png`, 
            (err) => {
                if (err) throw err;
                console.log(`DEBUG: File copy: 'default-avatar.png' -> ${Config.PUBLIC_FOLDER_PATH}/uploads/avatar/${page.uid}.png`);
            }
        );

        console.log(`DEBUG: Registered a new user (id: ${newUser._id}): ${newUser.name}`);

        return res.json({
            success: true,
            data: {
                user: newUser,
            }
        });
    } catch(err) {
        console.log("DEBUG: Cannot register an user. Error:", err)
        next(err);
    }

});

router.post('/login', async (req, res, next) => {
    try {
        const userService = new UserService();
        const pageService = new PageService();

        console.log("DEBUG: /auth/login called ...");
        const { body, query } = req;
        const grantType = query && query.grant_type;
        const setCookie = grantType !== 'token';

        const username = body.name;
        const password = body.password;

        if (!username || !password) {
            throw new CredentialsError(
                'auth.emptyCredentials',
                'Credentials are missing (either name, password or both).'
            );
        }

        const user = await userService.getUserByName(username);
        
        // Do not say username is bad, else we're making bruteforcing of the API easier.
        if (!user) {
            throw new CredentialsError(
                'auth.invalidCredentials',
                'Credentials are invalid.'
            );
        }

        const isPasswordValid = await userService.compareUserPassword(user._id, password);
        if (!isPasswordValid) {
            throw new CredentialsError(
                'auth.invalidCredentials',
                'Credentials are invalid.'
            );
        }

        if(body.mobileAppVersion != null && body.mobileAppName != null){
            user.useMobileApp = body.mobileAppName + " - " + body.mobileAppVersion
            user.save()
            console.log("User using mobile app : ", user.name, user.useMobileApp)
        }

        const token = await userService.generateTokenForUser(user._id);
        //const userPage = await pageService.getPageForUser(user._id);

        if (setCookie) {
            //console.log('DEBUG: Setting `plaanet_access_token` cookie');
            //console.log(`   DEBUG: Cookie Config:`, cookieConfig);
            res.cookie('plaanet_access_token', token, cookieConfig);
        }

        return res.json({
            success: true,
            data: {
                token: grantType === 'token' ? token : undefined,
                user: user,
            }
        });
    } catch (err) {
        next(err);
    }
});

router.post('/logout', (req, res) => {
    console.log("-------------------------")
    console.log("/auth/logout")
    const token = req.signedCookies['plaanet_access_token'];
    res.cookie('plaanet_access_token', token, {expires: Date.now(), maxAge: 0})
    //cookie('plaanet_access_token', {expires: Date.now()})
    res.send({  error: false })

});


module.exports = router;