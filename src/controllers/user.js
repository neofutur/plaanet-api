const axios = require('axios').default;
const express = require('express');

const auth = require("../core/middleware/auth");
const broadPage = require('../core/broadcast-page');
const EntityDoesNotExistsError = require('../core/errors/EntityDoesNotExistsError');
const ValidationError = require('../core/errors/ValidationError');

const { User } = require('../models/User');
const Page = require('../models/Page');

const UserService = require("../services/user");
const PageService = require("../services/page");
const NotificationService = require("../services/notification");
const PublicationService = require("../services/publication");
const InstanceService = require("../services/instance");

const router = express.Router();

router.get('/profil', auth, async (req, res) => {
  console.log("-------------------------")
  console.log("/user/profil")
  const user = await User.findById(req.user._id)
    .select("-password")
    .populate('pages')


  const userPage = await Page.findOne({ owner: req.user._id, type: 'user' })
  console.log("found user ?", user != null)
  if (user == null || userPage == null) {
    res.json({
      error: true,
      user: null,
      userPage: null,
    });
    return;
  }

  res.json({
    error: false,
    user: user,
    userPage: userPage
  });
})

router.get('/context', auth, async (req, res, next) => {
  try {
    console.log("DEBUG: /user/context called ...");
    const userService = new UserService();
    const publicationService = new PublicationService();
    const pageService = new PageService();

    const { user } = req;

    const userObj = await userService.getUserById(user._id);
    const userPage = await pageService.getPageForUser(user._id);
    const memberPages = await pageService.getMemberPages(userPage.uid);
    const firstPost = await publicationService.getFirstPostForPage(userPage._id);

    if (userObj == null || userPage == null) {
      throw new EntityDoesNotExistsError('User does not exists.', user._id);
    }

    let follows = await broadPage.serializeList(userPage.follows, res);
    userPage.follows = follows;

    return res.json({
      success: true,
      data: {
        isNewUser: firstPost == null,
        user: userObj,
        userPage: userPage,
        memberPages: memberPages
      }
    });
  } catch (err) {
    next(err);
  }
})

router.get('/pages', auth, async (req, res, next) => {
  try {
    console.log("DEBUG: /user/pages called ...");

    const { user } = req;
    const pageService = new PageService();

    const pages = await pageService.getPagesForUser(user._id);

    return res.json({
      success: true,
      data: {
        pages,
      },
    });
  } catch (err) {
    next(err);
  }
})

router.get('/notifications', auth,  async (req, res, next) => {
  try {
    console.log("-------------------------")
    console.log("/user/notifications")
    const { user } = req;
    const pageService = new PageService();
    const notificationService = new NotificationService();

    const userPage = await pageService.getPageForUser(user._id);
    const notifications = await notificationService.getNotificationsForUser(userPage.uid);

    console.log('notifications data.length:', notifications.length);

    return res.json({
      success: true,
      data: {
        notifications,
      },
    });
  } catch(err) {
    next(err);
  }
})

// TODO: Find a way to authenticate request.
router.get('/profile/:userUid', async (req, res, next) => {
  try {
    const { userUid } = req.params;
    console.log(`DEBUG: /user/profile/${userUid} called ...`);

    if (!userUid) {
      throw new ValidationError('Missing userId url param.');
    }

    const instanceService = new InstanceService();
    const userService = new UserService();

    const nodeName = userUid.split(':')[0];
    const userId = userUid.split(':')[1];
    if (userId.length !== 12 && userId.length !== 24) {
      throw new ValidationError('User ID must be a single String of 12 bytes or a string of 24 hex characters', userId)
    }
    
    const localNode = await instanceService.getLocalInstance(); 
    if (nodeName === localNode.name) {
      const user = await userService.getRedactedUserById(userId);
      if (!user) {
        throw new EntityDoesNotExistsError('User does not exists', userId);
      }

      /**
       * TODO: Lot of code that can fo into the utils here
       */

      return res.json({
        success: true,
        data: {
          profile: {
            _id: user._id,
            uid: `${localNode.name}:${user._id}`,
            slug: user.pages[0].slug,
            name: user.name,
            displayName: user.pages[0].name,
            avatarUrl: `${localNode.url}:${localNode.port}/uploads/avatar/${user.pages[0].uid}.png`,
          },
        }
      });
    }

    // Node is remote, ask the remote node :)
    const remoteNode = await instanceService.getInstanceByNameWithNodeKey(nodeName);
    const remoteNodeUrl = `${remoteNode.url}:${remoteNode.port}`;
    
    console.log(`DEBUG: Requesting user profile from instance ${remoteNode.name}:`, remoteNodeUrl);

    const { data } = await axios.get(`/user/profile/${userUid}`, {
      baseURL: remoteNodeUrl,
      headers: {
        'X-Auth-Token': localNode.nodeKey,
      }
    });

    if (!data.success) {
      return next(data.error);
    }

    return res.json({
      success: true,
      data: {
        profile: data.data.profile,
      }
    }); 
  } catch (err) {
    next(err);
  }
})

module.exports = router;