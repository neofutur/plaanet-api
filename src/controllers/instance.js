var fs = require('fs');
const express = require('express');
const config = require('config');
const bcrypt = require('bcryptjs');
const axios = require('axios');
const slugify = require('slugify');
//const jwt = require('jsonwebtoken');

const auth = require('../core/middleware/auth-anonymous');
const authAdmin = require("../core/middleware/auth-admin");
const core = require('../core/core');
const { User } = require('../models/User');
const Publication = require('../models/Publication');
const Notification = require('../models/Notification');
const Comment = require('../models/Comment');
const Instance = require('../models/Instance');
const StateHistory = require('../models/StateHistory');
const Page = require('../models/Page');
const StatUrl = require('../models/StatUrl');
const InstanceService = require('../services/instance');

const router = express.Router()

router.get('/exists'/*, auth*/,  async (req, res) => {
    console.log("-------------------------")
    console.log("/instance/exists")
    
    //const url = config.get('instance_url')
    //const port = config.get('instance_port')
    try{
        const instance = await Instance.findOne({ isLocal: true }).select("-nodeKey") // url: url, port: port }) 
        console.log(instance)
        res.json({ error: false, instance: (instance != null) });
    }catch(e){
        console.log("error", e)
        res.json({ error: false, instance: false, 
                   errorMsg: 'No instance found in this server' })
    }
})

router.get('/get-network'/*, auth*/,  async (req, res) => {
    console.log("-------------------------")
    console.log("/instance/get-network")
    
    //const url = config.get('instance_url')
    //const port = config.get('instance_port')
    try{
        const network = await Instance.find().select("-nodeKey")
        const localNode = await Instance.findOne({ isLocal: true }).select("-nodeKey")
        //console.log(network)
        res.json({ error: false, network: network, localNode: localNode });
    }catch(e){
        console.log("error", e)
        res.json({ error: true, network: false, localNode: null,
                   errorMsg: 'No instance found in this server' })
    }
})


router.post('/enter-network'/*, auth*/,  async (req, res) => {
    console.log("-------------------------")
    console.log("/instance/enter-network")
    try{
        console.log("req.body.url:", req.body.instance.url, "port:", req.body.instance.port)
        const instance = await Instance.findOne({ url: req.body.instance.url, port: req.body.instance.port }) // url: url, port: port }) 
        console.log("instance", instance)
        if( instance != null){
            res.json({  error: true,
                        errorMsg: 'This node is already registred on the network' })
        }else{
            const instance = new Instance(req.body.instance)
            instance.isLocal = false
            instance.save()
            
            // Update CORS allowed origins cache
            global.store.allowedOrigins.push(`https://${new URL(instance.url).hostname}`);

            const me = await Instance.findOne({ isLocal: true }) // url: url, port: port }) 
            res.json({  error: false,
                        nodeKey: me.nodeKey,
                        errorMsg: 'This node is now registred on the network' })
        }
        
    }catch(e){
        console.log("error", e)
        res.json({ error: true, network: false, 
                   errorMsg: 'No instance found in this server' })
    }
})


router.post('/init'/*, auth*/, async (req, res) => {
    const { isInstanceInitialized, initToken } = global.store;
    const { force } = req.query;
    const tokenHeader = req.header('x-init-token');

    if (isInstanceInitialized && !force) {
        console.log('Tried to /init an already initialized instance. Pass ?force=true to force.')
        return res.status(403).json({
            success: false,
            error: {
                code: 'EInstanceAlreadyInitialized',
                message: 'Instance is already initialized.',
            },
        });
    }

    if (!isInstanceInitialized && initToken !== tokenHeader) {
        console.log(`initToken: ${initToken} - X-Init-Token: ${tokenHeader}`)
        return res.status(403).json({
            success: false,
            error: {
                code: 'EInvalidToken',
                message: 'Initialization token is invalid.',
            },
        });
    }

    console.log("-------------------------")
    console.log("/instance/init")
    try{

        // REMOVE ALL DOCUMENTS
        console.log("remove", await User.countDocuments(), "Users")
        await User.remove()
        console.log("remove", await Publication.countDocuments(), "Publication")
        await Publication.remove()
        console.log("remove", await StateHistory.countDocuments(), "StateHistorys")
        await StateHistory.remove()
        console.log("remove", await Comment.countDocuments(), "Comment")
        await Comment.remove()
        console.log("remove", await Notification.countDocuments(), "Notification")
        await Notification.remove()
        console.log("remove", await Page.countDocuments(), "Page")
        await Page.remove()
        

        //CREATE THE INSTANCE (in bdd)
        console.log("let's create instance, name:", req.body.name)
        await Instance.remove()
        let instanceName = req.body.name != null ? req.body.name : "alpha"
        const nodeUrl = req.body.url || config.get('instance_url')
        const nodePort = req.body.port || config.get('instance_port')
        const nodeSocketPort = req.body.portSocket || config.get('instance_port_socket')

        // const isNodeReachable = await isReachable([nodeUrl, nodePort].join(':'))
        // if (!isNodeReachable) {
        //     throw new Error('Cannot reach node. Make sure .url & .port are valid and accessible from the internet (i.e. not a local or non-routable IP address)')
        // }

        let nodeKey = await bcrypt.hash(instanceName, 10)
        
        const instance = new Instance({
            name: slugify(instanceName, {
                lower: true,
                remove: /[*+~.()'"!:@]/g,
            }),
            nodeKey: nodeKey,
            url: nodeUrl,
            port: nodePort,
            portSocket: nodeSocketPort,
            city: req.body.city,
            state: req.body.state,
            position: { type: "Point", coordinates : [parseFloat(req.body.lng), parseFloat(req.body.lat)] },
            clientUrl: req.body.clientUrl,
            clientPort: req.body.clientPort,
            nbUserGlobal: 0,
            isLocal: true
        })

        await instance.save();
 
        let rootNode = config.get('root_node')
        core.get(rootNode.url + ":" + rootNode.port + '/broadcast/get-network', {},
            function(res){ //then
                console.log("network ?", res.data)
                res.data.network.forEach(node =>{
                    console.log(node.url+":"+node.port != instance.url+":"+instance.port)
                    if(node.url+":"+node.port != instance.url+":"+  instance.port){
                        console.log("go enter network", node)
                        console.log("my instance", instance)
                        
                        Instance.findOne({ url: node.url, port: node.port })
                        .then(iExists => {
                            console.log("iExists", iExists)
                            if( iExists == null ){
                                const i = new Instance(node)
                                i.isLocal = false
                                i.save()
                            }else{
                                console.log("this instance is already registred")
                            }

                            axios.post(node.url + ':' + node.port +'/instance/enter-network', {
                                instance: instance 
                            }).then( res => {
                                if(res.data.nodeKey != null){
                                    let n = Instance.findOne({ url: node.url, port: node.port })
                                    n.nodeKey = res.data.nodeKey
                                    n.save()
                                }
                                console.log("success", node.url + ':' + node.port +'/instance/enter-network', res.data)
                            }).catch( res => {
                                console.log("error", node.url + ':' + node.port +'/instance/enter-network', res.data)
                            })
                        })
                        
                    }else{
                        console.log("this instance is already registred")
                    }
                });
            }, function(res){})
     
    
      console.log(">>> INIT INSTANCE OK");

        // Set global state.
        global.store.isInstanceInitialized = true;
        global.store.initToken = null;

      return res.json({
        error: false,
        instance: instance,
      });
    } catch(e) {
      console.log("error", e)
      res.json({ error: true, instance: null })
    }
})



router.post('/find-closest-node', auth,  async (req, res) => {
    console.log("-------------------------")
    console.log("/instance/find-closest-node")
    
    //const url = config.get('instance_url')
    //const port = config.get('instance_port')
    try{
        const node = await Instance.findOne({
            'position' : {
                '$near' : {
                    $geometry: {
                        type: "Point" ,
                        coordinates: [ parseFloat(req.body.lng) , parseFloat(req.body.lat) ]
                    },
                    //$minDistance: 0
                }
                    
            }
        }).select("-nodeKey")
        //console.log(network)
        res.json({ node: node });
    }catch(e){
        console.log("error", e)
        res.json({ error: true, node: false, 
                   errorMsg: 'No instance found in this server' })
    }
})


router.post('/randomize', auth,  async (req, res) => {
    console.log("-------------------------")
    console.log("/instance/randomize")

    const i = await Instance.findOne({ isLocal:true })

    for(var x=0; x<100; x++){
        //create new user
        user = new User({
            name: "User"+x,
            password: "anonymous",
            //email: req.body.email,
            isActive: true,
            isAdmin: false //if it is the first user created, it is the ADMIN
        });

        let lat = -1 + (Math.random()*7) 
        let lng = 43 + (Math.random()*5) 
        let coordinates = [lat, lng]

        let page = new Page()
        page.name = user.name
        page.type = "user"
        page.owner = user.id
        page.slug = core.getSlug(page.name)
        page.slug = i.name + ":" + page.slug
        page.uid = i.name + ":" + page.id
        page.coordinates = coordinates
        page.blacklist = []
        page.follows = []
        
        let now = new Date()
        page.created = now
        page.dateLastPosition = now
        page.dateReadNotif = now
        page.dateReadPrivate = now
        
        await page.save()

        const publicPath = config.get('public_folder_path') || './public';
        fs.copyFile(`${publicPath}/default-avatar.png`, 
                    `${publicPath}/uploads/avatar/` + page.uid + '.png', 
                    (err) => {
            if (err) throw err;
            //console.log('source.txt was copied to ' + page.uid + '.png');
        });

        user.pages.push(page)
        await user.save();

        i.nbUserGlobal += 1
        await i.save()

        console.log('1 user more : User' + x);
    }
})


router.post('/init-day-stat-url',  async (req, res) => {
    console.log("-------------------------")
    console.log("/instance/init-day-stat-url")

    let instanceService = new InstanceService()
    instanceService.initDayStat()

    return res.json({ error: false })
})

router.post('/inc-stat-url',  async (req, res) => {
    //console.log("-------------------------")
    //console.log("++++++++ /instance/inc-stat-url", req.body.url)

    if(req.body.url == null) return res.json({ error: true })

    let date1D = new Date()
    date1D.setDate(date1D.getDate() - 1)

    let stat = await StatUrl.findOne({ url: req.body.url, 
                                        clientDomaineName: req.body.domaineName,
                                        date: { '$gt': date1D } })

    if(stat == null){
        console.log("+ Error: no state inited for current date")
        console.log("+ Try to init it")
        let instanceService = new InstanceService()
        instanceService.initDayStat()
        console.log("+ State is now inited for current date, and checked for 50 days")
        return res.json({ error: false })
    }else{
        let date = new Date()
        stat.lastDate = date
        stat.count = stat.count + 1
        stat.save()
        //console.log("++++++++ IncStat", req.body.url, stat.count)
        return res.json({ error: false })
    }
})

router.get('/get-stat-url-local/:domaineName', authAdmin,  async (req, res) => {
    console.log("-------------------------")
    console.log("/instance/get-stat-ur-locall", req.params.domaineName)

    let date30D = new Date()
    date30D.setDate(date30D.getDate() - 30)

    let stats = await StatUrl.find({ clientDomaineName: req.params.domaineName, 
                                     date: { '$gt': date30D } })
                                .sort({'inx':-1})

    console.log("/instance/get-stat-url-local nbStats:", stats.length)
    
    //stats = stats.reverse()

    return res.json({ error: false, stats: stats })
})

router.get('/get-stat-url-global/:domaineName', authAdmin,  async (req, res) => {
    console.log("-------------------------")
    console.log("/instance/get-stat-url-global", req.params.domaineName)

    try{ //broadcast request (to each instance of the network)
        //let nbPerPage = 5
        core.broadcaster.post('/broadcast/get-stat-url-global', 
            {   domaineName: req.params.domaineName,
            }, //params
            function(dataRes){ // == then (after response from all instances)
                //if network sent data
                if(dataRes != null){
                    //concat count values
                    let urlsChecked = []
                    let finalRes = []
                    let tempRes = []
                    dataRes.forEach((stat1)=>{
                        let key = stat1.url+stat1.date.substr(0, 10)
                        if(urlsChecked.indexOf(key) == -1){
                            //console.log(">>> key NOT exists", key)
                            // let count = stat1.count
                            // dataRes.forEach((stat2)=>{
                            //     if(stat1.url == stat2.url){
                            //         //console.log(">>> ", stat1.url, stat1.date.substr(0, 10))
                            //         if(stat1.date.substr(0, 10) == stat2.date.substr(0, 10)){
                            //             //console.log("inc count", count, stat2.count)
                            //             count += stat2.count
                            //         }
                            //     }
                            // })
                            // let s = stat1
                            // s.count = count
                            tempRes[key] = stat1
                            //finalRes.push(s)
                            urlsChecked.push(key)
                            //console.log("--- Count checked", count, s.url)
                        }else{
                            //console.log(">>> key exists", key, tempRes[key].count, "+", stat1.count)
                            tempRes[key].count += stat1.count
                        }
                    })

                    //console.log(">>> tempRes.forEach")
                    urlsChecked.forEach((key)=>{
                        //console.log(">>> tempRes.forEach", key, tempRes[key])
                        finalRes.push(tempRes[key])
                    })
                    //sort all res by "created" date
                    //finalRes = _.sortBy(finalRes, 'created').reverse()
                    console.log("SUCCESS /broadcast/get-stat-url-global", dataRes.length)
                    res.json({  error: false, 
                                stats: finalRes })
                }else{
                    console.log("error 1 : dataRes == null")
                    res.json({ error: true, stats: null, 
                            errorMsg: 'No stream found in this bdd' })
                }
                    
            },function(result){ // == catch
                console.log("error 2 : fCatch", result)
            },  
            'stats') //dataRes[id] to concat for each response after forward
    }catch(e){
        console.log("error 3")
        res.json({ error: true, stats: null, e:e })
    }
})


router.get('/last-mobile-app-version/:appName',  async (req, res) => {
    console.log("-------------------------")
    console.log("/instance/last-mobile-app-version", req.params)
    
    try{ //broadcast request (to each instance of the network)
        let rootNode = config.get('root_node')
        core.get(rootNode.url + ":" + rootNode.port +'/broadcast/last-mobile-app-version/'+req.params.appName, 
            {  }, //params
            async function(dataRes){ // == then (after response from all instances)
                //if network sent data
                if(dataRes.data.error != true){
                    res.json({  error: false, app: dataRes.data.app })
                }else{
                    console.log("error 1 : dataRes == null")
                    res.json({ error: true, appVersion: null })
                }
                    
            },function(result){ // == catch
                console.log("error 2 : fCatch", result)
                //res.json({ error: true, wallets: null, 
                           //errorMsg: 'An error occured', result: result })
            })
    }catch(e){
        console.log("error 3")
        res.json({ error: true, appVersion: null, e:e })
    }
})



module.exports = router;