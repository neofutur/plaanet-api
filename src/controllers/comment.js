var express = require("express")
var router = express.Router()
var app = express()
var cors = require('cors')

const auth = require("../core/middleware/auth");
const bcrypt = require("bcryptjs");
const { User, validate } = require('../models/User')
const Instance = require('../models/Instance')
const Wallet = require('../models/Wallet')
const Transaction = require('../models/Transaction')
const Publication = require('../models/Publication')
const Page = require('../models/Page')
const Comment = require('../models/Comment')

const config = require('config');
const _ = require('underscore');
const jwt = require('jsonwebtoken');

const axios = require('axios');
const core = require('../core/core');

router.post('/send-comment', auth,  async (req, res) => {
    console.log("-------------------------")
    console.log("/comment/send-comment", req.body)
    
    try{ //broadcast request (to each instance of the network)
        let nodePath = await core.getNodePath(req.body.postUid)
        let authPage = await Page.findOne( { owner: req.user._id, type:'user' } )
        let authorPage = {  uid: authPage.uid, 
                            name: authPage.name, 
                        }
        console.log("try contact", nodePath + '/broadcast/comment-send-comment', req.body.postUid)
        core.post(nodePath + '/broadcast/comment-send-comment', 
            {   postUid : req.body.postUid,
                commentStr: req.body.commentStr,
                parentCommentId: req.body.parentCommentId,
                authorPage: authorPage
            }, //params
            function(dataRes){ // == then (after response from all instances)
                //console.log("dataRes", dataRes.data)
                //if network sent data
                if(dataRes != null && dataRes.data.error == false){
                    let newComment = dataRes.data.newComment
                    newComment.isNew = true
                    res.json({  error: false, newComment: newComment })
                }else{
                    console.log("error 1 : dataRes == null")
                    res.json({ error: true, newComment: null, 
                               errorMsg: 'No comment found in this bdd' })
                }
                    
            },function(result){ // == catch
                console.log("error 2 : fCatch", result)
            })
    }catch(e){
        console.log("error 3", e)
        res.json({ error: true, newComment: null, e:e })
    }
})

router.post('/edit-comment', auth,  async (req, res) => {
    console.log("-------------------------")
    console.log("/comment/edit-comment", req.body)
    
    /**
    let userPage = await Page.findOne( { owner: req.user._id, type: "user" } )
    let comment = await Comment.findOne( { _id: req.body.commentId } )
    console.log("set text ?", comment.author.uid, userPage.uid)
    if(comment.author.uid == userPage.uid){
        console.log("set text1", comment.text)
        comment.text = req.body.text
        console.log("set text2", comment.text)
        await comment.save()
    }else{
        res.json( { error: true, commentText: "" } );
    }

    res.json( { error: false, commentText: comment.text } );
    */

    /** */

    if(req.body.postUid == null) return res.json({ error: true, commentText: '' })

    try{ //broadcast request (to each instance of the network)
        let nodePath = await core.getNodePath(req.body.postUid)
        let authPage = await Page.findOne( { owner: req.user._id, type:'user' } )
        let authorPage = {  uid: authPage.uid, 
                            name: authPage.name, 
                        }
        console.log("try contact", nodePath + '/broadcast/comment-edit', req.body.commentId)
        core.post(nodePath + '/broadcast/comment-edit', 
            {   //postUid : req.body.postUid,
                commentId: req.body.commentId,
                text: req.body.text,
                authorPage: authorPage
            }, //params
            function(dataRes){ // == then (after response from all instances)
                //console.log("dataRes", dataRes.data)
                //if network sent data
                if(dataRes != null && dataRes.data.error == false){
                    let commentText = dataRes.data.commentText
                    res.json({  error: false, commentText: commentText })
                }else{
                    console.log("error 1 : dataRes == null")
                    res.json({ error: true, commentText: null, 
                               errorMsg: 'No comment found in this bdd' })
                }
                    
            },function(result){ // == catch
                console.log("error 2 : fCatch", result)
            })
    }catch(e){
        console.log("error 3", e)
        res.json({ error: true, commentText: null, e:e })
    }
})

router.post('/delete-comment', auth,  async (req, res) => {
    console.log("-------------------------")
    console.log("/comment/delete-comment", req.body)
    
    try{ // to the instance of the post
        let nodePath = await core.getNodePath(req.body.parentPostUid)
        let authPage = await Page.findOne( { owner: req.user._id, type:'user' } )
        //let authorPageUid = authPage.uid
        let authorPage = {  uid: authPage.uid, 
                            name: authPage.name, 
                        }
        console.log("try contact", nodePath + '/broadcast/comment-delete', req.body.commentId)
        core.post(nodePath + '/broadcast/comment-delete', 
            {   commentId: req.body.commentId,
                authorPage: authorPage
            }, 
            function(dataRes){ // == then (after response from all instances)
                //console.log("dataRes", dataRes.data)
                //if network sent data
                if(dataRes != null && dataRes.data.error == false){
                    let comment = dataRes.data.comment
                    res.json({  error: false })
                }else{
                    console.log("error 1 : dataRes == null", dataRes.data)
                    res.json({ error: true })
                }
                    
            },function(result){ // == catch
                console.log("error 2 : fCatch", result)
            })
    }catch(e){
        console.log("error 3", e)
        res.json({ error: true, e:e })
    }

})


router.post('/send-emoji', auth,  async (req, res) => {
    console.log("-------------------------")
    console.log("/comment/send-emoji", req.body)
    
    try{ //broadcast request to the instance of the post
        let nodePath = await core.getNodePath(req.body.postUid)
        let authPage = await Page.findOne( { owner: req.user._id, type:'user' } )
        //let authorPageUid = authPage.uid
        let authorPage = {  uid: authPage.uid, 
                            name: authPage.name, 
                        }
        console.log("try contact", nodePath + '/broadcast/comment-send-emoji', req.body.commentId)
        core.post(nodePath + '/broadcast/comment-send-emoji', 
            {   commentId: req.body.commentId,
                emoji: req.body.emoji,
                postUid: req.body.postUid,
                parentCommentId: req.body.parentCommentId,
                authorPage: authorPage
            }, 
            function(dataRes){ // == then (after response from all instances)
                //console.log("dataRes", dataRes.data)
                //if network sent data
                if(dataRes != null && dataRes.data.error == false){
                    let comment = dataRes.data.comment
                    res.json({  error: false, comment: comment })
                }else{
                    console.log("error 1 : dataRes == null", dataRes.data)
                    res.json({ error: true, comment: null, 
                               errorMsg: dataRes.data.msg })
                }
                    
            },function(result){ // == catch
                console.log("error 2 : fCatch", result)
            })
    }catch(e){
        console.log("error 3", e)
        res.json({ error: true, comment: null, e:e })
    }

})


router.get('/get-conv', auth,  async (req, res) => {
    console.log("-------------------------")
    console.log("/comment/get-conv", req.query)
    
    try{
        let nodePath = await core.getNodePath(req.query.postUid)
        console.log("try contact", nodePath + '/broadcast/comment-get-conv', req.query.postUid)
        core.get(nodePath + '/broadcast/comment-get-conv', 
            { postUid: req.query.postUid,
              parentCommentId: req.query.parentCommentId
            }, 
            function(dataRes){ // == then (after response from all instances)
                //console.log("dataRes", dataRes.data)
                //if network sent data
                if(dataRes != null && dataRes.data.error == false){
                    let comments = dataRes.data.comments
                    res.json({  error: false, comments: comments })
                }else{
                    console.log("error 1 : dataRes == null", dataRes.data)
                    res.json({ error: true, comments: [], 
                               errorMsg: dataRes.data.msg })
                }
                    
            },function(result){ // == catch
                console.log("error 2 : fCatch", result)
            })
    }catch(e){
        console.log("error 3", e)
        res.json({ error: true, comments: [], e:e })
    }
    //let userPage = await Page.findOne( { owner: req.user._id, type: "user" } )
    //let comments = await Comment.find( { parentPost: req.query.postUid } )
    //console.log("found comments ?", comments.length)
    //res.json( { error: false, comments: comments } );
})




module.exports = router;