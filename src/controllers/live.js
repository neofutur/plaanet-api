const express = require('express');
const config = require('config');
const bcrypt = require('bcryptjs');
const axios = require('axios');
const slugify = require('slugify');
//const jwt = require('jsonwebtoken');

const auth = require('../core/middleware/auth');
const core = require('../core/core');
const { User } = require('../models/User');
const Live = require('../models/Live');
const Page = require('../models/Page');

const LiveService = require('../services/live');
const PageService = require('../services/page');
// const Notification = require('../models/Notification');
// const Comment = require('../models/Comment');
// const Instance = require('../models/Instance');
// const StateHistory = require('../models/StateHistory');
// const Page = require('../models/Page');
// const StatUrl = require('../models/StatUrl');
// const InstanceService = require('../services/instance');

const router = express.Router()

router.post('/open-live-stream', auth,  async (req, res) => {
  console.log("-------------------------")
  console.log("/live/open-live-stream")
  
  try{
    let nodePath = await core.getNodePath(req.body.pageUid)
    let userPage = await Page.findOne( { owner: req.user._id, type: "user" } ).select(['uid', 'name', 'type', 'roles'])

    if(userPage.uid == req.body.pageUid)
        return res.json({  error: false, page:userPage, live: null })

    console.log("try contact", nodePath + '/broadcast/open-live-stream', req.body.pageUid)
    core.post(nodePath + '/broadcast/open-live-stream', 
        { pageUid: req.body.pageUid,
          userPageUid: userPage.uid
        }, 
        async function(dataRes){ // == then (after response from all instances)
            //console.log("dataRes", dataRes.data)
            //if network sent data
            if(dataRes != null && dataRes.data.error == false){
                let live = dataRes.data.live
                res.json({  error: false, page: dataRes.data.page, live: live })
            }else{
                if(dataRes.data.flagCreateLive == true){ 
                    let live = await Live.findOne({ 'parentPageUid' : dataRes.data.pageUidRef })
                    if(live == null){
                        let pageService = new PageService()
                        live = await pageService.createLive(dataRes.data.pageUidRef)
                    }
                    res.json({  error: false, page: dataRes.data.page, live: live })
                }else{
                    console.log("error 1 : dataRes == null", dataRes.data)
                    res.json({ error: true, live: [], 
                            errorMsg: dataRes.data.msg })
                }
            }
                
        },function(result){ // == catch
            console.log("error 2 : fCatch", result)
        })
  }catch(e){
      console.log("error 3", e)
      res.json({ error: true, live: [], e:e })
  }
})

router.post('/send-message', auth,  async (req, res) => {
  console.log("-------------------------")
  console.log("/live/send-message")
  
  try{
    let nodePath = await core.getNodePath(req.body.liveUid)
    let userPage = await Page.findOne( { owner: req.user._id, type: "user" } )

    console.log("try contact", nodePath + '/broadcast/send-message', req.body.liveUid, req.body.categoryUid, req.body.channelUid)
    core.post(nodePath + '/broadcast/send-live-message', 
        { liveUid: req.body.liveUid,
          categoryUid: req.body.categoryUid,
          channelUid: req.body.channelUid,
          text: req.body.text,
          author: { uid: userPage.uid, name: userPage.name },
          mentions: req.body.mentions,
          toPageUid: req.body.toPageUid
        }, 
        function(dataRes){ // == then (after response from all instances)
            //console.log("dataRes", dataRes.data)
            //if network sent data
            if(dataRes != null && dataRes.data.error == false){
                res.json({  error: false, liveMsg: dataRes.data.liveMsg })
            }else{
                console.log("error 1 : dataRes == null", dataRes.data)
                res.json({ error: true, liveMsg: null, 
                           errorMsg: dataRes.data.msg })
            }
                
        },function(result){ // == catch
            console.log("error 2 : fCatch", result)
        })
  }catch(e){
      console.log("error 3", e)
      res.json({ error: true, liveMsg: [], e:e })
  }
})


router.post('/open-channel', auth,  async (req, res) => {
  console.log("-------------------------")
  console.log("/live/open-channel", req.body.liveUid)
  
  try{
    let nodePath = await core.getNodePath(req.body.liveUid)

    let userPage = await Page.findOne( { owner: req.user._id, type: "user" } )

    let params = {  liveUid: req.body.liveUid,
                    categoryUid: req.body.categoryUid,
                    channelUid: req.body.channelUid,
                    userPageUid: userPage.uid        
                }

    let live = await Live.findOne({ 'uid' : req.body.liveUid })
    //console.log("live found ?", live)
    if(live != null){
        let liveService = new LiveService()
        let json = await liveService.openLiveChannel(params)
        return res.json(json)
    }else{        
        //console.log("try contact", nodePath + '/broadcast/open-live-channel', 
        //            req.body.liveUid, req.body.categoryUid, req.body.channelUid)
        core.post(nodePath + '/broadcast/open-live-channel', 
            params, 
            function(dataRes){ 
                if(dataRes != null && dataRes.data.error == false){
                    res.json({  error: false, conversation: dataRes.data.conversation })
                }else{
                    console.log("error 1 : dataRes == null", dataRes.data)
                    res.json({ error: true, conversation: null, 
                            errorMsg: dataRes.data.msg })
                }
                    
            },function(result){ // == catch
                console.log("error 2 : fCatch", result)
            })    
    }
  }catch(e){
      console.log("error 3", e)
      res.json({ error: true, conversation: [], e:e })
  }
})


router.post('/create-channel', auth,  async (req, res) => {
    console.log("-------------------------")
    console.log("/live/create-channel")
    
    try{
      let nodePath = await core.getNodePath(req.body.liveUid)
      console.log("try contact", nodePath + '/broadcast/create-live-channel', 
                  req.body.liveUid, req.body.categoryUid, req.body.channelName)
  
      /* TODO secure channelName */
      core.post(nodePath + '/broadcast/create-live-channel', 
          { liveUid: req.body.liveUid,
            categoryUid: req.body.categoryUid,
            channelName: req.body.channelName,
          }, 
          function(dataRes){ // == then (after response from all instances)
              //console.log("dataRes", dataRes.data)
              //if network sent data
              if(dataRes != null && dataRes.data.error == false){
                  res.json({  error: false, live: dataRes.data.live, newChannelUid: dataRes.data.newChannelUid })
              }else{
                  console.log("error 1 : dataRes == null", dataRes.data)
                  res.json({ error: true, live: null, newChannelUid: null,
                             errorMsg: dataRes.data.msg })
              }
                  
          },function(result){ // == catch
              console.log("error 2 : fCatch", result)
          })
    }catch(e){
        console.log("error 3", e)
        res.json({ error: true, live: [], e:e })
    }
  })

  router.post('/create-category', auth,  async (req, res) => {
    console.log("-------------------------")
    console.log("/live/create-category")
    
    try{
      let nodePath = await core.getNodePath(req.body.liveUid)
      console.log("try contact", nodePath + '/broadcast/create-live-category', 
                  req.body.liveUid, req.body.categoryName)
  
      /* TODO secure channelName */
      core.post(nodePath + '/broadcast/create-live-category', 
          { liveUid: req.body.liveUid,
            categoryName: req.body.categoryName,
          }, 
          function(dataRes){ // == then (after response from all instances)
              //console.log("dataRes", dataRes.data)
              //if network sent data
              if(dataRes != null && dataRes.data.error == false){
                  res.json({  error: false, live: dataRes.data.live })
              }else{
                  console.log("error 1 : dataRes == null", dataRes.data)
                  res.json({ error: true, live: null, newChannelUid: null,
                             errorMsg: dataRes.data.msg })
              }
                  
          },function(result){ // == catch
              console.log("error 2 : fCatch", result)
          })
    }catch(e){
        console.log("error 3", e)
        res.json({ error: true, live: [], e:e })
    }
  })
    


  router.post('/edit-channel', auth,  async (req, res) => {
    console.log("-------------------------")
    console.log("/live/edit-channel")
    
    try{
      let userPage = await Page.findOne( { owner: req.user._id, type: "user" } )
      let nodePath = await core.getNodePath(req.body.liveUid)
      console.log("try contact", nodePath + '/broadcast/edit-live-channel', req.body.liveUid)
  
      /* TODO secure chanName */
      core.post(nodePath + '/broadcast/edit-live-channel', 
          { userPageUid: userPage.uid,
            liveUid: req.body.liveUid,
            chanName : req.body.chanName,
            chanUid : req.body.chanUid,
            catUid : req.body.catUid
          }, 
          function(dataRes){ 
              if(dataRes != null && dataRes.data.error == false){
                    console.log("Channel name edited ok :", req.body.chanName)
                  res.json({  error: false })
              }else{
                  console.log("error 1 : dataRes == null", dataRes.data)
                  res.json({ error: true })
              }
          },function(result){ console.log("error 2 : fCatch", result) })
    }catch(e){
        console.log("error 3", e)
        res.json({ error: true, e:e })
    }
  })
    

  router.post('/edit-category', auth,  async (req, res) => {
    console.log("-------------------------")
    console.log("/live/edit-category")
    
    try{
      let userPage = await Page.findOne( { owner: req.user._id, type: "user" } )
      let nodePath = await core.getNodePath(req.body.liveUid)
      console.log("try contact", nodePath + '/broadcast/edit-live-category', req.body.liveUid)
  
      /* TODO secure chanName */
      core.post(nodePath + '/broadcast/edit-live-category', 
          { userPageUid: userPage.uid,
            liveUid: req.body.liveUid,
            catUid : req.body.catUid,
            catName : req.body.catName
          }, 
          function(dataRes){ 
              if(dataRes != null && dataRes.data.error == false){
                    console.log("category name edited ok :", req.body.catName)
                  res.json({  error: false })
              }else{
                  console.log("error 1 : dataRes == null", dataRes.data)
                  res.json({ error: true })
              }
          },function(result){ console.log("error 2 : fCatch", result) })
    }catch(e){
        console.log("error 3", e)
        res.json({ error: true, e:e })
    }
  })
    



  router.post('/delete-channel', auth,  async (req, res) => {
    console.log("-------------------------")
    console.log("/live/delete-channel")
    
    try{
      let userPage = await Page.findOne( { owner: req.user._id, type: "user" } )
      let nodePath = await core.getNodePath(req.body.liveUid)
      console.log("try contact", nodePath + '/broadcast/delete-live-channel', req.body.liveUid)
  
      /* TODO secure chanName */
      core.post(nodePath + '/broadcast/delete-live-channel', 
          { userPageUid: userPage.uid,
            liveUid: req.body.liveUid,
            chanUid : req.body.chanUid,
            catUid : req.body.catUid
          }, 
          function(dataRes){ 
              if(dataRes != null && dataRes.data.error == false){
                    console.log("Channel name deleted ok :", req.body.chanUid)
                  res.json({  error: false })
              }else{
                  console.log("error 1 : dataRes == null", dataRes.data)
                  res.json({ error: true })
              }
          },function(result){ console.log("error 2 : fCatch", result) })
    }catch(e){
        console.log("error 3", e)
        res.json({ error: true, e:e })
    }
  })
    

  router.post('/delete-category', auth,  async (req, res) => {
    console.log("-------------------------")
    console.log("/live/delete-category")
    
    try{
      let userPage = await Page.findOne( { owner: req.user._id, type: "user" } )
      let nodePath = await core.getNodePath(req.body.liveUid)
      console.log("try contact", nodePath + '/broadcast/delete-live-category', req.body.liveUid)
  
      /* TODO secure chanName */
      core.post(nodePath + '/broadcast/delete-live-category', 
          { userPageUid: userPage.uid,
            liveUid: req.body.liveUid,
            catUid : req.body.catUid,
          }, 
          function(dataRes){ 
              if(dataRes != null && dataRes.data.error == false){
                    console.log("category name deleted ok :", req.body.catUid)
                  res.json({  error: false })
              }else{
                  console.log("error 1 : dataRes == null", dataRes.data)
                  res.json({ error: true })
              }
          },function(result){ console.log("error 2 : fCatch", result) })
    }catch(e){
        console.log("error 3", e)
        res.json({ error: true, e:e })
    }
  })
    


  router.post('/edit-live-msg', auth,  async (req, res) => {
    console.log("-------------------------")
    console.log("/live/edit-live-msg")
    
    try{
      let userPage = await Page.findOne( { owner: req.user._id, type: "user" } )
      let nodePath = await core.getNodePath(req.body.liveMsgUid)
      console.log("try contact", nodePath + '/broadcast/edit-live-msg', req.body.liveMsgUid)
  
      /* TODO secure chanName */
      core.post(nodePath + '/broadcast/edit-live-msg', 
          { userPageUid: userPage.uid,
            liveMsgUid: req.body.liveMsgUid,
            liveMsgText: req.body.liveMsgText,
            toPageUid: req.body.toPageUid
          }, 
          function(dataRes){ 
              if(dataRes != null && dataRes.data.error == false){
                    console.log("liveMsg edited ok :", req.body.liveMsgUid)
                  res.json({  error: false, liveMsg: dataRes.data.liveMsg })
              }else{
                  console.log("error 1 : dataRes == null", dataRes.data)
                  res.json({ error: true })
              }
          },function(result){ console.log("error 2 : fCatch", result) })
    }catch(e){
        console.log("error 3", e)
        res.json({ error: true, e:e })
    }
  })
    


  router.post('/delete-live-msg', auth,  async (req, res) => {
    console.log("-------------------------")
    console.log("/live/delete-live-msg")
    
    try{
      let userPage = await Page.findOne( { owner: req.user._id, type: "user" } )
      let nodePath = await core.getNodePath(req.body.liveMsgUid)
      console.log("try contact", nodePath + '/broadcast/delete-live-msg', req.body.liveMsgUid)
  
      /* TODO secure chanName */
      core.post(nodePath + '/broadcast/delete-live-msg', 
          { userPageUid: userPage.uid,
            liveMsgUid: req.body.liveMsgUid,
            toPageUid: req.body.toPageUid
          }, 
          function(dataRes){ 
              if(dataRes != null && dataRes.data.error == false){
                    console.log("liveMsg deleted ok :", req.body.liveMsgUid)
                  res.json({  error: false, liveMsg: dataRes.data.liveMsg })
              }else{
                  console.log("error 1 : dataRes == null", dataRes.data)
                  res.json({ error: true })
              }
          },function(result){ console.log("error 2 : fCatch", result) })
    }catch(e){
        console.log("error 3", e)
        res.json({ error: true, e:e })
    }
  })
    


  router.post('/who-is-online', auth,  async (req, res) => {
    //console.log("-------------------------")
    //console.log("/live/who-is-online")
    
    try{
      let pageService = new PageService()
      let page = await pageService.getBroadPage(req.body.pageUid)

      let liveService = new LiveService()
      let { arr, keys } = await liveService.flashMembersByNode(page)
      //console.log("arr, keys", arr, keys)
      let allConnected = []
      await Promise.all(keys.map(async (nodePath) => {
        //console.log("key nodePath", nodePath)
        let targets = arr[nodePath]
        let res =  await core.asyncPost(nodePath + '/broadcast/who-is-online', { targets: targets })
        //console.log("res " + nodePath + '/broadcast/who-is-online', res.data)
        res.data.connected.forEach((p)=>{
          allConnected.push(p)
        })
      }))
      //console.log("after key", allConnected)
      res.json({  error: false, connected: allConnected })
      
    }catch(e){
        console.log("error 3", e)
        res.json({ error: true, e:e })
    }
  })
    


  router.post('/get-conversation', auth,  async (req, res) => {
    console.log("-------------------------")
    console.log("/live/get-conversation")
    
    try{
      let userPage = await Page.findOne( { owner: req.user._id, type: "user" } )
      let nodePath = await core.getNodePath(req.body.liveUid)
      console.log("try contact", nodePath + '/broadcast/delete-live-msg', req.body.liveUid)
  
      let params = {  liveUid: req.body.liveUid,
                      categoryUid: req.body.categoryUid,
                      channelUid: req.body.channelUid,
                      limit: req.body.limit,
                      skip: req.body.skip,
                      userPageUid: userPage.uid,
    }

      /* TODO secure chanName */
      core.post(nodePath + '/broadcast/get-live-conversation', params, 
          function(dataRes){ 
              if(dataRes != null && dataRes.data.error == false){
                  res.json({  error: false, conversation: dataRes.data.conversation })
              }else{
                  console.log("error 1 : dataRes == null", dataRes.data)
                  res.json({ error: true })
              }
          },function(result){ console.log("error 2 : fCatch", result) })
    }catch(e){
        console.log("error 3", e)
        res.json({ error: true, e:e })
    }
  })
    



module.exports = router;