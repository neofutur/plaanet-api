var express = require("express")
var mongoose = require("mongoose")
var router = express.Router()
var app = express()
var cors = require('cors')

const auth = require("../core/middleware/auth");
const bcrypt = require("bcryptjs");
const { User, validate } = require('../models/User')
const Instance = require('../models/Instance')
const Wallet = require('../models/Wallet')
const Transaction = require('../models/Transaction')


const axios = require('axios');
const config = require('config');
const jwt = require('jsonwebtoken');


const core = require('../core/core');

router.get('/run', /*auth, */ async (req, res) => {
    console.log("-------------------------")
    console.log("/tqw/run")

    const i = await Instance.findOne({ isLocal: true })
    
    let tq = await Transaction.find({ status: 'pending',
                                      senderUid : new RegExp("^"+i.name, "i"), }).limit(20)

    console.log("TQ :", tq.length, "to process")
-
    await tq.forEach(async function(transaction) {
        
        const senderW = await Wallet.findOne({ uid : transaction.senderUid } )
                                    .populate('pendingTransactions');

        let amountUni = transaction.amount
        let amountU = amountUni * i.UDValue
        if(senderW.account_u < amountU){
            console.log("error senderW.account_u < amountU", senderW.account_u, "<", amountU)
            transaction.status = "refused"
            transaction.save()
        }else{
            const nameIReceiver = core.iName(transaction.receiverUid)
            const node = await Instance.findOne({ name: nameIReceiver })
        
            axios.post(node.url + ':' + node.port +'/tqw/process-transaction', {
                transaction: transaction
            }).then( res => {
                console.log("success", node.url + ':' + node.port +'/tqw/process-transaction', res.data)
                let endTrans = res.data.transaction
                let amountUni = transaction.amount
                let amountU = amountUni * i.UDValue
                    
                if(senderW.type == "current"){
                    senderW.account_u -= amountU
                }else if(senderW.type == "custom"){
                    senderW.account_uni -= amountUni
                }

                //remove pending transaction in both side
                senderW.pendingTransactions.pull({ _id: transaction.id })
                senderW.validatedTransactions.push(transaction)
                //save wallets to save pending and validated T
                senderW.save()
                
                //update transaction information
                transaction.status = 'validated'
                transaction.senderUni = senderW.type == "current" ? senderW.account_u / i.UDValue : senderW.account_uni,
                transaction.receiverUni = endTrans.receiverUni
                transaction.validated = new Date() 
                transaction.save()

                /* SEND SOCKET NOTIF TO SENDER */
                var uid = i.name + ":" + senderW.owner._id
                console.log("try emit 'sent-transaction' to", uid)
                req.ws.emit(uid, "sent-transaction", 
                            { receiverName: transaction.receiverName,
                              amountUni: transaction.amount })
                console.log("** EMITED sock to", uid)
                /* SEND SOCKET NOTIF TO SENDER */

            }).catch( res => {
                console.log("error", node.url + ':' + node.port +'/tqw/process-transaction', res.data)
                res.json({ error: true, transaction: transaction, res: res } )
            })

            console.log("-------------")
        }
    })
    res.json({ res: tq } )
})



router.post('/process-transaction'/*, auth*/,  async (req, res) => {
    console.log("-------------------------")
    console.log("/process-transaction")
    
    /* NEED TO SECURE : ONLY FOR OTHER NODES - AUTH/node ? user instance.nodeKey */

    try{
        //console.log("req.query.uid:", req.query.uid)
        const transaction = await Transaction.findOne({ transKey: req.body.transaction.transKey })
        const i = await Instance.findOne({ isLocal: true })
          
        if(transaction != null){
            console.log("transaction ok, found in this instance", transaction.transKey)
        }else{
            console.log("transaction ko, not found in this instance")
            res.json({ error: true, transaction: false, 
                    errorMsg: 'Transaction not found. id:'+transaction.id })
        } 
            
        //check if the wallet of the receiver of the transaction is registred in this instance
        if(core.iName(transaction.receiverUid) != i.name){
            res.json({  error: true, errorMsg: 'this wallet is not registred here' })
            return //if not exit and return error
        }

        //if the wallet is registred in this instance
        //get the wallet from local bdd
        const receiverW = await Wallet.findOne({ uid : req.body.transaction.receiverUid } )
          
        let amountUni = transaction.amount
        
        //add amount transaction to the wallet
        if(receiverW.type == "current"){
            receiverW.account_u += amountUni * i.UDValue
        }else if(receiverW.type == "custom"){
            receiverW.account_uni += amountUni
        }

        //remove pending transaction, and add to validated
        receiverW.pendingTransactions.pull({ _id: transaction.id })
        receiverW.validatedTransactions.push(transaction)
        
        //save wallet to save pending and validated T
        receiverW.save()

        //update transaction information
        transaction.status = 'validated'
        transaction.receiverUni = receiverW.type == "current" ? receiverW.account_u / i.UDValue : receiverW.account_uni,
            
        transaction.validated = new Date() 
        transaction.save()

        /* SEND SOCKET NOTIF TO RECEIVER */
        var uid = i.name + ":" + receiverW.owner._id
        console.log("try emit 'receive-transaction' to", uid)
        req.ws.emit(uid, "receive-transaction", 
                    { senderName: transaction.senderName,
                      amountUni: transaction.amount })
        console.log("** EMITED sock to", uid)
        /* SEND SOCKET NOTIF TO RECEIVER */

        res.json({  error: false,
                    transaction: transaction })
        
    }catch(e){
        console.log("error", e)
        res.json({ error: true, transaction: false, 
                   errorMsg: 'An error occured' })
    }
})

module.exports = router;