var express = require("express")
var router = express.Router()

const auth = require("../core/middleware/auth-node");
const bcrypt = require("bcryptjs");
const { User, validate } = require('../models/User')
const Instance = require('../models/Instance')
const Wallet = require('../models/Wallet')
const Transaction = require('../models/Transaction')
const StateHistory = require('../models/StateHistory')
const Publication = require('../models/Publication')
const Notification = require('../models/Notification')
const Page = require('../models/Page')
const File = require('../models/File')
const Comment = require('../models/Comment')
const Survey = require('../models/Survey')
const StatUrl = require('../models/StatUrl')
const Live = require('../models/Live')
const LiveMsg = require('../models/LiveMsg')
const MobileApp = require('../models/MobileApp')

const PageService = require("../services/page");
const RoomService = require("../services/room");
const InstanceService = require('../services/instance');
const LiveService = require('../services/live');
const PublicationService = require("../services/publication");

const config = require('config');
const jwt = require('jsonwebtoken');

const axios = require('axios');
const core = require('../core/core');
let CTP = require("circle-to-polygon");
const { map } = require("joi/lib/types/symbol");

//var ObjectId = require('mongoose').Types.ObjectId; 

let multichain = require("multinodejs")({
    port: config.multichain.port,
    host: config.multichain.host,
    user: config.multichain.user,
    pass: config.multichain.pass
  });

/*
    BROADCAST : this route file contains all routes suseptibles to be requested by others nodes
    All main decentralized processes should be writen in this file
    Authentification is managed by nodeKey, a unique key for each Instance in the network
*/

router.post('/get-map-pages', auth,  async (req, res) => {
    console.log("-------------------------")
    console.log("/broadcast/get-map-pages") //, req.body)

    let radiusMax = 300000 //300km
    let radius = req.body.radius != null ? req.body.radius : radiusMax 
    if(radius > radiusMax) radius = radiusMax

    let request = { 
        '$and' : [{
            'coordinates': {
                '$geoWithin': {
                    '$centerSphere' :
                        [[req.body.lng, req.body.lat], 
                        radius/1000/6378]
                } 
            }
        }]
    }

    let search = req.body.search
    //console.log("req.body.search", search)
    
    if(search != "" 
    && search != null
    && search.indexOf("#") == -1){
        request['$or'] = []
        request['$or'].push({ name: new RegExp(".*"+req.body.search.toLowerCase().trim(), "i") })
    }

    if(search != "" && search != null){
        let words = search.split(' ')
        //console.log("search.split", search, words)
        let arrayW = []
        words.forEach((w)=>{
            let ww = w.trim()
            //if no # then add #
            //console.log("ww.indexOf('#')", ww.indexOf('#'))
            ww = (ww.indexOf('#') === -1) ? "#" + ww : ww
            if(ww != "") arrayW.push(new RegExp(".*"+ww, "i"))
        })
        if(request['$or'] == null) request['$or'] = []
        request['$or'].push({ hashtags : {  '$in' : arrayW  } })

        //console.log("# search.split request hashtags", arrayW)
    }
    
    if(req.body.pageType != "" && req.body.pageType != null)
        request['$and'].push({ type: req.body.pageType })

    
    console.log("request", request)
    let nbPerPage = req.body.nbPerPage
    let map = await Page.find(request)
                        .sort( { 'owner.lastActivityDate': -1 } )
                        .skip( req.body.pageNumber > 0 ? ( ( req.body.pageNumber - 1 ) * nbPerPage ) : 0 )
                        .limit(nbPerPage)
                        .populate({ 
                            path: 'owner',
                            select: ['lastActivityDate']
                        })
                                          
    let totalUsers = 0
    //console.log("map length", map.length)
    res.json( { error: false,   mapPages: map, 
                                totalUserPages: totalUsers } );
})

router.post('/get-map-assembly', auth,  async (req, res) => {
    console.log("-------------------------")
    console.log("/broadcast/get-map-assembly", req.body)

    let map = await Page.find({ type: 'assembly' })
                        .sort( { 'owner.lastActivityDate': -1 } )
                        
    console.log("map length", map.length)
    res.json( { error: false,   map: map } )
})


router.post('/page-get-autocomplete', auth,  async (req, res) => {
    console.log("-------------------------")
    console.log("/page-get-autocomplete", req.body)
    let request = {}

    let search = req.body.search
    //console.log("req.body.search", search)
    if(search != "" 
    && search != null
    && search.indexOf("#") == -1)
        request.name = new RegExp(".*"+req.body.search.toLowerCase().trim(), "i")


    if(req.body.pageType != "" && req.body.pageType != null)
        request.type = req.body.pageType
    
    //console.log("request", request)
    let map = await Page.find(request)
                        .sort( { name: 1 } )
                        .limit(20)
                  
    let totalUsers = 0
    console.log("map length", map.length)
    res.json( { error: false,   pages: map } );
})



router.post('/get-count-pages', auth,  async (req, res) => {
    console.log("-------------------------")
    //console.log("/broadcast/get-count-pages", req.body)
    let request = { 
        '$and' : [{
            'coordinates': {
                '$geoWithin': {
                    '$centerSphere' :
                        [[req.body.lng, req.body.lat], 
                        req.body.radius/1000/6378]
                } 
            }
        }],
    }

    let search = req.body.search
    //console.log("req.body.search", search)
    
    if(search != "" 
    && search != null
    && search.indexOf("#") == -1){
        request['$or'] = []
        request['$or'].push({ name: new RegExp(".*"+req.body.search.toLowerCase().trim(), "i") })
    }

    if(search != "" && search != null){
        let words = search.split(' ')
        //console.log("search.split", search, words)
        let arrayW = []
        words.forEach((w)=>{
            let ww = w.trim()
            //if no # then add #
            //console.log("ww.indexOf('#')", ww.indexOf('#'))
            ww = (ww.indexOf('#') === -1) ? "#" + ww : ww
            if(ww != "") arrayW.push(new RegExp(".*"+ww, "i"))
        })
        if(request['$or'] == null) request['$or'] = []
        request['$or'].push({ hashtags : {  '$in' : arrayW  } })

        //console.log("# search.split request hashtags", arrayW)
    }
    

    if(req.body.pageType != "" && req.body.pageType != null)
        request['$and'].push({ type: req.body.pageType })

    
    //console.log("request", request)
    let count = await Page.countDocuments(request)
    //console.log("count", count)
    res.json( { error: false, count: count } );
})


router.get('/get-network'/*, auth*/,  async (req, res) => {
    console.log("-------------------------")
    console.log("/broadcast/get-network")

    /* Sécuriser ce process pour empêcher n'importe qui de récupérer ces données
    surtout les nodeKeys des instances */

    try{
        const network = await Instance.find()
        res.json({ network: network });
    }catch(e){
        console.log("error", e)
        res.json({ error: true, network: false, 
                   errorMsg: 'No instance found in this server' })
    }
})


router.get('/instance-info', auth,  async (req, res) => {
    console.log("-------------------------")
    console.log("/broadcast/instance-info")
    
    try{
        let instance = await Instance.findOne({ isLocal:true })//.select('-nodeKey')
        const nbUsers = await User.countDocuments()
        const nbPosts = await Publication.countDocuments()
        const nbNotifs = await Notification.countDocuments()
        const nbMobileUsers = await User.countDocuments({ useMobileApp : {$exists:true} })
        console.log("***********************************---------- nbMobileUsers", nbMobileUsers)
        instance.nbUsers = nbUsers
        instance.nbPosts = nbPosts
        instance.nbNotifs = nbNotifs
        instance.nbMobileUsers = nbMobileUsers
        instance.env = config.get('env')

        //update admin list of the instance
        let admins = await User.find({ isAdmin: true }).populate("pages")
        instance.admins = []
        admins.forEach((admin) => {
            instance.admins.push({ uid: admin.pages[0].uid, name: admin.name })
        })
        instance.dateInfoUpdated = new Date()
        instance.save()

        //enregistre l'état du serveur dans StateHistory
        let date1D = new Date()
        date1D.setDate(date1D.getDate() - 1);
        let s = await StateHistory.findOne({ date: { '$gt' : date1D } })
        //if a SH is already save today : no save any other
        if(s == null){ //create new SH for today
            let sh = new StateHistory({ date: new Date(),
                nbUsers: instance.nbUsers,
                nbPosts: instance.nbPosts,
                nbNotifs: instance.nbNotifs,
                nbMobileUsers: instance.nbMobileUsers
            }); await sh.save()
        }


        console.log("send response -", instance.name)
        res.json({ instance: instance, 
                    error: false });
    }catch(e){
        console.log("error", e)
        res.json({ error: true, instance: null, 
                   errorMsg: 'No instance found in this server' })
    }
})

router.post('/get-stream', auth,  async (req, res) => {
    console.log("-------------------------")
    console.log("/broadcast/get-stream") //, req.body)
    
    publicationService = new PublicationService()
    let streamRes = await publicationService.getStream(req.body)
    
    res.json( { error: streamRes.error, 
                stream: streamRes.stream, 
                nbPerPage: streamRes.nbPerPage }  )
})

router.post('/get-stream-private', auth,  async (req, res) => {
    console.log("-------------------------")
    console.log("/broadcast/get-stream-private", req.body.userPageUid)
    
    let request = { 
        'scopeType' : 'private',
       // 'receivers.uid' : { '$in' : [req.body.userPageUid] }
    }
    
    let search = req.body.search
    //console.log("req.body.search", search)
    if(search != "" && search != null){
        if(search.indexOf("#") < 0){
            //console.log("search by text", search)
            request['text'] = new RegExp(".*"+search.toLowerCase().trim(), "i")
        }else if(search.indexOf("#") == 0){
            //console.log("search by tag", search)
            request['hashtags'] =  { '$in' : new RegExp(".*"+search.toLowerCase().trim(), "i") } 
        }
    }
        

    let receivers = [] 
   
    if(req.body.userPageUid != null && req.body.pageUid != null)
        receivers = [req.body.pageUid]
    else if(req.body.userPageUid != null)
        receivers = [req.body.userPageUid]
    
    //console.log("receivers brefore concat", receivers)
    //console.log("admins in list :", req.body.pagesAdmin)
    if(req.body.pagesAdmin != null && req.body.pagesAdmin.length > 0)
        receivers = receivers.concat(req.body.pagesAdmin)

    //console.log("receivers after concat", receivers)

    //receivers = receivers.concat(req.body.userPageUid)
    //console.log("receivers after after concat", receivers)


    //request['receivers.uid'] = { '$in' : receivers }
    request['$and'] = []
    request['$and'].push({'receivers.uid' : { '$in' : receivers }})
    //request['$and'].push({'receivers.uid' : { '$in' : [req.body.userPageUid] }})

    //console.log("request", request)
    let nbPerPage = req.body.nbPerPage != null ? req.body.nbPerPage : 5
    let stream = await Publication.find(request)
                        .sort( { created: -1 } )
                        .skip( req.body.pageNumber > 0 ? ( ( req.body.pageNumber - 1 ) * nbPerPage ) : 0 )
                        .limit(nbPerPage)
                        .populate("author")
                        .populate("signed")
                        //.populate("target")
                        .populate("pdf")
                        .populate({
                            path: 'comments',
                            populate: { path: 'answers' }
                        })
    //console.log("stream length", stream.length)
    res.json( { error: false, 
                stream: stream, 
                nbPerPage: nbPerPage } );
})

router.post('/publication-action', auth,  async (req, res) => {
    console.log("-------------------------")
    console.log("/broadcast/publication-action", req.body)
    
    let post = await Publication.findOne({ uid: req.body.postUid })
                                .populate("signed")
                                .populate({ path: 'author',
                                            select: ['_id', 'uid', 'name', 'city', 'type', 'slug']
                                })
                                .populate({
                                    path: 'comments',
                                    options: {
                                        sort: 'created',
                                        //limit: 3,
                                    },
                                    populate: { 
                                        path: 'answers',
                                        options: {
                                           sort: 'created'
                                        } 
                                    }
                                })
                                
    let actions = ["likes", "dislikes", "favorites", "viewers"]
    actions.forEach(action => {
        if(req.body.type == action){
            //console.log("post[action]", post[action])
            let posAdded = post[action].indexOf(req.body.pageUid)
            if(req.body.bool == false){
                //console.log("force splice", posAdded)
                post[action].splice(posAdded, 1)
            }
            else if(posAdded === -1){ //if uid is NOT yet in the array
                    //console.log("push", req.body.pageUid)
                    post[action].push(req.body.pageUid)
            }else{
                //console.log("splice", req.body.pageUid)
                post[action].splice(posAdded, 1)
            }

            if(req.body.type == 'viewers'){

            }

            /* SEND NOTIF */
            //authorPage a liké une publication
            core.notify.send(req, {
                verb: action.toUpperCase(),
                authors: [{ uid: req.body.pageUid,
                            name: req.body.pageName }],
                whatObj : { uid: post.uid, type: 'publication', 
                            signedName: post.signed.name,
                            signedUid: post.signed.uid,
                            text: post.text.substr(0, 200),
                            posAdded: posAdded }
                
            }, ['viewers', 'signed'], true)
            /* SEND NOTIF */
        
            post.save()

        }
    });

    if(post.sharedSurveyUid != null){
        const roomService = new RoomService()
        let survey = await roomService.getSurvey(post.sharedSurveyUid, req.body.userPageCoordinates)
        if(survey != false) post.surveyShared = survey
    }
    
    res.json( { error: false, postObj: post } );
})

router.post('/publication-get-post', auth,  async (req, res) => {
    console.log("-------------------------")
    console.log("/broadcast/publication-get-post", req.body.postUid)
    
    let post = await Publication.findOne({ uid: req.body.postUid })
                                .populate("signed")
                                .populate("comments")
                                .populate({ path: 'author',
                                            select: ['_id', 'uid', 'name', 'city', 'type', 'slug']
                                })

    if(post != null){
        if(post.sharedSurveyUid != null){
            const roomService = new RoomService()
            let survey = await roomService.getSurvey(post.sharedSurveyUid, req.body.userPageCoordinates)
            if(survey != false) post.surveyShared = survey
        }
        //console.log("***** post.signed", post.signed)
        res.json( { error: false, postObj: post } );
    }else{
        res.json( { error: true } );
    }
})


router.post('/comment-send-comment', auth,  async (req, res) => {
    console.log("-------------------------")
    console.log("/broadcast/comment-send-comment",req.body)

    let authorPage = req.body.authorPage
    let parentPost = await Publication.findOne( { uid: req.body.postUid } )
                                      .populate('signed')
    
    if(parentPost == null){
        res.json( { error: false, newComment: com } );
    }

    let com = new Comment()
    com.text = req.body.commentStr
    com.author = {  uid: authorPage.uid, 
                    name: authorPage.name, 
                    avatar: (authorPage.avatar != null) 
                }
    
    if(req.body.parentCommentId != null){
        let parentCom = await Comment.findById(req.body.parentCommentId)
        if(parentCom == null){ res.json( { error: true, newComment: null } ); }

        parentCom.answers.push(com._id)
        parentCom.save()
        com.parentComment = parentCom._id
        //console.log("check answer", parentCom._id, com)
    }else{
        com.parentPost = parentPost._id
        parentPost.comments.push(com._id)
        parentPost.save()
    }
    com.emojis = new Array()
    com.answers = new Array()
    com.created = new Date()
    
    com.save()

    let authorP = await Page.findOne( { uid: parentPost.signed.uid } )
    
    /* SEND NOTIF */
    //authorPage a publié un commentaire à propos d'une publication
    core.notify.send(req, {
            verb: 'COMMENT',
            authors: [{ uid: authorPage.uid,
                        name: authorPage.name }],
            whatObj : { uid: com.id, _id: com.id, type: 'comment',
                        parentCommentId: req.body.parentCommentId,
                        text: com.text, emojis: [] }, 
            aboutObj: { uid: parentPost.uid, type: 'publication', 
                        signedName: parentPost.signed.name,
                        signedUid: parentPost.signed.uid,
                        text: parentPost.text.substr(0, 200) }
        }, ['viewers', 'receivers', 'commenters', 'signed', 'favorites'], true)

    /* SEND NOTIF */
    
    res.json( { error: false, newComment: com } );
})

router.post('/comment-send-emoji', auth,  async (req, res) => {
    console.log("-------------------------")
    console.log("/broadcast/comment-send-emoji", req.body)
    
    let com = await Comment.findById(req.body.commentId)
    if(com == null) {
        res.json( { error: true, msg: "no comment", comment: com } );
        return
    }

    let found = false
    let emos = com.emojis
    let authorPage = req.body.authorPage

    let parentPost = await Publication.findOne( { uid: req.body.postUid } )
                                      .populate('signed')
    
    if(parentPost == null){
        res.json( { error: true, msg: "no parent post", comment: com } );
        return
    }

    let pos = -1
    emos.forEach((emo, i) => {
        if(emo.name == req.body.emoji){
            pos = emos[i].users.indexOf(authorPage.uid)
            if(pos > -1){ emos[i].users.splice(pos, 1)
            }else{  emos[i].users.push(authorPage.uid) }
            found = true
        }
    });
    
    if(found == false){
        com.emojis.push({ name: req.body.emoji, users : [authorPage.uid] })
    }else{
        com.emojis = emos
    }
    await com.update(emos, {$set: {field: 'emojis'}});
    await com.save()

    /* SEND NOTIF */
    core.notify.send(req, {
        verb: 'EMOJI',
        authors: [{ uid: authorPage.uid,
                    name: authorPage.name }],
        whatObj : { uid: com.id, type: 'comment',
                    signedName: com.author.name,
                    signedUid: com.author.uid,
                    parentCommentId: req.body.parentCommentId,
                    text: com.text.substr(0, 200),
                    emoji: req.body.emoji,
                    posAdded: pos
                },
        aboutObj: { uid: parentPost.uid, type: 'publication', 
                    signedName: parentPost.signed.name,
                    signedUid: parentPost.signed.uid,
                    text: parentPost.text.substr(0, 200) }
    }, ['viewers', 'commenters'], false)
    /* SEND NOTIF */
    
    res.json( { error: false, comment: com } );
})


router.post('/page-profil', auth,  async (req, res) => {
    //console.log("-------------------------")
    //console.log("/broadcast/page-profil", req.body)

    let pageService = new PageService()
    let page = await pageService.getPageProfile(req.body.pageUid, req.body.userPageCoordinates) 
    
    //if(page!=null) console.log("/broadcast/page-profil, page", page.uid)
    res.json( { error: page==null,  page: page } );
})

router.post('/get-member-pages', auth,  async (req, res) => {
    //console.log("-------------------------")
    //console.log("/broadcast/get-member-pages", req.body.pageUid)
    let pages = await Page.find({ 'roles.editor.uid': req.body.pageUid })
                            .select(['_id', 'uid', 'name', 'description', 'city', 'type', 'coordinates', 'roles'])
    //console.log("++++++++++ +++++++++ ++++ Nb member page found : ", pages.length)

    let pageService = new PageService()
    let members = []
    //add notifications for live messages
    await Promise.all(pages.map(async (page, p)=>{
        let member = {
            '_id': page._id, 
            'uid': page.uid, 
            'name': page.name, 
            'description': page.description, 
            'city': page.city, 
            'type': page.type, 
            'coordinates': page.coordinates, 
            'roles': page.roles
        }
        let live = await pageService.findLiveForPage(page.uid, req.body.pageUid, page.type)
        //console.log("found live ?", live != null)
        if(live != null){
            if(live.categories != null)
            live.categories.forEach((category, c)=>{
                if(category.channels != null)
                category.channels.forEach((channel, ch)=>{
                    channel.dateLastOpen.forEach((data)=>{
                        if(data.uid == req.body.pageUid){
                            //console.log("check date last open", data.date, channel.dateLastMsg, data.date > channel.dateLastMsg)
                            if(data.date < channel.dateLastMsg){
                                member.alert = true
                            }
                        }
                    })
                    if(channel.mentions != null)
                    channel.mentions.forEach((data)=>{
                        if(data.uid == req.body.pageUid){
                            if(channel.dateLastOpen != null)
                            channel.dateLastOpen.forEach((open)=>{
                                if(open.uid == req.body.pageUid
                                    && open.date < data.date) {
                                        if(member.countMention == null) member.countMention = 1
                                        else member.countMention += 1
                                    }
                            })
                            //console.log("check date last open", data.date, channel.dateLastMsg, data.date > channel.dateLastMsg)
                        }
                    })
                })
            })
            //if(member.alert)
            //console.log("PAGE ALERT LIVE :", member.alert, member.name, member.countMention)
        }
        members.push(member)
    }))

    if(pages.length== 0)
     res.json( { error: true,  pages: pages } );
    else
        res.json( { error: false,  pages: members } );
})

router.post('/get-my-notifications', auth,  async (req, res) => {
    console.log("-------------------------")
    console.log("/broadcast/get-my-notifications", req.body)
   
    let request = { 'targets': req.body.pageUid }
    let nbPerPage = 30
    let notifications = await Notification.find(request)
                        .sort( { updated: 1 } )
                        //.skip( req.body.pageNumber > 0 ? ( ( req.body.pageNumber - 1 ) * nbPerPage ) : 0 )
                        .limit(nbPerPage)
    
    res.json( { error: false, notifications: notifications } );
})


router.post('/relay-socket', auth,  async (req, res) => {
    console.log("-------------------------")
    console.log("/broadcast/relay-socket", req.body.targetUid)
    
    req.ws.emit(req.body.targetUid, req.body.action, req.body.data)

    res.json( { error: false } );
})


router.post('/leave-post', auth,  async (req, res) => {
    console.log("-------------------------")
    console.log("/broadcast/leave-post", req.body)
    
    //let userPage = await Page.findOne( { owner: req.user._id, type: "user" } )
    let post = await Publication.findOne( { uid: req.body.postUid } )
    console.log("leave ?", post, req.body.userPageUid)
    
    let found = -1
    post.receivers.forEach((page, i)=>{
        if(page.uid == req.body.userPageUid) found = i
    })
    post.receivers.splice(found, 1)
    post.save()
    

    res.json( { error: false } );
})

router.post('/publication-add-receivers', auth,  async (req, res) => {
    console.log("-------------------------")
    console.log("/broadcast/publication-add-receivers", req.body)
    
    let post = await Publication.findOne( { uid: req.body.postUid } )
    if(post == null){
        res.json( { error: true, newReceivers: null } );
    }else{
        let inReceivers = false
        post.receivers.forEach((page, i)=>{
            //console.log("check page", page)
            if(page.uid == req.body.userPageUid) inReceivers = true
        })

        if(inReceivers && post.sharable == true){
            //console.log("set receivers1", post.receivers)
            post.receivers = post.receivers.concat(req.body.receivers)
            //console.log("set receivers2", post.receivers)
            await post.save()
        }else{
            res.json( { error: true, newReceivers: post.receivers } );
        }
        res.json( { error: false, newReceivers: post.receivers } );
    }
})


router.post('/page-add-follower', auth,  async (req, res) => {
    console.log("-------------------------")
    console.log("/broadcast/page-add-follower", req.body)
    
    let page = await Page.findOne( { uid: req.body.pageUid } )
    if(page == null){
        //console.log("page not found")
        return res.json( { error: true, newFollowers: null } );
    }else{
        let inFollowers = false
        if(page.followers == null) 
            page.followers = []

        page.followers.forEach((p, i)=>{
            //console.log("check page", req.body.userPageUid, p)
            if(p == req.body.userPageUid) inFollowers = true
        })
        //console.log("in followers", inFollowers)

        if(!inFollowers){
            //console.log("set followers1", page.followers)
            page.followers.push(req.body.userPageUid)
            //console.log("set followers2", page.followers)
            await page.save()
        }else{
            return res.json( { error: true, newFollowers: page.followers } );
        }
        return res.json( { error: false, newFollowers: page.followers } );
    }
})

router.post('/page-remove-follower', auth,  async (req, res) => {
    console.log("-------------------------")
    console.log("/broadcast/page-remove-follower", req.body)
    
    let page = await Page.findOne( { uid: req.body.pageUid } )
    if(page == null){
        //console.log("page not found")
        return res.json( { error: true } );
    }else{
        //console.log("page found", page)
        if(page.followers == null) page.followers = []
        let pos = -1
        page.followers.forEach((p, i)=>{
            if(p == req.body.userPageUid) pos = i
        })
        if(pos > -1) page.followers.splice(pos, 1)
        //console.log("splice follower", pos)
        page.save()
        
        res.json( { error: false } );
    }
})

router.post('/page-add-blacklister', auth,  async (req, res) => {
    console.log("-------------------------")
    console.log("/broadcast/page-add-follower", req.body)
    
    let page = await Page.findOne( { uid: req.body.userPageUid, type: 'user' } )
    if(page == null){
        res.json( { error: true, newReceivers: null } );
    }else{
        let inBlacklisters = false
        page.blacklisters.forEach((page, i)=>{
            //console.log("check page", page)
            if(page.uid == req.body.userPageUid) inBlacklisters = true
        })

        if(inBlacklisters){
            //console.log("set blacklisters1", page.blacklisters)
            page.blacklisters.push(req.body.userPageUid)
            //console.log("set blacklisters4", page.blacklisters)
            await page.save()
        }else{
            res.json( { error: true, newBlacklisters: page.blacklisters } );
        }
        res.json( { error: false, newBlacklisters: page.blacklisters } );
    }
})



router.post('/page-get-library', auth,  async (req, res) => {
    console.log("-------------------------")
    console.log("/broadcast/page-get-library", req.body.pageUid)

    let page = await Page.findOne( { uid: req.body.pageUid } )

    let files = await File.find( { author: page.id } )
    //console.log("/page/get-library", files.length)
    if(files.length == 0){
        res.json( { error: true } );
        return
    }
    res.json( { error: false, files: files } );
})


router.get('/comment-get-conv', auth,  async (req, res) => {
    console.log("-------------------------")
    console.log("/broadcast/comment-get-conv", req.query)
    
    //let userPage = await Page.findOne( { owner: req.user._id, type: "user" } )
    //let query = {}

    if(req.query.parentCommentId != null) {
        const comments = await Comment.find( { //parentPost: core.iKey(req.query.postUid),
                                               parentComment: req.query.parentCommentId } )
                                        .sort('created')
        return res.json( { error: false, comments: comments } )
    }
    else if(req.query.postUid != null){
        const comments = await Comment.find( { parentPost: core.iKey(req.query.postUid) } )
                                      .sort('created')
        return res.json( { error: false, comments: comments } )
    } 
    
    //console.log("error comments ?")
    res.json( { error: true, comments: [] } );
})



router.post('/comment-delete', auth,  async (req, res) => {
    console.log("-------------------------")
    console.log("/broadcast/comment-delete", req.body)
    
    
    let comment = await Comment.findOne( { _id: req.body.commentId } )
                                .populate({
                                    path: 'parentPost',
                                    populate: { path: 'signed' }
                                })

    //check roles
    let isAdmin = core.isRole(req.body.authorPage.uid, comment.parentPost.signed, 'admin')
    let isModerator = core.isRole(req.body.authorPage.uid, comment.parentPost.signed, 'moderator')
    
    if(comment != null 
    && (  comment.author.uid == req.body.authorPage.uid 
       || comment.parentPost.signed.uid == req.body.authorPage.uid)
       || isModerator || isAdmin
    ){
        await comment.remove()
        return res.json( { error: false } )
    }else{
        return res.json( { error: true } )
    }

})



router.post('/publication-delete', auth,  async (req, res) => {
    console.log("-------------------------")
    console.log("/broadcast/publication-delete")
    
    
    let post = await Publication.findOne( { uid: req.body.postUid } )
                                .populate("author").populate("signed")
                                .populate("comments").populate("pdf")

    if(post == null) return res.json( { error: true, msg: 'post not found' } )

    //check roles
    let isAdmin = core.isRole(req.body.authorPage.uid, post.signed, 'admin')
    let isModerator = core.isRole(req.body.authorPage.uid, post.signed, 'moderator')
    let isAuthor = req.body.authorPage.uid == post.author.uid

    if(isAdmin || isModerator || isAuthor){

        console.log("delete all comments")
        post.comments.forEach(async (com)=>{ await com.remove() })

        console.log("delete all notifications")
        let notifs = await Notification.find( { whatObj: post.uid } )
        notifs.forEach(async (notif)=>{ await notif.remove() })

        if(post.pdf != null){
            console.log("delete PDF file uploaded")
            let file = await File.findOne( { _id: post.pdf._id  } )
            if(file != null){
                console.log("remove pdf file", file.name)
                fs.unlink("./public/uploads/pdf/" + post.signed.uid + "/" + file.name, (err) => {
                    if (err) { console.error(err); return; }
                })                
                await file.remove()
            }
        }

        console.log("delete post")
        await post.remove()

        res.json( { error: false } );

    }else{
        res.json( { error: true } );
    }

})


router.post('/publication-edit', auth,  async (req, res) => {
    console.log("-------------------------")
    console.log("/broadcast/publication-edit")
    
    
    let post = await Publication.findOne( { uid: req.body.postUid } )
                                .populate("author").populate("signed")
                                .populate("comments").populate("pdf")

    if(post == null) return res.json( { error: true, msg: 'post not found' } )

    //check roles
    let isAdmin = core.isRole(req.body.authorPage.uid, post.signed, 'admin')
    let isModerator = core.isRole(req.body.authorPage.uid, post.signed, 'moderator')
    let isAuthor = req.body.authorPage.uid == post.author.uid

    if(isAdmin || isModerator || isAuthor){

        let post = await Publication.findOne( { uid: req.body.postUid } ).populate("author")

        if(post == null) res.json( { error: true, msg: 'post not found', postText: "" } );

            //console.log("set text1", post.text)
            post.text = req.body.text
            //post.embedMetaLinks = req.body.embedMetaLinks
            //post.markModified('embedMetaLinks')

            let hashtags = core.extractTags(post.text)
            post.hashtags = hashtags

            //console.log("set text2", post.text)
            await post.save()
            res.json( { error: false, postText: post.text, postTags: post.hashtags } );

    }else{
        res.json( { error: true } );
    }

})

router.post('/comment-edit', auth,  async (req, res) => {
    console.log("-------------------------")
    console.log("/broadcast/comment-edit", req.body)
    
    
    let comment = await Comment.findOne( { _id: req.body.commentId } )
    //console.log("set text ?", comment.author.uid, req.body.authorPage.uid)
    if(comment.author.uid == req.body.authorPage.uid){
        //console.log("set text1", comment.text)
        comment.text = req.body.text
        //console.log("set text2", comment.text)
        await comment.save()
    }else{
        res.json( { error: true, commentText: "" } );
    }

    res.json( { error: false, commentText: comment.text } );
})




/********************* ROOM SONDAGES VOTES ****************************/

  //to send a new survey
  router.post('/save-survey', auth, async (req, res) => {
    console.log("-------------------------")
    console.log("/broadcast/save-survey")
  
    const parentPage = await Page.findOne({ uid: req.body.roomUid })
    const i = await Instance.findOne({ isLocal:true })


    const roomService = new RoomService()
    let canSurvey = roomService.canSurvey(parentPage, req.body.authorPage.coordinates)
    if(!canSurvey) return res.json( { error: true, msg: 'your position is too far from the assembly, you cant publish survey here' } )
  
    let post = new Publication()
    //post.hashtags = hashtags
    let now = new Date()
    post.created = now
    post.text = ''
    post.uid = i.name + ":" + post.id
    post.author = parentPage._id
    post.signed = parentPage._id
    post.target = { uid: parentPage.uid, name: parentPage.name, city: parentPage.city }
    post.sharable = true
    post.likes = []
    post.dislikes = []
    post.favorites = []
    post.comments = []
    post.viewers = []
    post.scopeType = "localised"
    post.coordinates = [parentPage.coordinates[1], parentPage.coordinates[0]]
    post.radius = 300000
    post.fromCity = parentPage.city
    post.scopeGeo = CTP([parentPage.coordinates[0], parentPage.coordinates[1]], post.radius, 30)
  
    let survey = new Survey()
    survey.uid = i.name + ":" + survey.id
    survey.roomUid = req.body.roomUid
    survey.type = req.body.type
    survey.title = req.body.title
    survey.text = req.body.text
    survey.tags = req.body.tags
    survey.answers = req.body.answers
    survey.open = false
    survey.rejected = false
    survey.parentPage = parentPage
    survey.created = now
    survey.duration = req.body.duration
    survey.author = req.body.authorPage
    survey.originalSurveyUid = req.body.originalSurveyUid != null ? req.body.originalSurveyUid : survey.uid

    post.sharedSurveyUid = survey.uid  
    survey.postUid = post.uid
  
    //dateOpen: Date,
    //dateEnd: Date
    
    await survey.save()
    await post.save()

    //set a first support for the author of the survey
    //(the author always support his own survey)
    //init support result on :       support 100% - 0% reject => validated
    //if 1 user reject this survey : support 50% - 50% reject => validated
    //if 2 user reject this survey : support 33% - 66% reject => rejected
    await multichain.publish({  stream: 'support', 
                                key: survey.uid, 
                                data: { "json": { 
                                            "voteValue" : "1",
                                            "authorPageUid": req.body.authorPage.uid
                                        } },
                                options: 'offchain'
                                })
                    .catch((e)=>{
                        console.log("error publish :", e)
                        return res.json( { error: true, e: e } );
                    })

    console.log("survey created :", survey != null)
    res.json( { error: false, survey: survey } )
  })
  
  //to send a new survey
  router.post('/edit-survey', auth, async (req, res) => {
    console.log("-------------------------")
    console.log("/broadcast/edit-survey")
  
    let survey = await Survey.findOne({ uid: req.body.originalSurveyUid })
    //si le survey n'est pas trouvé
    if(survey == null) return res.json( { error: true, msg: 'survey is null' } )
    //si le survey n'est pas trouvé
    if(survey.open == true) return res.json( { error: true, msg: 'survey is open' } )
    //si l'auteur du survey n'est pas celui envoyé en param
    if(survey.author.uid != req.body.authorPage.uid) return res.json( { error: true, msg: 'you are not the author'} )

    survey.title = req.body.title
    survey.text = req.body.text
    survey.tags = req.body.tags
    survey.answers = req.body.answers
    survey.duration = req.body.duration
    
    await survey.save()

    console.log("survey created :", survey != null)
    res.json( { error: false, survey: survey } )
  })
  
  //allow users to vote
  router.get('/open-vote-survey',  async (req, res) => {
    console.log("-------------------------")
    console.log("/broadcast/open-survey")
  
    
    // multichain.publish({  stream: 'surveys', 
    //                       key: req.data.assemblyUid, 
    //                       data: req.body.survey,
    //                       options: 'offchain'
    //                     }).catch((e)=>{
    //                       console.log("error publish :", e)
    //                     })
    
    console.log("items :", items)
  
  })
  
  
  router.post('/get-surveys', auth,  async (req, res) => {
    console.log("-------------------------")
    console.log("/broadcast/get-surveys", req.body.roomUid)
  
    let query = { rejected: false }

    if(req.body.roomUid != null) query.roomUid = req.body.roomUid
    if(req.body.searchType != 'all') query.type = req.body.searchType
    //filter ?= none support startedSoon endSoon nbVotes ended

    if(req.body.searchStr){
        query['$or'] = []
        query['$or'].push({ title : new RegExp(".*"+req.body.searchStr.toLowerCase().trim(), "i") })
        let words = req.body.searchStr.split(' ')
        //console.log("search.split", search, words)
        let arrayW = []
        words.forEach((w)=>{
            let ww = w.trim()
            //if no # then add #
            //console.log("ww.indexOf('#')", ww.indexOf('#'))
            ww = (ww.indexOf('#') === -1) ? "#" + ww : ww
            if(ww != "") arrayW.push(new RegExp(".*"+ww, "i"))
        })
        query['$or'].push({ tags : {  '$in' : arrayW  } })
    }

    if(req.body.filter == 'support'){ 
        query.open = false
        query.dateEnd = { $exists: false }
    }
    else if(req.body.filter != 'none') query.open = true

    if(req.body.filter == 'ended'){
        query.open = false
        query.dateEnd = { $lt: new Date() }
    } 

    //console.log("/broadcast/get-surveys query:", query)

    //use req.body.userPage.coordinates to set permission to vote (< 100km ?)
    let surveys = []
    surveys = await Survey.find( query )
                            .populate({ path: 'parentPage',
                                        select: ['_id', 'uid', 'name', 'city', 'type', 'coordinates']
                            })
                            .sort( { created: -1 } ).limit(100)

    const roomService = new RoomService()
    await Promise.all(surveys.map(async (survey, i) => {
        let supports = await roomService.getSupports(multichain, survey.uid)
        surveys[i].supports = supports
        //console.log("supports founds", supports.length)
        if(survey.open === true || survey.dateEnd < new Date()){
            let { votes, results } = await roomService.getVotes(multichain, survey)
            surveys[i].votes = votes
            surveys[i].nbVotes = votes.length
            surveys[i].results = results
        }

        if(req.body.userPage.coordinates == null){
            console.log("please provide user coordinates to calculate canVote authorization")
            surveys[i].canVote = false //security
            //return res.json( { error: true, msg:"please provide user coordinates to calculate canVote authorization" } );
        }else{
            //if(survey.parentPage == null) survey.remove() //si la page de l'assemblée parent a été supprimées
            //else 
            surveys[i].canVote = roomService.canVote(survey, req.body.userPage.coordinates)
        }
    }))

    surveys = roomService.orderBy(surveys, req.body.filter)
    
    console.log("surveys founds", surveys.length)
    res.json( { error: false, surveys: surveys } );
  })
  
  
  router.post('/get-survey/', auth,  async (req, res) => {
    console.log("-------------------------")
    console.log("/broadcast/get-survey", req.body.surveyUid)
  
    //use req.body.userPage.coordinates to set permission to vote (< 100km ?)
    let survey = await Survey.findOne( { uid: req.body.surveyUid } )
                              .populate({ path: 'parentPage',
                                          select: ['_id', 'uid', 'name', 'city', 'type', 'coordinates']
                              })
    if(survey == null){
        return res.json( { error: true } );
    }                          
    let post = await Publication.findOne( { uid: survey.postUid } )
                                .populate({
                                  path: 'comments',
                                  options: {
                                      sort: 'created',
                                      //limit: 3,
                                  },
                                  populate: { 
                                      path: 'answers',
                                      options: {
                                        sort: 'created'
                                      } 
                                  }
                              })
    
                              
    const roomService = new RoomService();
    let supports = await roomService.getSupports(multichain, req.body.surveyUid)
    survey.supports = supports

    if(survey.open === true || survey.dateEnd < new Date()){
        let { votes, results } = await roomService.getVotes(multichain, survey)
        survey.votes = votes
        survey.results = results
    }

    if(req.body.userPage.coordinates == null){
        console.log("please provide user coordinates to calculate canVote authorization")
        survey.canVote = false
        //return res.json( { error: true, msg:"please provide user coordinates to calculate canVote authorization" } );
    }else{
        survey.canVote = roomService.canVote(survey, req.body.userPage.coordinates)
    }

    //console.log("survey founds", survey != null, survey.open)
    res.json( { error: false, survey: survey, post: post } );

  })


  router.post('/send-support/', auth,  async (req, res) => {
    console.log("-------------------------")
    console.log("/broadcast/send-support", req.body)
  
    const roomService = new RoomService()
    let alreadySupport = await roomService.hasAlreadySupport(multichain, req.body.surveyUid, req.body.userPage.uid)
    if(alreadySupport) return res.json( { error: true, msg: 'you already sent support' } );

    //check if you can vote
    let survey = await Survey.findOne( { uid: req.body.surveyUid } )
                              .populate({ path: 'parentPage',
                                          select: ['_id', 'uid', 'coordinates'] })

    let canVote = roomService.canVote(survey, req.body.userPage.coordinates)
    if(!canVote) return res.json( { error: true, msg: 'your position is too far from the assembly, you cant vote' } )

    await multichain.publish({  stream: 'support', 
                                key: req.body.surveyUid, 
                                data: { "json": { 
                                            "voteValue" : ""+req.body.voteValue,
                                            "authorPageUid": req.body.userPage.uid
                                        } },
                                options: 'offchain'
                                })
                    .catch((e)=>{
                        console.log("error publish :", e)
                        return res.json( { error: true, e: e } );
                    })

    let supports = await roomService.getSupports(multichain, req.body.surveyUid)

    console.log("send-support ok", supports.length)
    return res.json( { error: false, supports: supports } );

  })

  router.post('/send-vote/', auth,  async (req, res) => {
    console.log("-------------------------")
    console.log("/broadcast/send-vote", req.body)
  
    const roomService = new RoomService()
    let alreadyVote = await roomService.hasAlreadyVote(multichain, req.body.surveyUid, req.body.userPage.uid)
    if(alreadyVote) return res.json( { error: true, msg: 'you already sent your vote' } );
    
    //check if you can vote
    let survey = await Survey.findOne( { uid: req.body.surveyUid } )
                             .populate({ path: 'parentPage',
                                         select: ['_id', 'uid', 'type', 'coordinates'] })

    let canVote = roomService.canVote(survey, req.body.userPage.coordinates)
    if(!canVote) return res.json( { error: true, msg: 'your position is too far from the assembly, you cant vote' } )

    await multichain.publish({  stream: 'vote', 
                                key: req.body.surveyUid, 
                                data: { "json": { 
                                            "voteValue" : ""+req.body.voteValue,
                                            "authorPageUid": req.body.userPage.uid
                                        } },
                                options: 'offchain'
                                })
                    .catch((e)=>{
                        console.log("error publish :", e)
                        return res.json( { error: true, e: e } )
                    })

    //console.log("getVotes ?", req.body.surveyUid)
    
    let { votes, results } = await roomService.getVotes(multichain, survey)

    console.log("send-votes ok", votes.length)
    return res.json( { error: false, votes: votes, results: results } );

  })

  router.post('/delete-survey/', auth,  async (req, res) => {
    console.log("-------------------------")
    console.log("/broadcast/delete-survey", req.body)
  
    let survey = await Survey.findOne( { uid: req.body.surveyUid } )

    if(survey == null) return res.json( { error: true } )

    //si le user qui veut delete n'est pas l'auteur du survey
    if(survey.author.uid != req.body.userPageUid){
        //si le user qui veut delete n'est pas l'admin du serveur sur lequel se trouve le survey
        if(req.body.userPageIsAdmin == false  
        || core.iName(req.body.surveyUid) != core.iName(req.body.userPageUid)){
            console.log("sorry, you are not authorized to delete this survey")
            return res.json( { error: true } )
        }
    } 
        
    
    let post = await Publication.findOne( { uid: survey.postUid } )

    await Comment.deleteMany( { parentPost: post._id } )
    await Publication.deleteOne( { uid: survey.postUid } )
    await Survey.deleteOne( { uid: req.body.surveyUid } )

    console.log("delete-survey ok")
    return res.json( { error: false } )

  })

  router.post('/get-cumul-results/', auth,  async (req, res) => {
    console.log("-------------------------")
    console.log("/broadcast/get-cumul-results", req.body)
  
    let surveys = await Survey.find( { originalSurveyUid: req.body.surveyUid, dateOpen: { '$exists': true } } )
                              .populate('parentPage')

    console.log("get-cumul-results survey found :", surveys.length)

    if(surveys == null) return res.json( { error: true } )

    const roomService = new RoomService()
    await Promise.all(surveys.map(async (survey, s) => {
        let { votes, results } = await roomService.getVotes(multichain, survey)
        surveys[s].votes = votes
        surveys[s].results = results
    }))
    
    return res.json( { error: false, surveys: surveys } )

  })

  router.post('/manage-role/', auth,  async (req, res) => {
    console.log("-------------------------")
    console.log("/broadcast/manage-role", req.body)

    let pageService = new PageService()
    let page = await Page.findOne( { uid: req.body.onPageUid } )
    //console.log("found page ?", page.name)

    let id = {uid: req.body.page.uid, name: req.body.page.name }
    let type = req.body.type
    let role = req.body.role

    if(req.body.action == 'role'){

        if(type == 'add'){
            //console.log("adding role")
            //si on veut ajouter un role, vérifie si la demande était bien dans les pending ou les invites
            //sinon renvoit une erreur
            if(pageService.isRole(page.pendingRoles, role, id)){
                //console.log("is in pending")
                //pour les pending, vérifie que c'est un admin qui valide la demande
                if(!pageService.isRole(page.roles, 'admin', { uid: req.body.userPageUid })){
                    //console.log("is not admin")
                    return res.json( { error: true, msg: 'you are not an admin, sorry buddy' } )
                }//else{ isAuth = true }
            }
            //sinon, vérifie que le user avait été invité pour un role
            else if(!pageService.isRole(page.inviteRoles, role, id)){ 
                //console.log("is not in invites")
                if(page.type != "user")
                return res.json( { error: true, msg: 'you are not invited to do zat, sorry buddy' } ) 
            }
        }
        if(type == 'cancel'){
            //console.log("canceling role")
            //si on veut annuler un role, vérifie si le user est admin de la page
            if(!pageService.isRole(page.roles, 'admin', { uid: req.body.userPageUid })){
                //console.log("is not admin")

                //ou sinon que j'annule mon propre role, et pas celui de quelqu'un dautre
                //console.log("cancel my own role ?", page.uid, req.body.userPageUid)
                if(id.uid != req.body.userPageUid)
                return res.json( { error: true, msg: 'you can not cancel the role of quelqun dautre, if you are not admin, sorry buddy' } )
            }
        }

        //si aucune erreur : on peut effectuer l'opération

        if(type == 'add')    page.inviteRoles = pageService.deleteRole(page, 'inviteRoles', role, id)
        if(type == 'add')    page.pendingRoles = pageService.deleteRole(page, 'pendingRoles', role, id)
        if(type == 'add')    page.roles = pageService.insertRole(page.roles, role, id)
        if(type == 'cancel') page.roles = pageService.deleteRole(page, 'roles', role, id)
        page.markModified('roles')
        page.markModified('inviteRoles')
        page.markModified('pendingRoles')
        //console.log(type, "role", role, 'to', id.name, 'on', page.name)

    }
    if(req.body.action == 'invite'){
        if(type == 'add')    page.inviteRoles = pageService.insertRole(page.inviteRoles, role, id)
        if(type == 'cancel') page.inviteRoles = pageService.deleteRole(page, 'inviteRoles', role, id)
        page.markModified('inviteRoles')
        //envoie une notif au user invité
        if(type == 'add') req.ws.emit(id.uid, "new-role", { action: req.body.action })
        //console.log(type, "role", role, 'to', id.name, 'on', page.name)
    }
    if(req.body.action == 'pending'){
        if(type == 'add')    page.pendingRoles = pageService.insertRole(page.pendingRoles, role, id)
        if(type == 'cancel') page.pendingRoles = pageService.deleteRole(page, 'pendingRoles', role, id)
        page.markModified('pendingRoles')
        //envoie une notif à tous les admin de la page, pour signaler une nouvelle demande de role
        if(type == 'add'){
            page.roles.admin.forEach((p)=> { 
                req.ws.emit(p.uid, "new-role", { action: req.body.action })
            })
        }

        //console.log(type, "role", role, 'to', id.name, 'on', page.name)
    }

    await page.save()
    return res.json( { error: false, page: page, pageType: page.type, pageName: page.name } )
  })

  router.post('/get-invite-roles/', auth,  async (req, res) => {
    //console.log("-------------------------")
    //console.log("/broadcast/get-invite-roles", req.body)

    let invitesAdmin = await Page.find( { 'inviteRoles.admin.uid': req.body.userPageUid } ).select(['uid', 'name'])
    let invitesEditor = await Page.find( { 'inviteRoles.editor.uid': req.body.userPageUid } ).select(['uid', 'name'])
    let invitesModerator = await Page.find( { 'inviteRoles.moderator.uid': req.body.userPageUid } ).select(['uid', 'name'])
    
    let invites = []
    invitesAdmin.forEach((page)=>{ invites.push({'role' : 'admin', uid: page.uid, name: page.name }) })
    invitesEditor.forEach((page)=>{ invites.push({'role' : 'editor', uid: page.uid, name: page.name }) })
    invitesModerator.forEach((page)=>{ invites.push({'role' : 'moderator', uid: page.uid, name: page.name }) })

    return res.json( { error: false, invites: invites } )
  })


  router.post('/get-pending-roles/', auth,  async (req, res) => {
    //console.log("-------------------------")
    //console.log("/broadcast/get-pending-roles", req.body)

    let pages = await Page.find( { 'roles.admin.uid': req.body.userPageUid } ).select(['uid', 'name', 'pendingRoles'])

    let roles = ['admin', 'editor', 'moderator']
    let pendings = []
    pages.forEach((page)=>{
        //console.log("page", page)
        roles.forEach((role)=>{
            page.pendingRoles[role].forEach((pending)=>{
                pendings.push({'role' : role, 
                                uid: pending.uid, name: pending.name,
                                onPageUid: page.uid, onPageName: page.name,
                              })
            })
        })
    })
    return res.json( { error: false, pendings: pendings } )
  })

  router.post('/get-stat-url-global', auth,  async (req, res) => {
    console.log("-------------------------")
    console.log("/broadcast/get-stat-url-global", req.body.domaineName)

    let instanceService = new InstanceService()
    await instanceService.initDayStat()

    let date30D = new Date()
    date30D.setDate(date30D.getDate() - 30)

    let stats = await StatUrl.find({ clientDomaineName: req.body.domaineName, 
                                    date: { '$gt': date30D } })
                                .sort({'inx':-1})

    console.log("/broadcast/get-stat-url-global nbStats:", stats.length)
    
    //stats = stats.reverse()

    return res.json({ error: false, stats: stats })
})

router.post('/admin-get-page-blacklisted', auth,  async (req, res) => {
    console.log("-------------------------")
    console.log("/broadcast/admin-get-page-blacklisted", req.body.nodeName)

    //cherche toutes les pages qui ont blacklisté une page venant de l'instance nodeName
    let pages = await Page.find({ 'blacklist.uid' : new RegExp(".*"+req.body.nodeName, "i") })
                          .limit(50)

    console.log("/broadcast/admin-get-page-blacklisted:1", pages.length)

    //pour chaque page, récupère l'uid des pages blacklistées
    let pagesBL = []
    pages.forEach((p)=>{
        p.blacklist.forEach((bl)=>{
            if(core.iName(bl.uid) == req.body.nodeName) pagesBL.push(bl.uid)
        })
    })
    console.log("/broadcast/admin-get-page-blacklisted:2", pagesBL.length)
    
    return res.json({ error: false, pages: pagesBL })
})


router.post('/admin-save-mobile-app', auth,  async (req, res) => {
    console.log("-------------------------")
    console.log("/broadcast/admin-save-mobile-app", req.body.appName)

    let mApp = new MobileApp()
    mApp.name = req.body.appName
    mApp.description = req.body.appDescription
    mApp.OS = req.body.appOS
    mApp.version = "0.0.1"

    let date = new Date()
    mApp.created = date
    mApp.updated = date
    
    mApp.save()
    console.log("new App saved !", mApp.name, mApp.version)

    return res.json({ error: false})
})



router.post('/admin-maj-mobile-app', auth,  async (req, res) => {
    console.log("-------------------------")
    console.log("/broadcast/admin-maj-mobile-app", req.body.appName)

    let mApp = await MobileApp.findOne({ name: req.body.appName })
    mApp.version = req.body.newVersion
    mApp.description = req.body.appDescription

    let date = new Date()
    mApp.updated = date
    
    mApp.save()
    console.log("MAJ : MOBILE APP UPDATED !", mApp.name, mApp.version)

    return res.json({ error: false})
})


router.get('/last-mobile-app-version/:appName', auth,  async (req, res) => {
    console.log("-------------------------")
    console.log("/broadcast/last-mobile-app-version", req.params.appName)

    let mApp = await MobileApp.findOne({ name: req.params.appName })
    return res.json({ error: false, app: mApp })
})




/******************************** LIVE **********************************/



router.post('/open-live-stream', auth,  async (req, res) => {
    console.log("-------------------------")
    console.log("/broadcast/open-live-stream", req.body.pageUid)

    let page = await Page.findOne({ 'uid' : req.body.pageUid })
    if(page == null) return res.json({ error: true })
    //console.log("/broadcast/open-live-stream Page found :", page.name)


    let isEditor = core.isRole(req.body.userPageUid, page, 'editor')
    if(!isEditor) 
    return res.json({ error: false, 
                        msg: req.body.userPageUid + " is not editor or friend with " + page.uid 
                    })

    let pageService = new PageService()
    let live = await pageService.findLiveForPage(req.body.pageUid, req.body.userPageUid, page.type)
    if(live == null){
        let pageUidRef = pageService.getPageUidRef(req.body.pageUid, req.body.userPageUid, page.type)
        if(page.type == "user"){ //si on n'a pas trouvé le live d'un user 
            const rootNode = await Instance.findOne({'isLocal': true}) 
            if(core.iName(pageUidRef) != rootNode.name) //et si l'uid ne pointe pas vers ce serveur
                return res.json({ error: true, page: page, 
                                  pageUidRef: pageUidRef, flagCreateLive: true }) //retourne une erreur pour créer le live sur le serveur initial
        }
        live = await pageService.createLive(pageUidRef)
        
    }else{
        console.log("Found Live")
    }
    
    return res.json({ error: false, page: page, live: live })
})



router.post('/send-live-message', auth,  async (req, res) => {
    console.log("-------------------------")
    console.log("/broadcast/send-live-message", req.body.liveUid)

    let live = await Live.findOne({ 'uid' : req.body.liveUid })
    if(live == null) return res.json({ error: true, liveMsg: null })
    //console.log("/broadcast/send-live-message Live found :", live.uid)


    /** TODO : check if category and channel exist */
    /** TODO : check permission to send msg here */

    let liveMsg = new LiveMsg()
    liveMsg.uid = core.getRandUid(core.iName(req.body.liveUid)),
    liveMsg.liveUid = req.body.liveUid
    liveMsg.channelUid = req.body.channelUid
    liveMsg.categoryUid = req.body.categoryUid
    liveMsg.parentPageUid = live.parentPageUid
    liveMsg.text = req.body.text
    liveMsg.author = req.body.author

    let todate = new Date()
    liveMsg.created = todate
    liveMsg.updated = todate
    
    liveMsg.save()

    let channelName = ""

    live.categories.forEach((category, a) => {
        if(category.uid == req.body.categoryUid){
            category.channels.forEach((channel, b) => {
                if(channel.uid == req.body.channelUid){
                    channelName = channel.name
                    //mise à jour de la date du dernier message dans ce channel
                    live.categories[a].channels[b].dateLastMsg = todate
                    let found = false
                    live.categories[a].channels[b].dateLastOpen.forEach((dataOpen, c)=>{
                        if(dataOpen.uid == req.body.author.uid){
                            live.categories[a].channels[b].dateLastOpen[c].date = new Date()
                            found = true
                        }
                    })
                    if(!found){
                        live.categories[a].channels[b].dateLastOpen.push({
                            uid: req.body.userPageUid,
                            date: new Date()
                        })
                    }
                    if(channel.mentions == null)
                        live.categories[a].channels[b].mentions = []

                    req.body.mentions.forEach((mention)=>{
                        live.categories[a].channels[b].mentions.push(mention)
                        if(channel.mentions.length > 100) //limite la taille de la liste des mentions à 100
                            live.categories[a].channels[b].mentions.splice(0, 1)
                    })                    
                }
            })
        }
    })
    live.dateLastMsg = todate
    live.markModified('categories')
    live.save()

    liveMsg.channelName = channelName

    //send alert by socket to each editor/member of the parentPage
    let page = await Page.findOne({ 'uid' : live.parentPageUid })
    if(page == null){ //aucune page ne correspond à live.parentPageUid : c'est un live privé entre 2 user (donc pas de page Parente)  
        page = await Page.findOne({ 'uid' : req.body.toPageUid })
        if(page != null){
            liveMsg.livePageName = page.name
            liveMsg.livePageType = page.type
        }
        req.ws.emit(req.body.toPageUid, "receive-liveMsg", { liveMsg: liveMsg, pageUid: live.parentPageUid, mentions: req.body.mentions })
    }else{    
        liveMsg.livePageName = page.name
        liveMsg.livePageType = page.type
        page.roles.editor.forEach((member) =>{
            if(member.uid != req.body.author.uid){
                req.ws.emit(member.uid, "receive-liveMsg", { liveMsg: liveMsg, pageUid: live.parentPageUid, mentions: req.body.mentions }) //send pageUid to show avatar if needed
            }})
    }

    return res.json({ error: false, liveMsg: liveMsg })
})

router.post('/open-live-channel', auth,  async (req, res) => {
    console.log("-------------------------")
    console.log("/broadcast/open-live-channel", req.body.liveUid)

    let liveService = new LiveService()
    let json = await liveService.openLiveChannel(req.body)
    res.json(json)
    
})

router.post('/create-live-channel', auth,  async (req, res) => {
    console.log("-------------------------")
    console.log("/broadcast/create-live-channel", req.body.liveUid)

    /* TODO : SECURE RIGHT TO CREATE ONLY FOR ADMIN AND MODERATOR */

    let live = await Live.findOne({ 'uid' : req.body.liveUid })
    if(live == null) return res.json({ error: true, live: null })
    console.log("/broadcast/create-live-channel Live found :", live.uid)

    let newUid = core.getRandUid(core.iName(req.body.liveUid))
    live.categories.forEach((category, i) => {
        if(category.uid == req.body.categoryUid){
            live.categories[i].channels.push({
                                uid: newUid,
                                name: req.body.channelName,
                                created: new Date(),
                                dateLastMsg: new Date(),
                                dateLastOpen: []
            })
        }
    })
    live.markModified('categories')
    live.save()

    return res.json({ error: false, live: live, newChannelUid: newUid })
})


router.post('/create-live-category', auth,  async (req, res) => {
    console.log("-------------------------")
    console.log("/broadcast/create-live-category", req.body.liveUid)

    /* TODO : SECURE RIGHT TO CREATE ONLY FOR ADMIN AND MODERATOR */

    let live = await Live.findOne({ 'uid' : req.body.liveUid })
    if(live == null) return res.json({ error: true, live: null })
    console.log("/broadcast/create-live-category Live found :", live.uid)

    let newUid = core.getRandUid(core.iName(req.body.liveUid))
    live.categories.push({
        uid: newUid,
        name: req.body.categoryName,
        channels: []
    })
    live.markModified('categories')
    live.save()

    return res.json({ error: false, live: live })
})

router.post('/edit-live-channel', auth,  async (req, res) => {
    console.log("-------------------------")
    console.log("/broadcast/edit-live-channel", req.body.liveUid)

    let live = await Live.findOne({ 'uid' : req.body.liveUid })
    if(live == null) return res.json({ error: true, live: null })
    console.log("/broadcast/edit-live-channel Live found :", live.uid)

    let parentPage = await Page.findOne({ 'uid' : live.parentPageUid })
    if(parentPage == null) return res.json({ error: true, parentPage: null })

    //secure only for moderator and admin
    let isAdmin = core.isRole(req.body.userPageUid, parentPage, 'admin')
    let isModerator = core.isRole(req.body.userPageUid, parentPage, 'moderator')
    if(!isAdmin && !isModerator) return res.json({ error: true, parentPage: null })

    live.categories.forEach((category, i) => {
        if(category.uid == req.body.catUid){
            live.categories[i].channels.forEach((chan, j) => {
                if(chan.uid == req.body.chanUid)
                    live.categories[i].channels[j].name = req.body.chanName            
            })
        }
    })
    live.markModified('categories')
    await live.save()
    return res.json({ error: false })
})


router.post('/edit-live-category', auth,  async (req, res) => {
    console.log("-------------------------")
    console.log("/broadcast/edit-live-category", req.body)

    let live = await Live.findOne({ 'uid' : req.body.liveUid })
    if(live == null) return res.json({ error: true, live: null })
    console.log("/broadcast/edit-live-category Live found :", live.uid)

    let parentPage = await Page.findOne({ 'uid' : live.parentPageUid })
    if(parentPage == null) return res.json({ error: true, parentPage: null })

    //secure only for moderator and admin
    let isAdmin = core.isRole(req.body.userPageUid, parentPage, 'admin')
    let isModerator = core.isRole(req.body.userPageUid, parentPage, 'moderator')
    if(!isAdmin && !isModerator) return res.json({ error: true, parentPage: null })

    live.categories.forEach((category, i) => {
        if(category.uid == req.body.catUid){
            live.categories[i].name = req.body.catName
        }
    })
    live.markModified('categories')
    await live.save()
    return res.json({ error: false })
})


router.post('/delete-live-channel', auth,  async (req, res) => {
    console.log("-------------------------")
    console.log("/broadcast/delete-live-channel", req.body.liveUid)

    let live = await Live.findOne({ 'uid' : req.body.liveUid })
    if(live == null) return res.json({ error: true, live: null })
    console.log("/broadcast/delete-live-channel Live found :", live.uid)

    let parentPage = await Page.findOne({ 'uid' : live.parentPageUid })
    if(parentPage == null) return res.json({ error: true, parentPage: null })

    //secure only for moderator and admin
    let isAdmin = core.isRole(req.body.userPageUid, parentPage, 'admin')
    let isModerator = core.isRole(req.body.userPageUid, parentPage, 'moderator')
    if(!isAdmin && !isModerator) return res.json({ error: true, parentPage: null })

    live.categories.forEach((category, i) => {
        if(category.uid == req.body.catUid){
            live.categories[i].channels.forEach((chan, j) => {
                if(chan.uid == req.body.chanUid)
                    live.categories[i].channels.splice(j, 1)
                    
            })
        }
    })
    live.markModified('categories')
    await live.save()
    return res.json({ error: false })
})


router.post('/delete-live-category', auth,  async (req, res) => {
    console.log("-------------------------")
    console.log("/broadcast/delete-live-category", req.body)

    let live = await Live.findOne({ 'uid' : req.body.liveUid })
    if(live == null) return res.json({ error: true, live: null })
    console.log("/broadcast/delete-live-category Live found :", live.uid)

    let parentPage = await Page.findOne({ 'uid' : live.parentPageUid })
    if(parentPage == null) return res.json({ error: true, parentPage: null })

    //secure only for moderator and admin
    let isAdmin = core.isRole(req.body.userPageUid, parentPage, 'admin')
    let isModerator = core.isRole(req.body.userPageUid, parentPage, 'moderator')
    if(!isAdmin && !isModerator) return res.json({ error: true, parentPage: null })

    live.categories.forEach((category, i) => {
        if(category.uid == req.body.catUid){
            live.categories.splice(i, 1)
        }
    })
    live.markModified('categories')
    await live.save()
    return res.json({ error: false })
})


router.post('/edit-live-msg', auth,  async (req, res) => {
    console.log("-------------------------")
    console.log("/broadcast/edit-live-msg", req.body)

    let liveMsg = await LiveMsg.findOne({ 'uid' : req.body.liveMsgUid })
    if(liveMsg == null) return res.json({ error: true, liveMsg: null })
    console.log("/broadcast/edit-live-msg liveMsg found :", liveMsg!=null)

    let live = await Live.findOne({ 'uid' : liveMsg.liveUid })
    if(live == null) return res.json({ error: true, live: null })
    let isAuthor = (liveMsg.author.uid == req.body.userPageUid)

    //let parentPage = await Page.findOne({ 'uid' : live.parentPageUid })
    let pageService = new PageService()
    let parentPage = await pageService.getBroadPage(live.parentPageUid)
    if(parentPage == null) //aucune page ne correspond à live.parentPageUid : c'est un live privé entre 2 user (donc pas de page Parente)  
        parentPage = await pageService.getBroadPage(req.body.toPageUid)

    //console.log("parentPage ??", parentPage.name)
    if(!isAuthor){ //si ce n'est pas l'auteur, vérifie qu'il est admin ou moderateur
        if(parentPage == null) return res.json({ error: true, parentPage: null })
        //secure only for moderator and admin
        let isAdmin = core.isRole(req.body.userPageUid, parentPage, 'admin')
        let isModerator = core.isRole(req.body.userPageUid, parentPage, 'moderator')
        if(!isAdmin && !isModerator) return res.json({ error: true, parentPage: null })
    }

    liveMsg.text = req.body.liveMsgText
    liveMsg.updated = new Date()
    await liveMsg.save()

    if(parentPage.type != 'user'){
        parentPage.roles.editor.forEach((page) =>{
            req.ws.emit(page.uid, "edited-liveMsg", { liveMsg: liveMsg })
        })
    }else{
        req.ws.emit(parentPage.uid, "edited-liveMsg", { liveMsg: liveMsg })
    }

    return res.json({ error: false, liveMsg: liveMsg })
})


router.post('/delete-live-msg', auth,  async (req, res) => {
    console.log("-------------------------")
    console.log("/broadcast/delete-live-msg", req.body)

    let liveMsg = await LiveMsg.findOne({ 'uid' : req.body.liveMsgUid })
    if(liveMsg == null) return res.json({ error: true, liveMsg: null })
    console.log("/broadcast/delete-live-msg liveMsg found :", liveMsg)

    let live = await Live.findOne({ 'uid' : liveMsg.liveUid })
    if(live == null) return res.json({ error: true, live: null })
    let isAuthor = (liveMsg.author.uid == req.body.userPageUid)

    let pageService = new PageService()
    let parentPage = await pageService.getBroadPage(live.parentPageUid)
    if(parentPage == null) //aucune page ne correspond à live.parentPageUid : c'est un live privé entre 2 user (donc pas de page Parente)  
        parentPage = await pageService.getBroadPage(req.body.toPageUid)

    if(!isAuthor){ //si ce n'est pas l'auteur, vérifie qu'il est admin ou moderateur
        if(parentPage == null) return res.json({ error: true, parentPage: null })
        
        //secure only for moderator and admin
        let isAdmin = core.isRole(req.body.userPageUid, parentPage, 'admin')
        let isModerator = core.isRole(req.body.userPageUid, parentPage, 'moderator')
        if(!isAdmin && !isModerator) return res.json({ error: true, parentPage: null })
    }
    let msg = liveMsg
    await liveMsg.delete()

    if(parentPage.type != 'user'){
        parentPage.roles.editor.forEach((page) =>{
            req.ws.emit(page.uid, "deleted-liveMsg", { liveMsg: liveMsg })
        })
    }else{
        req.ws.emit(parentPage.uid, "deleted-liveMsg", { liveMsg: liveMsg })
    }

    return res.json({ error: false, liveMsg: msg })
})



router.post('/who-is-online', auth,  async (req, res) => {
    //console.log("-------------------------")
    //console.log("/broadcast/who-is-online") //, req.body.targets)
    
    let targets = req.body.targets
    let connected = []
    targets.forEach((uid) =>{
        if(req.ws.isOnline(uid)) connected.push(uid)
    })
    //console.log("connected :", connected.length)
    res.json( { error: false, connected: connected } );
})


router.post('/get-live-data', auth,  async (req, res) => {
    console.log("-------------------------")
    console.log("/broadcast/get-live-data") //, req.body.targets)

    let live = await Live.findOne({ 'parentPageUid' : req.body.pageUidRef })
    res.json( { error: false, live: live } );
})

router.post('/get-live-conversation', auth,  async (req, res) => {
    console.log("-------------------------")
    console.log("/broadcast/get-live-conversation") //, req.body.targets)

    let liveService = new LiveService()
    let conv = await liveService.getConversation(req.body, req.body.limit, req.body.skip)
    res.json( { error: false, conversation: conv } );
})


module.exports = router;