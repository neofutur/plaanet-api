const { Router } = require("express")
const config = require('config');

const core = require('../core/core');
const auth = require("../core/middleware/auth-admin");

const { User } = require('../models/User')
const Wallet = require('../models/Wallet')
const Instance = require('../models/Instance')
const StateHistory = require('../models/StateHistory')
const Transaction = require('../models/Transaction')
const Publication = require('../models/Publication')
const Comment = require('../models/Comment')
const Notification = require('../models/Notification')
const Page = require('../models/Page')
const MobileApp = require('../models/MobileApp')

const PageService = require("../services/page");

const router = Router();

router.get('/instance-info', auth,  async (req, res) => {
    console.log("-------------------------")
    console.log("/admin/instance-info")
    
    try{
        let instance = await Instance.findOne({ isLocal:true })
        const nbUsers = await User.countDocuments()
        const nbPosts = await Publication.countDocuments()
        const nbDays = await StateHistory.countDocuments()

        res.json({ instance: instance, 
                    nbUsers: nbUsers, 
                    nbPosts: nbPosts, 
                    nbDays: nbDays, 
                    env: config.get('env') });
    }catch(e){
        console.log("error", e)
        res.json({ error: true, instance: null, 
                   errorMsg: 'No instance found in this server' })
    }
})


router.post('/instance-create', auth, async (req, res) => {
    console.log("-------------------------")
    console.log("/admin/instance-create")
    try{
        const i = await Instance.findOne({ isLocal:true })
        if(i == null){
            const instance = new Instance({
                name: req.body.name,
                url: config.get('instance_url'),
                port: config.get('instance_port'),
                moneyMassLocal: 0,
                moneyMassGlobal: 0,
                //nbUserGlobal: 0,
                UDValue: 0,
                TQRunning: false
            })
            await instance.save();
            res.json({ error: false, instance: instance })
        }else{
            res.json({ error: true, instance: i, 
                       errorMsg: 'An instance allready exists in this server' })
        }
    }catch(e){
        console.log("error", e)
        res.json({ error: true, instance: null })
    }
})


router.post('/add-users-dev'/*, auth*/, async (req, res) => {
    console.log("-------------------------")
    console.log("/admin/instance-init-dev")

    if(config.get("env") != "dev"){
        console.log("error env is not dev")
        res.json({ error: true })
        return
    }


    try{
        const ins = await Instance.findOne({ isLocal:true })
        var nbUsers = req.body.nbUsers
        console.log("let's create", nbUsers, "new users !")

        let nbRealUsers = await User.countDocuments()
        for(var i=1; i<= nbUsers; i++){ 
            //console.log("ready to save user", i, nbUsers)
            //create current wallet (compte courant)
            let firstTrans = ins.moneyMassGlobal / nbRealUsers
            //let index = await Wallet.countDocuments()
            var name = "anonymous" + i
            let wallet = new Wallet({ 
                            uid: '',
                            type: 'current',
                            name: name,
                            isPublic: true,
                            account_u: firstTrans,
                            account_uni: 0,
                            pendingTransactions: [],
                            validatedTransactions: [],
                            created: new Date()  
                        });
            
            //create new user
            var user = new User({
                name: name,
                password: "anonymous",
                email: name + "@mail.com",
                isActive: true,
                isAdmin: false,
                wallets: [ wallet ]
            });

            wallet.uid = ins.name + ":" + wallet.id
            wallet.owner = user.id
            await wallet.save();
            await user.save();

            ins.moneyMassLocal += firstTrans
            ins.moneyMassGlobal = ins.moneyMassLocal
            ins.save()

            nbRealUsers++
        }

        console.log(">>> ", nbUsers, "USERS CREATED : OK")
        res.json({ error: false })

    }catch(e){
        console.log("error", e)
        res.json({ error: true, errorMsg: e })
    }
})


router.post('/get-wallets-data', auth, async (req, res) => {
    console.log("-------------------------")
    console.log("/admin/get-wallets-data")
    try{
        let walletsCurrent = await Wallet.find({'type': 'current'})
        let walletsCustom = await Wallet.find({'type': 'custom'})
    
        res.json({  error: false, 
                    walletsCurrent: walletsCurrent,
                    walletsCustom: walletsCustom, })
    }catch(e){
        console.log("error", e)
        res.json({ error: true, instance: null })
    }
})


router.post('/calc-money-mass', auth, async (req, res) => {
    console.log("-------------------------")
    console.log("/admin/calc-money-mass")
    try{
        let users = await User.find().populate('wallets')
        if(users != null){
            //var mm = 0
            /*users.forEach(user => {
                mm += user.wallets[0].account_u
            });*/
            const i = await Instance.findOne({ isLocal:true })
            //i.moneyMassLocal = mm
            //i.moneyMassGlobal = mm
            //i.nbUserGlobal = users.length
            i.UDValue = Instance.calcUniValue(i.moneyMassGlobal, users.length)
            i.save()

            res.json({  error: false, instanceState: i })
        }else{
            res.json({ error: true, 
                       errorMsg: 'No user found in this bdd' })
        }
    }catch(e){
        console.log("error", e)
        res.json({ error: true, instance: null })
    }
})


router.post('/work-ud', auth, async (req, res) => {
    console.log("-------------------------")
    console.log("/admin/work-ud")
    try{
        const i = await Instance.findOne({ isLocal: true })
        
        //only for dev
        i.moneyMassGlobal = i.moneyMassLocal
        
        let moneyAdded = 0
        const nbUsers = await User.countDocuments()

        //recalculate UDValue before send new money
        i.UDValue = Instance.calcUniValue(i.moneyMassGlobal, nbUsers)
        var UD = i.UDValue

        console.log("UD", UD)

        //REBASE
        console.log("UD", UD, "need to rebase ?", (i.moneyMassGlobal / nbUsers > 1000), i.moneyMassGlobal / nbUsers, ">", 1000)
        if(i.moneyMassGlobal / nbUsers > 1000){
            console.log("REBASE")
            await User.find().populate('wallets').then(users => {
                users.forEach(user => {
                    //console.log(user.name)
                    user.wallets[0].account_u = user.wallets[0].account_u / 100
                    user.wallets[0].save()
                });
                i.moneyMassLocal = i.moneyMassLocal / 100
                i.moneyMassGlobal = i.moneyMassLocal
                //moneyMass = i.moneyMassGlobal
                i.UDValue = Instance.calcUniValue(i.moneyMassGlobal, nbUsers)
                UD = i.UDValue
            })
        }

        const wbank = await Wallet.findOne({ type: "bank" })
        const average = i.moneyMassGlobal / nbUsers / i.UDValue
        const futurUD = Instance.calcUniValue(i.moneyMassGlobal+(nbUsers*UD), nbUsers)
        //SEND UD
       await User.find().populate('wallets').then(users => { 
            users.forEach(user => {
                 
                user.wallets[0].account_u += UD
                moneyAdded += UD
            

                let transaction = new Transaction({
                    amount: 1,
                    label: "UD",
                    senderUid: wbank.uid,
                    receiverUid: user.wallets[0].uid,
                    senderName: wbank.name,
                    receiverName: user.wallets[0].name,
                    senderUni: 0,
                    receiverUni: user.wallets[0].account_u / futurUD,
                    created: new Date(),
                    validated: new Date(),
                    status: 'validated'
                })

                user.wallets[0].validatedTransactions.push(transaction)
                transaction.save()
                user.wallets[0].save()
                
            });
        })
       
        i.moneyMassLocal += moneyAdded
        //only for dev
        i.moneyMassGlobal = i.moneyMassLocal
        i.UDValue = Instance.calcUniValue(i.moneyMassGlobal, nbUsers)
        
        let date = new Date()
        let nbd = await StateHistory.countDocuments()
        date.setDate(date.getDate() + nbd)
        
        let hist = new StateHistory({
            date: date,
            moneyMassLocal: i.moneyMassLocal,
            moneyMassGlobal: i.moneyMassGlobal,
            nbUsersLocal: nbUsers,
            nbUsersGlobal: nbUsers,
            UDValue: i.UDValue
        })
        hist.save()
        i.save()

        console.log(">>> UDW OK, money added :", moneyAdded, "average", average)
        
        res.json({  error: false, instance: i, nbUsers: nbUsers, date: date })
       
    }catch(e){
        console.log("error", e)
        res.json({ error: true, e: e })
    }

})


router.get('/update-network', auth,  async (req, res) => {
    console.log("-------------------------")
    console.log("/admin/update-network")
    
    try{
        //const network = await Instance.find().select("-nodeKey")
        let network = []
        core.broadcaster.get("/broadcast/instance-info",
            function(result){ console.log("res from /broadcast/instance-info", result.length)
                if(result != null){
                    network = result
                    //update local instance informations in db
                    let atts = ['name', 'nodeKey', 'url', 'port', 'portSocket', 
                                'city', 'state', 'position', 'clientUrl', 'clientPort', 'env',
                                'nbPosts', 'nbUsers', 'nbNotifs', 'admins']
                    network.forEach(async (node, i) => {
                        let localNode = await Instance.findOne({ url: node.url, port: node.port  })
                        if(localNode != null){
                            atts.forEach((att) => {
                               localNode[att] = node[att] 
                            })
                            await localNode.save()
                        }
                        if(i == network.length -1){
                            let finalNetwork = await Instance.find()
                            res.json({ error: false, network: finalNetwork });
                        }
                    })
                    //here can update local instance data
                }else{
                    console.log("error 1 on /broadcast/instance-info")
                }
            },
            function(res){
                console.log("error 2 on /broadcast/instance-info")
            },
            'instance'
        )
        //console.log(network)
        //res.json({ network: network });
    }catch(e){
        console.log("error", e)
        res.json({ error: true, network: false, 
                   errorMsg: 'No instance found in this server' })
    }
})


router.post('/lock-instance', auth,  async (req, res) => {
    console.log("-------------------------")
    console.log("/admin/lock-instance")
    
    try{
        let instance = await Instance.findOne({ name: req.body.name, 
                                                url: req.body.url,
                                                port: req.body.port })

        if(instance.lockIn == null) instance.lockIn = false
        if(instance.lockOut == null) instance.lockOut = false
        if(req.body.typeInOut == 'in')  instance.lockIn  = !instance.lockIn
        if(req.body.typeInOut == 'out') instance.lockOut = !instance.lockOut

        instance.save()

        res.json({ error: false, instance: instance })
    }catch(e){
        console.log("error", e)
        res.json({ error: true, instance: null, 
                   errorMsg: 'No instance found in this server' })
    }
})

router.get('/check-instance-activity', auth,  async (req, res) => {
    console.log("-------------------------")
    console.log("/admin/heck-instance-activity")
    
    let resActivity = await core.broadcaster.checkActivity()
    console.log("/admin/check-instance-activity resCheck", resActivity.network.length)

    res.json({ error: false, network: resActivity.network })
})


router.post('/save-state-history', auth,  async (req, res) => {
    console.log("-------------------------")
    console.log("/admin/save-state-history")
    
    try{
        let instance = await Instance.findOne({ isLocal: true })

        //if a SH is already save today : no save any other, so exit
        let date1D = new Date()
        date1D.setDate(date1D.getDate() - 1);
        let s = await StateHistory.findOne({ date: { '$gt' : date1D } })
        if(s != null){
            console.log("error history already save today")
            res.json({ error: true })
        }

        //initialize first state to 0 (if no document in collection)
        let shCount = await StateHistory.countDocuments()
        if(shCount == 0){
            let sh0 = new StateHistory({
                date: new Date(),
                nbUsers: 0,
                nbPosts: 0,
                nbNotifs: 0
            })
            await sh0.save()
        }

        //create new SH for today
        let sh = new StateHistory({
            date: new Date(),
            nbUsers: instance.nbUsers,
            nbPosts: instance.nbPosts,
            nbNotifs: instance.nbNotifs
        })
        await sh.save()

        console.log("/admin/save-state-history", sh.date)
        res.json({ error: false })
    }
    catch(e){
        console.log("error /admin/save-state-history")
        res.json({ error: true })
    }
})


// remove comment if you need to run this root on your dev serveur (never in production)
// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
// router.post('/simulate-state-history', auth,  async (req, res) => {
//     console.log("-------------------------")
//     console.log("/admin/simulate-state-history")
    
//     try{
//         let instance = await Instance.findOne({ isLocal: true })

//         await StateHistory.remove()

//         for(let i = 0; i<30; i++){
//             let date = new Date()
//             let sh = new StateHistory({
//                 date: date.setDate(date.getDate() + i),
//                 nbUsers: instance.nbUsers + ((i+1)*3),
//                 nbPosts: instance.nbPosts + ((i+2)*8),
//                 nbNotifs: instance.nbNotifs + ((i+3)*13)
//             })
//             await sh.save()
//             console.log("/admin/save-state-history", sh.date)
//         }

//         res.json({ error: false })
//     }catch(e){
//         console.log("error /admin/save-state-history")
//         res.json({ error: true })
//     }
// })


router.get('/get-state-history',  async (req, res) => {
    console.log("-------------------------")
    console.log("/admin/get-state-history")
    
    try{
        let stateHistory = await StateHistory.find().limit(30).sort({ 'date' : -1 })
        stateHistory = stateHistory.reverse()
        res.json({ error: false, stateHistory: stateHistory })
    }catch(e){
        console.log("error /admin/save-state-history")
        res.json({ error: true })
    }
})

router.post('/delete-traces', auth,  async (req, res) => {
    console.log("-------------------------")
    console.log("/admin/delete-traces")
    
    let regxNodeName =  new RegExp(".*"+req.body.nodeName, "i")
    let pageService = new PageService()
    
    /* NOTIFICATIONS */
    await Comment.deleteMany({ 'author.uid' : regxNodeName })
    await Publication.deleteMany({ 'sharedPostUid' : regxNodeName })

    let notifs = await Notification.find({ 'authors.uid' : regxNodeName })
    notifs.forEach(notif => {
        let p = -1
        notif.authors.forEach((author, x) => {
            if(author.uid.indexOf(req.body.nodeName) === 0) p = x
        })
        console.log("notifs A", notif.authors.length)
        if(p > -1) notif.authors.splice(p, 1)
        console.log("notifs B", notif.authors.length)

        notif.markModified('authors')
        notif.save()
    })

    /* ROLES */
    let inRoles = await Page.find( { '$or' : [
        { "roles.admin.uid" : regxNodeName },
        { "roles.moderator.uid" : regxNodeName },
        { "roles.editor.uid" : regxNodeName },
        { "inviteRoles.admin.uid" : regxNodeName },
        { "inviteRoles.moderator.uid" : regxNodeName },
        { "inviteRoles.editor.uid" : regxNodeName },
        { "pendingRoles.admin.uid" : regxNodeName },
        { "pendingRoles.moderator.uid" : regxNodeName },
        { "pendingRoles.editor.uid" : regxNodeName }
    ]}).select(["uid", "name", "roles", "pendingRoles", "inviteRoles"])

    inRoles.forEach((page, x) =>{
        //let id = {uid: page.uid }
        let RT = ['admin', 'moderator', 'editor']  
        RT.forEach((rt) => {
            page.roles = pageService.deleteRoleTrace(page, 'roles', rt, req.body.nodeName)
            page.pendingRoles = pageService.deleteRoleTrace(page, 'pendingRoles', rt, req.body.nodeName)
            page.inviteRoles = pageService.deleteRoleTrace(page, 'inviteRoles', rt, req.body.nodeName)
        })

        page.markModified('roles')
        page.markModified('inviteRoles')
        page.markModified('pendingRoles')
        page.save()
    })

    /* RELATIONS */
    let follows = await Page.find({ 'follows.uid' : regxNodeName }).select(["uid", "follows"])
    let blacklist = await Page.find({ 'blacklist.uid' : regxNodeName }).select(["uid", "blacklist"])
    let followers = await Page.find({ 'followers' : regxNodeName }).select(["uid", "followers"])
        
    follows.forEach((page, p) =>{ let x = -1;
        page.follows.forEach((f, z) =>{ if(regxNodeName.test(f.uid)) x = z })
        if(x > -1) page.follows.splice(x , 1)
        page.markModified('follows'); page.save()
    })
    blacklist.forEach((page, p) =>{ let x = -1;
        page.blacklist.forEach((f, z) =>{ if(regxNodeName.test(f.uid)) x = z })
        if(x > -1) page.blacklist.splice(x , 1)
        page.markModified('blacklist'); page.save()
    })
    followers.forEach((page, p) =>{ let x = -1;
        console.log("----------- page.followers", page.followers, page.followers.length)
        page.followers.forEach((f, z) =>{ console.log("f", f, z, regxNodeName.test(f)); if(regxNodeName.test(f)) x = z })
        if(x > -1) page.followers.splice(x , 1)
        console.log("page.followers", x, page.followers.length)
        page.markModified('followers'); page.save()
    })

    res.json({ error: false })
})

router.post('/get-traces-infos', auth, async (req, res) => {
    console.log("-------------------------")
    console.log("/admin/get-traces-infos")

    let regxNodeName =  new RegExp(".*"+req.body.nodeName, "i")
    
    let comments = await Comment.find({ 'author.uid' : regxNodeName }).select("author.uid")
    let sharedPosts = await Publication.find({ 'sharedPostUid' : regxNodeName }).select("sharedPostUid")
    let notifs = await Notification.find({ '$or': [
                                                { 'authors.uid' : regxNodeName }
                                            ]
                                        }).select("_id")
    
    let follows = await Page.find({ 'follows.uid' : regxNodeName }).select("uid")
    let blacklist = await Page.find({ 'blacklist.uid' : regxNodeName }).select("uid")
    let followers = await Page.find({ 'followers' : regxNodeName }).select("uid")

    
    let inRoles = await Page.find( { '$or' : [
        { "roles.admin.uid" : regxNodeName },
        { "roles.moderator.uid" : regxNodeName },
        { "roles.editor.uid" : regxNodeName },
        { "inviteRoles.admin.uid" : regxNodeName },
        { "inviteRoles.moderator.uid" : regxNodeName },
        { "inviteRoles.editor.uid" : regxNodeName },
        { "pendingRoles.admin.uid" : regxNodeName },
        { "pendingRoles.moderator.uid" : regxNodeName },
        { "pendingRoles.editor.uid" : regxNodeName }
    ]}).select(["uid", "roles"])
   
    console.log("inRoles ?", inRoles)

    res.json({ error: false, 
               nodeName: req.body.nodeName,

                comments: comments,
                sharedPosts: sharedPosts,
                notifs: notifs,

                follows: follows,
                blacklist: blacklist,
                followers: followers,
                
                inRoles: inRoles,
            })
   
})



router.post('/disconnect-node', auth,  async (req, res) => {
    console.log("-------------------------")
    console.log("/admin/disconnect-node")
    
    try{
        let node = await Instance.deleteOne({ name: req.body.nodeName })
        res.json({ error: false })
    }catch(e){
        console.log("error /admin/disconnect-node")
        res.json({ error: true })
    }
})


router.post('/get-pages', auth,  async (req, res) => {
    console.log("-------------------------")
    console.log("/get-pages", req.body)
    let request = {}

    let search = req.body.search
    //console.log("req.body.search", search)
    if(search != "" 
    && search != null
    && search.indexOf("#") == -1)
        request.name = new RegExp(".*"+req.body.search.toLowerCase().trim(), "i")


    if(req.body.pageType != "" && req.body.pageType != null)
    request.type = req.body.pageType

    //console.log("request", request)
    let map = await Page.find(request)
                        .sort( { name: 1 } )
                        .limit(50)
                  
    //let totalUsers = 0
    //console.log("map length", map.length)
    res.json( { error: false,   pages: map } );
})

/** MOBILE APPLICATION VERSIONING */

router.post('/save-mobile-app', auth,  async (req, res) => {
    console.log("-------------------------")
    console.log("/admin/save-mobile-app", req.body)
    
    try{ //broadcast request (to each instance of the network)

        //check if you are admin of the root node
        const i = await Instance.findOne({ isLocal:true })
        if(i.url+i.port != config.get('root_node').url+config.get('root_node').port) 
            return res.json({  error: true })

        let rootNode = config.get('root_node')
        core.post(rootNode.url + ":" + rootNode.port +'/broadcast/admin-save-mobile-app', 
            { appName: req.body.appName,
                appDescription: req.body.appDescription,
                appOS: req.body.appOS,
            }, //params
            async function(dataRes){ // == then (after response from all instances)
                //if network sent data
                if(dataRes.data.error != true){
                    res.json({  error: false })
                }else{
                    console.log("error 1 : dataRes == null")
                    res.json({ error: true })
                }
                    
            },function(result){ // == catch
                console.log("error 2 : fCatch", result)
                //res.json({ error: true, wallets: null, 
                           //errorMsg: 'An error occured', result: result })
            })
    }catch(e){
        console.log("error 3")
        res.json({ error: true, e:e })
    }
})

router.post('/maj-mobile-app', auth,  async (req, res) => {
    console.log("-------------------------")
    console.log("/admin/maj-mobile-app", req.body)
    
    try{ //broadcast request (to each instance of the network)
        
        //check if you are admin of the root node
        const i = await Instance.findOne({ isLocal:true })
        if(i.url+i.port != config.get('root_node').url+config.get('root_node').port) 
            return res.json({  error: true })

        let rootNode = config.get('root_node')
        core.post(rootNode.url + ":" + rootNode.port +'/broadcast/admin-maj-mobile-app', 
            {   appName: req.body.appName, 
                appDescription: req.body.appDescription,
                newVersion: req.body.newVersion,
            }, //params
            async function(dataRes){ // == then (after response from all instances)
                //if network sent data
                if(dataRes.data.error != true){
                    res.json({  error: false })
                }else{
                    console.log("error 1 : dataRes == null")
                    res.json({ error: true })
                }
                    
            },function(result){ // == catch
                console.log("error 2 : fCatch", result)
                //res.json({ error: true, wallets: null, 
                           //errorMsg: 'An error occured', result: result })
            })
    }catch(e){
        console.log("error 3")
        res.json({ error: true, e:e })
    }
})


router.post('/delete-mobile-app', auth,  async (req, res) => {
    console.log("-------------------------")
    console.log("/admin/delete-mobile-app", req.body)
    
    //check if you are admin of the root node
    const i = await Instance.findOne({ isLocal:true })
    if(i.url+i.port != config.get('root_node').url+config.get('root_node').port) 
        return res.json({ error: true })
        
    let resDelete = await MobileApp.deleteOne({ name: req.body.appName })
    return res.json({ error: false, res: resDelete })
    
})



router.post('/get-mobile-apps', auth,  async (req, res) => {
    console.log("-------------------------")
    console.log("/admin/get-mobile-apps", req.body)
    
    //check if you are admin of the root node
    const i = await Instance.findOne({ isLocal:true })
    if(i.url+i.port != config.get('root_node').url+config.get('root_node').port) 
        return res.json({ error: true })
        
    let apps = await MobileApp.find()
    return res.json({ error: false, mobileApps: apps })
    
})




module.exports = router;