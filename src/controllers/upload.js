const fs = require('fs');
const config = require('config');
const express = require('express');
const multer = require('multer');
const sharp = require('sharp');
const jwt = require('jsonwebtoken');
const uuid = require('uuid').v4;

const core = require('../core/core');
const auth = require('../core/middleware/auth');
const Page = require('../models/Page');
const File = require('../models/File');

const router = express.Router();

const PUBLIC_FOLDER_PATH = config.get('public_folder_path') || './public';

/* ************* IMAGE ************* */

const fileFilter = (req, file, cb) => {
    console.log("fileFilter")
    const allowedTypes = ["image/jpeg", "image/jpg", "image/png"];
    if (!allowedTypes.includes(file.mimetype)) {
        const error = new Error("Incorrect file");
        error.code = "INCORRECT_FILETYPE";
        return cb(error, false)
    }
    cb(null, true);
}

const uploadImgPost = multer({
    dest: `${PUBLIC_FOLDER_PATH}/uploads/post`,
    fileFilter,
    limits: {
        fileSize: 5000000
    }
});



router.post('/image-post', uploadImgPost.single('image-post'), (req, res) => {
    console.log("-------------------------")
    console.log("/upload/image-post", req.file)

    //if(req.user == null)  res.json({ error: true })

    const imagePath = req.file.path
    const imageName = req.file.filename
    sharp(imagePath).resize(600).toBuffer(function (err, buf) {
        console.log("error ?")
        if (err) return next(err)
        const image =  buf.toString('base64');
        console.log("try to write", imageName);
        fs.writeFile(imagePath, image, {encoding: 'base64'},  function(err){
            console.log("error in writeFile", err)
            if(err != null) return res.json({ error: true, error: err })
        });
        // This will replace your original image with compressed once.
    });

    console.log("no error", imageName)
    res.json({ error: false, imageName: imageName })
})



/* ************* PDF ************* */

const PDFFilter = (req, file, cb) => {
   // console.log("PDFFilter")
    const allowedTypes = ["application/pdf"];
   // console.log("file.mimetype", file.mimetype)
    if (!allowedTypes.includes(file.mimetype)) {
        const error = new Error("Incorrect file");
        error.code = "INCORRECT_FILETYPE";
        return cb(error, false)
    }
    cb(null, file.filename);
}
var storage = multer.diskStorage({
    destination: function (req, file, cb) {
        //console.log("request1", req.body)
        //console.log("request2", file)
      let path = `${PUBLIC_FOLDER_PATH}/uploads/pdf/` + req.body.signedUid
      if(!fs.existsSync(path)){ fs.mkdirSync(path) }
      cb(null, path)
    },
    filename: function (req, file, cb) {
      const fileName = `${uuid()}.pdf`;
      cb(null, fileName);
    }
  })

const uploadPDF = multer({
    //dest: './public/uploads/pdf',
    storage: storage,
    PDFFilter,
    limits: {
        fileSize: 50000000
    }
});


router.post('/pdf-post', auth, uploadPDF.single('pdf-post'), async (req, res) => {
    console.log("-------------------------")
    console.log("/upload/pdf-post", req.body)

    let authorPage = await Page.findOne( { uid: req.body.signedUid } )
    let nodePath = await core.getNodePath(authorPage.uid)

    let file = new File()

    file.uid = core.iName(req.body.signedUid) + ":" + file._id
    file.name = req.file.originalname
    file.type = "PDF"
    file.author = authorPage
    file.date = new Date()
    file.path = nodePath + "/uploads/pdf/" + req.body.signedUid + "/" + req.file.filename;

    file.save()
    res.json({ error: false, pdfName: req.file.originalname, pdfId: file._id })
})





router.post('/avatar', auth, async (req, res) => {
    console.log("-------------------------")
    console.log("/upload/avatar", req.body.pageUid)

    let checkPage = await Page.findOne( { owner: req.user._id, uid: req.body.pageUid } )
    if(checkPage == null){
        res.json({ error: true })
        return
    }

    let base64Image = req.body.avatar.split(';base64,').pop();
    let now = Date.now()

    let path = "/uploads/avatar/" + checkPage.uid + /*"-" + now +*/ ".png" 

    fs.writeFile(`${PUBLIC_FOLDER_PATH}/${path}`, base64Image, {encoding: 'base64'}, function(err) {
        console.log('File created, error:', err);
        if(err != null) res.json({ error: true, err: err })
    });

    console.log("/upload/avatar OK", req.body.pageUid)

    checkPage.save()

    res.json({ error: false })
})

router.use((err, req, res, next) => {

    console.log("-------------------------")
    console.log("XXX", err.code)

    if (err.code === "INCORRECT_FILETYPE") {
      res.status(422).json({ error: 'File type not allowed' });
      return;
    }
    if (err.code === "LIMIT_FILE_SIZE") {
      res.status(422).json({ error: 'Allow file size is 500KB for image and 5000KB for PDF' });
      return;
    }


    res.status(422).json({ error: err.code });
  });

module.exports = router;