var express = require("express")
var router = express.Router()
var app = express()

const auth = require("../core/middleware/auth")

const { User, validate } = require('../models/User')
const Instance = require('../models/Instance')
const File = require('../models/File')
const Publication = require('../models/Publication')
const Notification = require('../models/Notification')
const Page = require('../models/Page')
const PageService = require("../services/page");
const PublicationService = require("../services/publication");


const core = require('../core/core')
var urlMetadata =require("url-metadata")

const _ = require('underscore')
var fs = require('fs')

router.post('/send-post', auth,  async (req, res) => {
    console.log("-------------------------")
    console.log("/publication/send-post")
    
    let authorPage = await Page.findOne( { type:'user', owner: req.user._id } )
    let targetPage = authorPage
    if(req.body.postSignedUid != null){
        let pageService = new PageService()
        targetPage = await pageService.getBroadPage(req.body.postSignedUid)
        //Page.findOne( { uid: req.body.postSignedUid } )
    }
    

    let kmToday = 0
    if(req.body.scopeType != 'followers'){
        kmToday = await core.getKmToday(req.body.postSignedUid)
        //console.log("kmToday", kmToday)
        if(core.kmMax - (kmToday + req.body.radius) < 0){
            res.json( { error: true, 
                        msg: 'You already send more than ' + core.kmMax + ' km today',
                        //kmMax: kmMax - kmLimit
                    } );
            return
        }
    }
    let hashtags = core.extractTags(req.body.text)

    let post = new Publication()
    post.text = req.body.text
    post.hashtags = hashtags
    post.embedMetaLinks = req.body.embedMetaLinks
    post.created = new Date()
    post.author = authorPage._id
    post.signed = authorPage._id
    post.target = { uid: targetPage.uid, name: targetPage.name, city: targetPage.city }
    post.sharable = req.body.sharable
    post.likes = []
    post.dislikes = []
    post.favorites = []
    post.comments = []
    post.viewers = []
    post.sharedPostUid = req.body.sharedPostUid
   
    if(req.body.imageName != null)
        post.imageName = req.body.imageName
    
    if(req.body.pdf != null)
        post.pdf = req.body.pdf

    if(req.body.scopeType != 'followers'){
        post.scopeType = "localised"
        post.coordinates = [req.body.lat, req.body.lng]
        post.radius = req.body.radius
        post.fromCity = req.body.fromCity
        post.scopeGeo = req.body.scopeGeo
    }else{
        post.scopeType = "followers"
        post.fromCity = req.body.fromCity
    }
    
    //set UID
    const rootNode = await Instance.findOne({'isLocal': true}) 
    post.uid = rootNode.name + ":" + post.id
    await post.save()

    const finalPost = await Publication.findOne({'uid': post.uid})
                                        .populate('signed').populate('author')

    console.log("finalPost.sharedPostUid", finalPost.sharedPostUid)
    if(finalPost.sharedPostUid != null){
        let nodePath = await core.getNodePath(finalPost.sharedPostUid)
        //console.log("try contact", nodePath + '/broadcast/publication-get-post', finalPost.sharedPostUid)
        core.post(nodePath + '/broadcast/publication-get-post', 
                        {   postUid : finalPost.sharedPostUid,
                            userPageCoordinates: authorPage.coordinates
                        },
            function(dataRes){ 
                console.log("dataRes", dataRes.data)
                //if network sent data
                if(dataRes != null && dataRes.data.error == false){
                    finalPost.postShared = dataRes.data.postObj
                    res.json( { error: false, 
                                post: finalPost, 
                                kmRemainToday: core.kmMax - kmToday } )
                }else{
                    console.log("error after share post")
                }
            },function(result){ // == catch
                console.log("error 2 after share post", result)
            })
    }else{
        res.json( { error: false, 
                    post: finalPost, 
                    kmRemainToday: core.kmMax - kmToday } )
    }
        
})

router.post('/send-first-post', auth,  async (req, res) => {
    console.log("-------------------------")
    console.log("/publication/send-first-post")
    
    let authorPage = await Page.findOne( { type:'user', owner: req.user._id } )
    let hashtags = core.extractTags(req.body.text)

    let post = new Publication()
    post.text = req.body.text
    post.hashtags = hashtags
    post.coordinates = [authorPage.coordinates[1], authorPage.coordinates[0]]
    post.radius = 300000
    post.fromCity = req.body.fromCity
    post.embedMetaLinks = req.body.embedMetaLinks
    post.created = new Date()
    post.author = authorPage._id
    post.signed = authorPage._id
    post.target = { uid: authorPage.uid, name: authorPage.name, city: authorPage.city }
    post.scopeType = "localised"
    post.scopeGeo = req.body.scopeGeo
    post.sharable = false
    post.isFirst = true
    post.likes = []
    post.dislikes = []
    post.favorites = []
    post.comments = []
    post.viewers = []
    
    //set UID
    const rootNode = await Instance.findOne({'isLocal': true}) 
    post.uid = rootNode.name + ":" + post.id
    await post.save()

    console.log("search first post", post.uid)
    const finalPost = await Publication.findOne({'uid': post.uid}).populate('signed').populate('author')
    res.json( { error: false, 
                post: finalPost } );
})

router.post('/send-post-private', auth,  async (req, res) => {
    console.log("-------------------------")
    console.log("/publication/send-post-private")
    
    let authorPage = await Page.findOne( { owner: req.user._id, type: 'user' } )
    if(req.body.postSignedUid != null) //signed by a page owned by the user
        authorPage = await Page.findOne( { uid: req.body.postSignedUid } )

    let hashtags = core.extractTags(req.body.text)

    let post = new Publication()
    post.title = req.body.title
    post.text = req.body.text
    post.hashtags = hashtags
    post.embedMetaLinks = req.body.embedMetaLinks
    post.created = new Date()
    post.author = authorPage._id
    post.signed = authorPage._id
    post.target = { uid: authorPage.uid, name: authorPage.name, city: authorPage.city }
    post.scopeType = "private"
    post.receivers = req.body.receivers
    post.receivers.push({ uid: authorPage.uid, name: authorPage.name })
    post.sharable = req.body.sharable
    post.likes = []
    post.dislikes = []
    post.favorites = []
    post.comments = []
    post.viewers = []


    if(req.body.imageName != null)
        post.imageName = req.body.imageName
        
    if(req.body.pdf != null)
        post.pdf = req.body.pdf
    
    //set UID
    const rootNode = await Instance.findOne({'isLocal': true}) 
    post.uid = rootNode.name + ":" + post.id
    await post.save()
    
    /* SEND NOTIF */
    //authorPage a liké une publication
    core.notify.send(req, {
        verb: "NEW_PRIVATE_MSG",
        authors: [{ uid: authorPage.uid,
                    name: authorPage.name }],
        whatObj : { uid: post.uid, type: 'publication', 
                    signedName: authorPage.name,
                    signedUid: authorPage.uid,
                    text: post.text.substr(0, 200) }
        
    }, ['receivers'], true)
    /* SEND NOTIF */
    
    const finalPost = await Publication.findOne({'uid': post.uid}).populate('signed').populate('author')
    res.json( { error: false, 
                post: finalPost } );
})

router.post('/add-receivers', auth,  async (req, res) => {
    console.log("-------------------------")
    console.log("/publication/add-receivers", req.body)
    
    try{ 
        let nodePath = await core.getNodePath(req.body.postUid)
        let userPage = await Page.findOne({ owner: req.user._id })
        console.log("try contact", nodePath + '/broadcast/publication-add-receivers', req.body.type, req.body.postUid)
        core.post(nodePath + '/broadcast/publication-add-receivers', 
            {   postUid : req.body.postUid,
                userPageUid: userPage.uid,
                receivers: req.body.receivers
            }, //params
            function(dataRes){ // == then (after response from all instances)
                console.log("dataRes", dataRes.data)
                res.json(dataRes.data)                    
            },function(result){ // == catch
                console.log("error 2 : fCatch", result)
            })
    }catch(e){
        console.log("error 3")
        res.json({ error: true, e:e })
    }
})

router.post('/edit-post', auth,  async (req, res) => {
    console.log("-------------------------")
    console.log("/publication/edit-post", req.body, ' ---- ', req.body.embedMetaLinks)
    
    try{ // to the instance of the post
        let nodePath = await core.getNodePath(req.body.postUid)
        let authPage = await Page.findOne( { owner: req.user._id, type:'user' } )
        let authorPage = { uid: authPage.uid }
        //console.log("try contact", nodePath + '/broadcast/publication-edit', req.body.postUid)
        core.post(nodePath + '/broadcast/publication-edit', 
            {   postUid: req.body.postUid,
                authorPage: authorPage,
                text: req.body.text
            }, 
            function(dataRes){ // == then (after response from all instances)
                //console.log("dataRes", dataRes.data)
                //if network sent data
                if(dataRes != null && dataRes.data.error == false){
                    //let post = dataRes.data.post
                    res.json({  error: false, 
                                postText: dataRes.data.postText, 
                                postTags: dataRes.data.postTags })
                }else{
                    console.log("error 1 : dataRes == null", dataRes.data)
                    res.json({ error: true })
                }
                    
            },function(result){ // == catch
                console.log("error 2 : fCatch", result)
            })
    }catch(e){
        console.log("error 3", e)
        res.json({ error: true, e:e })
    }

})

router.post('/delete-post', auth,  async (req, res) => {
    console.log("-------------------------")
    console.log("/publication/delete-post", req.body)

    try{ // to the instance of the post
        let nodePath = await core.getNodePath(req.body.postUid)
        let authPage = await Page.findOne( { owner: req.user._id, type:'user' } )
        let authorPage = { uid: authPage.uid }
        console.log("try contact", nodePath + '/broadcast/publication-delete', req.body.postUid)
        core.post(nodePath + '/broadcast/publication-delete', 
            {   postUid: req.body.postUid,
                authorPage: authorPage
            }, 
            function(dataRes){ // == then (after response from all instances)
                //console.log("dataRes", dataRes.data)
                //if network sent data
                if(dataRes != null && dataRes.data.error == false){
                    //let post = dataRes.data.post
                    res.json({  error: false })
                }else{
                    console.log("error 1 : dataRes == null", dataRes.data)
                    res.json({ error: true })
                }
                    
            },function(result){ // == catch
                console.log("error 2 : fCatch", result)
            })
    }catch(e){
        console.log("error 3", e)
        res.json({ error: true, e:e })
    }

})


router.post('/delete-pdf', auth,  async (req, res) => {
    console.log("-------------------------")
    console.log("/publication/delete-pdf", req.body)
    
    let posts = await Publication.find( { pdf: req.body.pdfId } ).populate("signed").populate("pdf")
    //si aucun post trouvé, il a dû être enregistré sous l'ancien format (avec le pdfName)
    if(posts.length == 0) 
        posts = await Publication.find( { pdfName: req.body.pdfName } ).populate("signed")

    console.log("delete ?", posts.length)
    if(posts != null && posts.length > 0){
        await Promise.all(posts.map(async (post, i) => {      
            console.log('processing delete for post', post.id) 
            let file = null
            if(post.pdf != null){
                console.log("lookin for PDF file id", post.pdf._id)
                file = await File.findOne( { _id: post.pdf._id } )                
            }else if(post.pdfName != null){
                console.log("lookin for PDF file name", post.pdfName)
                file = await File.findOne( { name: post.pdfName } )
            }

            if(file != null){
                console.log("remove pdf file", file.name)
                if(i == 0){ //delete le fichier seulement une fois, sinon : Error: ENOENT: no such file or directory
                    fs.unlink("./public/uploads/pdf/" + post.signed.uid + "/" + file.name, (err) => {
                        if (err) { console.error(err); return }
                    })         
                }       
                file.name = "**deleted**"
                file.save()
                console.log("modified post", post.uid, "and pdfId", req.body.pdfId)
            }else{
                console.log("no file found", post.pdf, post.pdfName)
            }
        }))
        res.json( { error: false } );
    }else{
        let file = await File.findOne( { _id: req.body.pdfId } )  
        if(file == null) file = await File.findOne( { name: req.body.pdfName } )

        if(file != null){               
            file.name = "**deleted**"
            file.save()
            res.json( { error: false } );
        }else{
            console.log("no file found", post.pdf, post.pdfName)
            res.json( { error: true } );
        }
    }

})



router.post('/leave-post', auth,  async (req, res) => {
    console.log("-------------------------")
    console.log("/publication/leave-post", req.body.postUid)

    try{ //broadcast request (to each instance of the network)
        let nodePath = await core.getNodePath(req.body.postUid)
        let userPage = await Page.findOne({ owner: req.user._id, type: 'user' })
        console.log("try contact", nodePath + '/broadcast/leave-post', req.body.type, req.body.postUid)
        core.post(nodePath + '/broadcast/leave-post', 
            {   postUid : req.body.postUid,
                userPageUid: userPage.uid
            }, //params
            function(dataRes){ // == then (after response from all instances)
                console.log("dataRes", dataRes.data)
                res.json(dataRes.data)                    
            },function(result){ // == catch
                console.log("error 2 : fCatch", result)
            })
    }catch(e){
        console.log("error 3")
        res.json({ error: true, e:e })
    }
})

router.post('/get-stream', auth,  async (req, res) => {
    console.log("-------------------------")
    console.log("/publication/get-stream") //, req.body)
    
    let userPage = await Page.findOne( { owner: req.user._id, type: "user" } )

    try{ //broadcast request (to each instance of the network)
        let myFollows = []
        if(req.body.streamOrigin == "follows"){
            //keep only uid attribute
            userPage.follows.forEach((page) => {
                myFollows.push(page.uid)
            })
        }
        let nbPerPage = 3
        let params = { 
            radius : req.body.radius,
            lat: req.body.lat,
            lng: req.body.lng,
            pageNumber: req.body.pageNumber,
            nbPerPage: nbPerPage,
            search: req.body.search,
            streamOrigin: req.body.streamOrigin,
            userPageUid: userPage.uid,
            userPageCoordinates: userPage.coordinates,
            follows: myFollows
          }

        core.broadcaster.post('/broadcast/get-stream', params, //params
            async function(dataRes){ // == then (after response from all instances)
                //if network sent data
                if(dataRes != null){
                    console.log("dataRes from /broadcast/get-stream")

                    //get stream from local bdd
                    publicationService = new PublicationService()
                    let streamRes = await publicationService.getStream(params)
                    //add post to stream from other nodes
                    dataRes = streamRes.stream.concat(dataRes)

                    //remove all post from blacklisted authors
                    let finalRes = []
                    dataRes.forEach((post)=>{
                        let pos = -1
                        userPage.blacklist.forEach((p, i)=>{
                            if(p.uid == post.signed.uid) pos = i
                        })
                        //if the author is in my black list: not pushed
                        if(pos == -1) finalRes.push(post)
                    })
                    //sort all res by "created" date
                    finalRes = _.sortBy(finalRes, 'created').reverse()
                    res.json({  error: false, 
                                stream: finalRes, 
                                nbPerPage: nbPerPage })
                }else{
                    console.log("error 1 : dataRes == null")
                    res.json({  error: true, stream: null, 
                                errorMsg: 'No stream found in this bdd' })
                }
                    
            },function(result){ // == catch
                console.log("error 2 : fCatch")
                //res.json({ error: true, wallets: null, 
                           //errorMsg: 'An error occured', result: result })
            },  
            'stream',//dataRes[id] to concat for each response after forward
            true) //true for ignoreSelfNode
    }catch(e){
        console.log("error 3")
        res.json({ error: true, stream: null, e:e })
    }
   
})

router.post('/get-stream-private', auth,  async (req, res) => {
    console.log("-------------------------")
    console.log("/publication/get-stream-private") //, req.body)
    
    let userPage = await Page.findOne( { owner: req.user._id, type: "user" } )
    let adminPages = await Page.find( { owner: req.user._id, type: { '$ne' : "user" } } )
    let pagesAdmin = []
    console.log("adminPages", adminPages.length)
    if(adminPages != null)
        adminPages.forEach(page=>{ pagesAdmin.push(page.uid) })

    try{ //broadcast request (to each instance of the network)
        let nbPerPage = 5
        core.broadcaster.post('/broadcast/get-stream-private', 
            {   pageNumber: req.body.pageNumber,
                search: req.body.search,
                pageUid: req.body.pageUid,
                userPageUid: userPage.uid,
                nbPerPage: nbPerPage,
                pagesAdmin: pagesAdmin,
                userPageCoordinates: userPage.coordinates,
            }, //params
            function(dataRes){ // == then (after response from all instances)
                //if network sent data
                if(dataRes != null){
                    //remove all post from blacklisted authors
                    let finalRes = []
                    dataRes.forEach((post)=>{
                        let pos = -1
                        userPage.blacklist.forEach((p, i)=>{
                            if(p.uid == post.signed.uid) pos = i
                        })
                        //if the author is in my black list: not pushed
                        if(pos == -1) finalRes.push(post)
                    })
                    //sort all res by "created" date
                    finalRes = _.sortBy(finalRes, 'created').reverse()
                    res.json({  error: false, 
                                stream: finalRes, 
                                nbPerPage: nbPerPage })
                }else{
                    console.log("error 1 : dataRes == null")
                    res.json({ error: true, stream: null, 
                            errorMsg: 'No stream found in this bdd' })
                }
                    
            },function(result){ // == catch
                console.log("error 2 : fCatch")
            },  
            'stream') //dataRes[id] to concat for each response after forward
    }catch(e){
        console.log("error 3")
        res.json({ error: true, stream: null, e:e })
    }
   
})

router.post('/get-post', auth,  async (req, res) => {
    console.log("-------------------------")
    console.log("/publication/get-post", req.body.postUid)
    
    try{ //broadcast request (to each instance of the network)
        let nodePath = await core.getNodePath(req.body.postUid)
        let userPage = await Page.findOne({ owner: req.user._id })
        //console.log("try contact", nodePath + '/broadcast/publication-get-post', req.body.postUid)
        core.post(nodePath + '/broadcast/publication-get-post', 
            {   postUid : req.body.postUid,
                userPageCoordinates: userPage.coordinates
            }, //params
            function(dataRes){ // == then (after response from all instances)
                console.log("dataRes", dataRes.data)
                //if network sent data
                if(dataRes != null && dataRes.data.error == false){
                    let postObj = dataRes.data.postObj

                    //for private post, check if the user is the owner of a page in receivers
                    if(postObj.scopeType == "private"){
                        let found = false
                        postObj.receivers.forEach(receiver => {
                            req.user.pages.forEach(myPage => {
                                if(receiver.uid == myPage.uid) found = true
                            })
                        })
                        console.log("Want private message, authorized ?", found)
                        if(!found) return res.json({ error: true })
                    }

                    res.json({  error: false, singlePost: postObj })
                }else{
                    console.log("error 1 : dataRes == null")
                    res.json({ error: true, singlePost: null, 
                            errorMsg: 'No stream found in this bdd' })
                }
                    
            },function(result){ // == catch
                console.log("error 2 : fCatch", result)
            })
    }catch(e){
        console.log("error 3")
        res.json({ error: true, stream: null, e:e })
    }
   
})

router.post('/action', auth,  async (req, res) => {
    console.log("-------------------------") //test
    console.log("/publication/action", req.body.type)
    
    try{ //broadcast request (to each instance of the network)
        let nodePath = await core.getNodePath(req.body.postUid)
        let userPage = await Page.findOne({ owner: req.user._id })
        console.log("try contact", nodePath + '/broadcast/publication-action', req.body.type, req.body.postUid)
        core.post(nodePath + '/broadcast/publication-action', 
            {   type : req.body.type,
                postUid : req.body.postUid,
                pageUid : userPage.uid,
                pageName : userPage.name,
                bool : req.body.bool,
                userPageCoordinates: userPage.coordinates
            }, //params
            function(dataRes){ // == then (after response from all instances)
                console.log("dataRes", dataRes.data)
                //if network sent data
                if(dataRes != null){
                    let postObj = dataRes.data.postObj
                    res.json({  error: false, postObj: postObj })
                }else{
                    console.log("error 1 : dataRes == null")
                    res.json({ error: true, postObj: null, 
                            errorMsg: 'No stream found in this bdd' })
                }
                    
            },function(result){ // == catch
                console.log("error 2 : fCatch", result)
            })
    }catch(e){
        console.log("error 3")
        res.json({ error: true, stream: null, e:e })
    }
   
})

router.post('/extract-meta', auth,  async (req, res) => {
    console.log("-------------------------")
    console.log("/publication/extract-meta", req.body.link)
    
    urlMetadata(req.body.link).then(
        function (metadata) { // success handler
          console.log(metadata)
          let meta = {
            url: metadata['og:url'],
            title: metadata.title,
            image: metadata.image,
            description: metadata['og:description'],
            site_name: metadata['og:site_name'],
          }
          res.json({ error: false, embedMetaLink: meta })
        },
        function (error) { // failure handler
          console.log("urlMetadata error" , error)
          res.json({ error: true, embedMetaLink: null, e:error })
        })
   
})

router.get('/km-limit', auth,  async (req, res) => {
    console.log("-------------------------")
    console.log("/publication/km-limit")
    
    let userPage = await Page.findOne( { owner: req.user._id, type: "user" } )
    if(userPage == null)
        res.json({ error: true })

    let kmToday = await core.getKmToday(userPage.uid)
    console.log("/publication/km-limit kmMax", core.kmMax, kmToday)
    res.json({ error: false, kmRemainToday: core.kmMax - kmToday })
})

module.exports = router;