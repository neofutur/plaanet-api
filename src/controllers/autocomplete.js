const { Router } = require('express');
const urlMetadata = require('url-metadata');

const { ValidationError, EntityDoesNotExistsError } = require('../core/errors');
const authGuard = require('../core/middleware/auth');
const OEmbedService = require('../services/oembed');

const router = Router();

const emptyToNull = (str) =>
  str.replace(/\s/, '') !== ''
    ? str
    : null;

/**
 * @route GET /autocomplete/oembed
 * @description Resolve the embed metadatas for an URL using the OEmbed standard.
 * @requires auth
 * @param {Query<String>} url - The URL you want to get OEmbed metas from.
 */
router.get('/oembed', authGuard, async function autocompleteOEmbed(req, res, next) {
  try {
    const { query } = req;
    const oembedService = new OEmbedService();

    if (!query.url) {
      throw new ValidationError('Missing url query param.');
    }

    const metadatas = await oembedService.resolveForUrl(query.url);
    if (!metadatas) {
      throw new EntityDoesNotExistsError('Cannot retrieve metadatas for url.', url);
    }

    return res.json({
      success: true,
      data: {
        oembed: {
          url: query.url,
          title: metadatas.title,
          description: null,
          image: metadatas.thumbnail_url,
          site_name: metadatas.provider_name,
          site_url: metadatas.provider_url,
          embed: {
            html: metadatas.html,
            width: metadatas.width,
            height: metadatas.height,
          }
        },
      },
    });
  } catch (err) {
    next(err);
  }
});

/**
 * @route GET /autocomplete/opengraph
 * @description Resolve the embed metadatas for an URL using the OpenGraph standard.
 * @requires auth
 * @param {Query<String>} url - The URL you want to get OpenGraph metas from
 */
router.get('/opengraph', authGuard, async function autocompleteOpenGraph(req, res, next) {
  try {
    const { query } = req;

    if (!query.url) {
      throw new ValidationError('Missing url query param.');
    }

    const metadatas = await urlMetadata(query.url);
    if (!metadatas) {
      throw new EntityDoesNotExistsError('Cannot retrieve metadatas for url.', url);
    }

    return res.json({
      success: true,
      data: {
        opengraph: {
          url: query.url,
          title: emptyToNull(metadatas['title']),
          description: emptyToNull(metadatas['og:description']),
          image: emptyToNull(metadatas['image']) || emptyToNull(metadatas['og:image']) || emptyToNull(metadatas['twitter:image']),
          site_name: emptyToNull(metadatas['og:site_name']),
        }
      },
    });
  } catch (err) {
    next(err);
  }
});

module.exports = router;