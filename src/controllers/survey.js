var express = require("express")
var router = express.Router()
const config = require('config');

const auth = require("../core/middleware/auth")

// const { User, validate } = require('../models/User')
// const Instance = require('../models/Instance')
// const Publication = require('../models/Publication')
// const Notification = require('../models/Notification')
// const Page = require('../models/Page')
//const core = require('../core/core')

let multichain = require("multinodejs")({
  port: config.multichain.port,
  host: config.multichain.host,
  user: config.multichain.user,
  pass: config.multichain.pass
});


router.get('/test',  async (req, res) => {
    console.log("-------------------------")
    console.log("/survey/test")

    
    multichain.getInfo((err, info) => {
      //console.log('getInfo error', info)
      if(err) throw err 
    })

    multichain.publish({  stream: 'test1', 
                          key: 'nodekey2', 
                          data: {"text":"hello world bamboula !"},
                          options: 'offchain'
                        }).catch((e)=>{
                          console.log("error publish :", e)
                        })
    
    let items = await multichain.listStreamQueryItems({ stream: 'test1', query : { key: 'nodekey2' } })
    console.log("items :", items)

})

module.exports = router;