var mongoose = require("mongoose")
const config = require('config');
const Joi = require('joi');

var userSchema = new mongoose.Schema({
    name: String,
    email: String,
    password: String,
    accessToken: String,
    isActive: Boolean,
    isAdmin: Boolean,
    lastActivityDate: Date,
    referrerUid: String,
    useMobileApp: String,
    referredCount: {
      type: Number,
      default: 0,
    },

    pages: [
      {
          type: mongoose.Schema.Types.ObjectId,
          ref: 'Page'
      }
    ],
    wallets: [
      {
          type: mongoose.Schema.Types.ObjectId,
          ref: 'Wallet'
      }
    ],
    contacts: [
      {
          type: mongoose.Schema.Types.ObjectId,
          ref: 'Wallet'
      }
    ]
})

var User = mongoose.model('User', userSchema)


//function to validate user 
function validateUser(user) {
    const schema = {
      username: Joi.string().min(3).max(50).required(),
      //email: Joi.string().min(5).max(255).required().email(),
      password: Joi.string().min(8).max(255).required(),
      referrerUid: Joi.string().allow('').optional(),
    };
  
    return Joi.validate(user, schema);
  }
  
exports.User = User; 
exports.validate = validateUser;
