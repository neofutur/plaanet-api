var mongoose = require("mongoose")

var pageSchema = new mongoose.Schema({
    uid: String,
    slug: String,
    slug: String,
    name: String,
    description: String,
    city: String,
    address: String,
    coordinates: Array,
    avatar: String,
    type: String,
    created: Date,
    dateLastPosition: Date,
    dateReadNotif: Date,
    dateReadPrivate: Date,
    startDate: Date,
    blacklist: Array,
    follows: Array,
    followers: Array,
    contacts: Array,
    hashtags: Array,
    hashtagsG: Array,
    hashtagsC: Array,
    hashtagsM: Array,
    canSendSurvey: Boolean,
    
    roles: mongoose.Schema.Types.Mixed,        //{ admins: [{ uid, name}], moderators:[], editors: [] }
    pendingRoles: mongoose.Schema.Types.Mixed, //set by user  { admins: [{ uid, name}], moderators:[], editors: [] }
    inviteRoles: mongoose.Schema.Types.Mixed,  //set by admin { admins: [{ uid, name}], moderators:[], editors: [] }
    
    owner: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User'
    }
})


pageSchema.virtual('pages', {
    ref: 'User',
    localField: '_id',
    foreignField: 'pages'
});


pageSchema.virtual('author', {
    ref: 'Publication',
    localField: '_id',
    foreignField: 'owner'
});


pageSchema.virtual('signed', {
    ref: 'Publication',
    localField: '_id',
    foreignField: 'pages'
});


pageSchema.virtual('target', {
    ref: 'Publication',
    localField: '_id',
    foreignField: 'pages'
});


var Page = mongoose.model('Page', pageSchema)
module.exports = Page;
