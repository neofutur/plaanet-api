var mongoose = require("mongoose")

var surveySchema = new mongoose.Schema({
    uid: String, //uniq id of the survey
    roomUid: String, //correspond au pageUid d'une Page de type Assemblée
    type: String,
    title: String,
    text: String,
    tags: Array,
    answers: Array, //array of strings
    created: Date,
    originalSurveyUid: String,

    duration: Number, //number of day while vote are open
    dateOpen: Date,
    dateEnd: Date,
    
    open: Boolean,
    rejected: Boolean,
    postUid: String,

    author: mongoose.Schema.Types.Mixed, //{ uid, name, city }
    supports: Array, 
    votes: Array, 
    results: Array, 

    canVote: Boolean, //not set in db, only calculated and return after find (based on user coordinates)

    parentPage: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Page'
    }
})


// pageSchema.virtual('target', {
//     ref: 'Publication',
//     localField: '_id',
//     foreignField: 'pages'
// });


var Survey = mongoose.model('Survey', surveySchema)
module.exports = Survey;
