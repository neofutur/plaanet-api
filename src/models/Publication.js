var mongoose = require("mongoose")

var publicationSchema = new mongoose.Schema({
    uid: String,
    text: String,
    title: String,
    coordinates: Array,
    scopeGeo: mongoose.Schema.Types.Mixed,
    embedMetaLinks: mongoose.Schema.Types.Mixed,
    fromCity: String,
    imageName: String,
    radius: Number,
    scopeType: String,
    created: Date,
    sharable: Boolean,
    isFirst: Boolean,
    sharedPostUid: String,
    sharedSurveyUid: String,
    pdfName: String,
    author: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Page'
    },
    signed: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Page'
    },
    target:  mongoose.Schema.Types.Mixed, //{ uid, name, city }
    comments: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Comment'
    }],
    pdf: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'File'
    },
    likes: Array,
    dislikes: Array,
    favorites: Array,
    viewers: Array,
    receivers: Array,
    hashtags: Array,
    postShared: mongoose.Schema.Types.Mixed,
    surveyShared: mongoose.Schema.Types.Mixed
})

var Publication = mongoose.model('Publication', publicationSchema)
module.exports = Publication;
