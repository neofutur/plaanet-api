const UserService = require('./user');
const Page = require('../models/Page');
const Live = require('../models/Live');
const LiveMsg = require('../models/LiveMsg');
const PageService = require("../services/page");
const { EntityDoesNotExistsError } = require('../core/errors');
const core = require('../core/core');
const axios = require('axios');

module.exports = class LiveService {

  constructor() {
    //this.liveService = new LiveService();
  }

  async openLiveChannel(params){
    console.log("openLiveChannel") //, params)

    let live = await Live.findOne({ 'uid' : params.liveUid })
    if(live == null) return { error: true, liveMsg: null }
    //console.log("> LIVE SERVICE openLiveChannel Live found :", live.uid)

    /** TODO : check if category and channel exist */
    let page = await Page.findOne({ 'uid' : live.parentPageUid })
    if(page != null){
        let pageService = new PageService()
        let isEditor = pageService.isRole(page.roles, 'editor', { uid: params.userPageUid })
        //console.log("isEditor", isEditor, page.roles, params.userPageUid)
        if(!isEditor) 
          return { error: true, 
                   liveMsg: null, 
                   msg: 'sorry, you are not member of this page' }
    }
    /** TODO : check permission to read msg here */

    //update dateLastOpen for the opening channel 
    live.categories.forEach((category, a) => {
        if(category.uid == params.categoryUid){
            category.channels.forEach((channel, b) => {
                if(channel.uid == params.channelUid){
                    if(channel.dateLastOpen == null) 
                      live.categories[a].channels[b].dateLastOpen = []
                    let found = false
                    channel.dateLastOpen.forEach((data, c) => {
                        if(data.uid == params.userPageUid){
                            live.categories[a].channels[b].dateLastOpen[c].date = new Date()
                            found = true
                        }
                    })
                    if(!found){
                        live.categories[a].channels[b].dateLastOpen.push({
                            uid: params.userPageUid,
                            date: new Date()
                        })
                    }
                }
            })
        }
    })
    live.markModified('categories')
    live.save()

    let conversation = await this.getConversation(params, 50, 0)

    //console.log("> LIVE SERVICE openLiveChannel conversation found :", conversation.length)
    return { error: false, conversation: conversation }
  }

  async getConversation(params, limit, skip){
    let conv = await LiveMsg.find({ 'liveUid' : params.liveUid,
                                    'categoryUid' : params.categoryUid,
                                    'channelUid' : params.channelUid, })
                            .sort({'created':-1})
                            .limit(limit)
                            .skip(limit*skip)
    return conv.reverse()
  }

  async flashMembersByNode(page){
    let arr = []
    let keys = []
    if(typeof page != "undefined" && page != null)
    await Promise.all(page.roles.editor.map(async (member, x) => {
      let nodeName = await core.getNodePath(member.uid)
      if(arr[nodeName] == null) {
        arr[nodeName] = []
        keys.push(nodeName)
      }

      arr[nodeName].push(member.uid)
      //console.log("flashMembersByNode", member.uid, nodeName, arr)
    }))
    //console.log("flashMembersByNode", page.uid, page.roles.editor.length)
    return { arr: arr, keys: keys } 
  }
  
}