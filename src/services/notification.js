const _ = require('underscore');

const core = require('../core/core');

module.exports = class NotificationService {
  
  constructor() { }

  async getNotificationsForUser(id) {
    return new Promise((resolve, reject) => {
      //broadcast request (to each instance of the network)
      core.broadcaster.post(
        '/broadcast/get-my-notifications',
        { pageUid: id }, //params
        function (dataRes) { // == then (after response from all instances)
          //if network sent data
          if (dataRes != null) {
            //sort all res by "created" date
            dataRes = dataRes.sort((a, b) => new Date(a.updated).getTime() - new Date(b.updated).getTime());
            //dataRes = _.sortBy(dataRes, 'updated').reverse()
            resolve(dataRes);
          } else {
            reject(new Error('No notifications found'))
          }
        }, function (err) { // == catch
          reject(err)
        },
        'notifications'
      )
    });
  }

}