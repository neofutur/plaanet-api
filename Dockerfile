FROM node:10.13.0-alpine

RUN mkdir -p /opt/plaanet-api
WORKDIR /opt/plaanet-api

# Copy only package.json and yarn.lock for cache
COPY package.json yarn.lock ./

# Install Dependncies
RUN yarn install --ignore-optional --pure-lockfile --non-interactive

# Copy Files
COPY . ./
RUN touch ./config/production.json

# Run production API.
ENV NODE_ENV=production
ENTRYPOINT [ "node", "main.js" ]
