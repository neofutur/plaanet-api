# Deploy with Docker and Docker Compose

In order for newscomers to get started easily and without much troubles we packaged the Plaanet API as a Docker image. This image is also the base image that is used in the `docker-compose.yml` file so you can bootstrap a new node in no time!

## Using Docker

First you will need to pull the official `plaanet-api:latest` image so you can run it. Please note that you'll need to have a `mongodb` server running somewhere (can be configured in `config/production.json`)

```shell
docker pull registry.gitlab.com/plaanet/plaanet-api:latest
```

Once done, you can create & run a container this way:

```shell
docker run -d -p 3000:3000 -p 8080:8080 --name plaanet-api-prod plaanet-api:latest
```

If everything gone well, you should be able to reach https://127.0.0.1:3000/ and be greeted by an awesome `Cannot GET /` error page, meaning that your Plaanet instance is up and running. Please then follow the regular install path ([plaanet-install: Boot your node](https://gitlab.com/plaanet/plaanet-install#boot-your-node-)).

## Using docker-compose

If you don't have a running mongodb server, or you prefer to use `docker-compose` (i.e. so you can scale with swarm or load-balance requests) we do provide a `docker-compose.yml` file. You can also write your own or modify it so it better fit your needs. Please note that Plaanet API can be configured from this file by using env vars, or via the `config/production.json` file.

Heads up to the project root and run this:

```shell
docker-compose up
```

This will mount several volumes for you to configure important things easily:

- `config/`: This folder/volume will contain the configuration files for Plaanet api to work.
- `ssl/`: This is where you should place your `ca.crt`, `host.crt` and `host.key` files in order to get SSL to work.
- `db-volume/`: This one is created and used by mongodb itself to store instance's data.

If everything gone well, you should be able to reach https://127.0.0.1:3000/ and be greeted by an awesome `Cannot GET /` error page, meaning that your Plaanet instance is up and running. Please then follow the regular install path ([plaanet-install: Boot your node](https://gitlab.com/plaanet/plaanet-install#boot-your-node-)).

## docker-compose for local development

It may be usefull to use the `docker-compose` strategy in development to ensure that you're developing in the same environment as the production one. Also, it avoids you to rebuild the Docker image every single time you perform a modification to the source code, as docker-compose will do it (and cache it) for you, for free!

That's where `docker-compose.dev.yml` file comes in handy.

Starting the dev nodes (2 nodes by default) can be done by typing this in a shell:

```shell
docker-compose -f docker-compose.dev.yml up -d
```

You can restart the services this way:

```shell
$ docker-compose -f docker-compose.dev.yml up -d --force-recreate --build api-node1 api-node2
$ docker-compose -f docker-compose.dev.yml logs -f api-node1 api-node2
```

If you want to restart only one node (i.e. to test for regressions), do this:

```shell
$ docker-compose -f docker-compose.dev.yml up -d --force-recreate --build api-node1 
$ docker-compose -f docker-compose.dev.yml logs -f api-node1
```
