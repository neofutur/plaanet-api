const Config = require('./src/core/config');

const dbUrl = Config.Database.PASS
    ? `mongodb://${Config.Database.HOST}:${Config.Database.PORT}/${Config.Database.NAME}?authSource=admin&readPreference=primary&appName=plaanet-api&ssl=${Config.Database.SECURE}`
    : `mongodb://${Config.Database.HOST}:${Config.Database.PORT}/${Config.Database.NAME}?readPreference=primary&appName=plaanet-api&ssl=${Config.Database.SECURE}`;

const config = {
  mongodb: {
    url: dbUrl,
    databaseName: Config.Database.NAME,
    user: Config.Database.USER,
    pass: Config.Database.PASS,
    options: {
      useNewUrlParser: true, // removes a deprecation warning when connecting
      useUnifiedTopology: true, // removes a deprecating warning when connecting
    }
  },
  migrationsDir: "migrations",
  changelogCollectionName: "changelog",
  migrationFileExtension: ".js",
};

// Return the config as a promise
module.exports = config;
