const PlaanetApp = require('./src/app');

/** 
 * That's ugly I know... lol
 * something in spirit of redux/awilix would better fit
 */
if (!global.store) {
  global.store = {
    isInstanceInitialized: false,
    initToken: null,
  };
}

process.on('ERROR: unhandledRejection', error => {
  console.log('ERROR: unhandledRejection', error.message);
});

process.on('ERROR: uncaughtException', error => {
  console.log('ERROR: uncaughtException', error.message);
})

new PlaanetApp();
